package provisioningapi

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/api/equality"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/tools/cache"
)

const resyncDuration = 5 * time.Minute

type GitlabTenantsDB struct {
	DB map[int64]TenantData
	sync.RWMutex
}

var (
	gitlabTenantGVR = schema.GroupVersionResource{
		Group:    "opstrace.com",
		Version:  "v1alpha1",
		Resource: "gitlabobservabilitytenants",
	}
)

const (
	tenantStatusReady    = "ready"
	tenantStatusNotReady = "not ready"
	tenantStatusUnknown  = "unknown"
)

func determineTenantStatus(u *unstructured.Unstructured) (string, error) {
	var conditionList []metav1.Condition

	// NOTE(prozlach): We do not do error-checking here as we are know that we
	// will always be dealing with GitLabObservabilityTenant objects which must
	// have this status. If they dont, then the program should crash hard
	// anyway as this is a programming error.
	//nolint: errcheck
	conditions, ok, _ := unstructured.NestedSlice(u.Object, "status", "conditions")
	if !ok {
		// The controller has not yet set the status information
		return tenantStatusUnknown, nil
	}
	conditionsJSON, err := json.Marshal(conditions)
	if err != nil {
		return "", fmt.Errorf("unable to marshal conditions back from unstructured: %w", err)
	}
	err = json.Unmarshal(conditionsJSON, &conditionList)
	if err != nil {
		// NOTE(prozlach): Yes, it can be triggered, for example due to
		// out-of-mem issues.
		return "", fmt.Errorf("unable to unmarshal conditions: %w", err)
	}

	for _, condition := range conditionList {
		if condition.Type == common.ConditionTypeReady {
			if condition.Status == metav1.ConditionTrue {
				return tenantStatusReady, nil
			}

			return tenantStatusNotReady, nil
		}
	}
	return tenantStatusUnknown, nil
}

func unstructuredToTenantData(u *unstructured.Unstructured) (TenantData, error) {
	name := u.GetName()
	// NOTE(prozlach): We do not do error-checking here as we are know that we
	// will always be dealing with GitLabObservabilityTenant objects which must
	// have this status. If they dont, then the program should crash hard
	// anyway as this is a programming error.
	//nolint: errcheck
	topLevelNamespaceID, _, _ := unstructured.NestedInt64(
		u.Object, "spec", "topLevelNamespaceID",
	)
	status, err := determineTenantStatus(u)
	if err != nil {
		return TenantData{}, fmt.Errorf("unable to determine tenant status: %w", err)
	}

	res := TenantData{
		Name:                &name,
		TopLevelNamespaceID: &topLevelNamespaceID,
		Status:              &status,
	}

	return res, nil
}

func informerAddFuncFactory(logger *zap.SugaredLogger, db *GitlabTenantsDB) func(any) {
	return func(obj interface{}) {
		//nolint: errcheck
		u := obj.(*unstructured.Unstructured)

		db.Lock()
		defer db.Unlock()

		newTenant, err := unstructuredToTenantData(u)
		if err != nil {
			logger.Errorw("unable to convert unstructured to tenants data", "error", err)
		}
		oldTenant, ok := db.DB[*newTenant.TopLevelNamespaceID]

		if ok {
			if *oldTenant.Name != *newTenant.Name {
				// NOTE(prozlach): Ideally this should never happen, in practice
				// preventing it using validating webhook is tracked by:
				// https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2288
				logger.Errorw(
					"Overriding existing tenant with a new one in the DB",
					"oldTenantName", oldTenant.Name,
					"newTenantName", newTenant.Name,
					"namespaceID", newTenant.TopLevelNamespaceID,
				)
			} else {
				logger.Warnw(
					"Add call for the same tenant data",
					"tenantName", newTenant.Name,
					"namespaceID", newTenant.TopLevelNamespaceID,
				)
			}
		} else {
			logger.Debugw(
				"Adding tenant to the DB",
				"tenantName", newTenant.Name,
				"namespaceID", newTenant.TopLevelNamespaceID,
				"status", newTenant.Status,
			)
		}

		db.DB[*newTenant.TopLevelNamespaceID] = newTenant
	}
}

func informerUpdateFuncFactory(logger *zap.SugaredLogger, db *GitlabTenantsDB) func(any, any) {
	return func(oldObj, newObj interface{}) {
		//nolint: errcheck
		newU := newObj.(*unstructured.Unstructured)
		//nolint: errcheck
		oldU := oldObj.(*unstructured.Unstructured)
		if equality.Semantic.DeepEqual(oldU, newU) {
			return
		}

		db.Lock()
		defer db.Unlock()

		newTenant, err := unstructuredToTenantData(newU)
		if err != nil {
			logger.Errorw("unable to convert unstructured to tenants data", "error", err)
		}
		oldTenant, err := unstructuredToTenantData(oldU)
		if err != nil {
			logger.Errorw("unable to convert unstructured to tenants data", "error", err)
		}

		tenantNameChange := *newTenant.TopLevelNamespaceID != *oldTenant.TopLevelNamespaceID
		_, tenantInDB := db.DB[*newTenant.TopLevelNamespaceID]

		if tenantNameChange || !tenantInDB {
			if tenantNameChange {
				logger.Warnw(
					"Update event where tenant changes its topLevelNamespaceID",
					"newTenantName", newTenant.Name,
					"oldTenantName", oldTenant.Name,
					"namespaceID", newTenant.TopLevelNamespaceID,
				)
			}
			if !tenantInDB {
				logger.Warnw(
					"Update event of a tenant that isn't in DB yet",
					"tenantName", newTenant.Name,
					"namespaceID", newTenant.TopLevelNamespaceID,
				)
			}
		} else {
			logger.Debugw(
				"Updating tenant",
				"tenantName", newTenant.Name,
				"namespaceID", newTenant.TopLevelNamespaceID,
				"status", newTenant.Status,
			)
		}

		db.DB[*newTenant.TopLevelNamespaceID] = newTenant
	}
}

func informerDeleteFuncFactory(logger *zap.SugaredLogger, db *GitlabTenantsDB) func(any) {
	return func(obj interface{}) {
		u, ok := obj.(*unstructured.Unstructured)
		if !ok {
			logger.Warnw("Informer received unknown delete event")
			return
		}

		db.Lock()
		defer db.Unlock()

		tenant, err := unstructuredToTenantData(u)
		if err != nil {
			logger.Errorw("unable to convert unstructured to tenants data", "error", err)
		}

		if _, ok := db.DB[*tenant.TopLevelNamespaceID]; !ok {
			logger.Warnw(
				"Update event for tenant that does not exist in DB",
				"tenantName", tenant.Name,
				"namespaceID", tenant.TopLevelNamespaceID,
			)
		} else {
			logger.Debugw(
				"Deleting tenant",
				"tenantName", tenant.Name,
				"namespaceID", tenant.TopLevelNamespaceID,
			)

			delete(db.DB, *tenant.TopLevelNamespaceID)
		}
	}
}

func NewGitlabTenantDB(
	logger *zap.SugaredLogger,
	ctx context.Context,
	dynamicClient *dynamic.DynamicClient,
) (*GitlabTenantsDB, error) {
	db := new(GitlabTenantsDB)
	db.DB = make(map[int64]TenantData)

	factory := dynamicinformer.NewDynamicSharedInformerFactory(dynamicClient, resyncDuration)
	informer := factory.ForResource(gitlabTenantGVR).Informer()
	_, err := informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    informerAddFuncFactory(logger, db),
		UpdateFunc: informerUpdateFuncFactory(logger, db),
		DeleteFunc: informerDeleteFuncFactory(logger, db),
	})
	if err != nil {
		return nil, fmt.Errorf("unable to add event handlers to informer: %w", err)
	}

	go func() {
		informer.Run(ctx.Done())
	}()
	cache.WaitForCacheSync(ctx.Done(), informer.HasSynced)

	logger.Info("Informer has started and synchronized")

	return db, nil
}
