package provisioningapi

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ProjectPathParams struct {
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

type TopLevelNamespaceHeader struct {
	GroupID *int64 `header:"x-top-level-namespace" binding:"required,numeric,min=1"`
}

type TenantData struct {
	Name                *string `json:"name" form:"name" binding:"required,dns_rfc1035_label,min=1"`
	TopLevelNamespaceID *int64  `json:"topLevelNamespaceID" form:"topLevelNamespaceID" binding:"required,numeric,min=1"`
	Status              *string `json:"status"`
}

type Config struct {
	K8sClient client.Client
	TenantsDB *GitlabTenantsDB
	Logger    *zap.SugaredLogger
}

func handlerCommon(logger *zap.SugaredLogger, ginCtx *gin.Context, fName string) int64 {
	var pathParams ProjectPathParams
	if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
		ginCtx.AbortWithError(
			http.StatusBadRequest,
			fmt.Errorf("path parameters validation failed: %w", err),
		)
		return -1
	}

	headers := new(TopLevelNamespaceHeader)
	if err := ginCtx.ShouldBindHeader(headers); err != nil {
		// NOTE(prozlach): This header should have been set by Gatekeeper,
		// abort.
		ginCtx.AbortWithError(
			http.StatusBadRequest,
			fmt.Errorf(
				"top-level namespace header is not set or invalid: %w", err,
			),
		)
		return -1
	}
	logger.Debugw(
		fName,
		"projectID", pathParams.ProjectID,
		"topLevelNamespaceID", headers.GroupID,
	)

	return *headers.GroupID
}

func FetchTenantHandlerFactory(cfg Config) func(*gin.Context) {
	return func(ginCtx *gin.Context) {
		groupID := handlerCommon(cfg.Logger, ginCtx, "FetchTenantHandlerFactory")
		if ginCtx.IsAborted() {
			return
		}

		// NOTE(prozlach): ProvisioningAPI by design is mean to allow fetching
		// tenants assigned to the given project ID. We do not forsee ATM
		// fetching tenants by top-level GroupID or tenant name.
		cfg.TenantsDB.RLock()
		defer cfg.TenantsDB.RUnlock()

		tenantData, ok := cfg.TenantsDB.DB[groupID]
		if !ok {
			ginCtx.AbortWithStatus(http.StatusNotFound)
			return
		}

		ginCtx.JSON(http.StatusOK, tenantData)
	}
}

func CreateTenantHandlerFactory(cfg Config) func(*gin.Context) {
	return func(ginCtx *gin.Context) {
		groupID := handlerCommon(cfg.Logger, ginCtx, "CreateTenantHandlerFactory")
		if ginCtx.IsAborted() {
			return
		}

		// NOTE(prozlach): For now the GitlabTenant CRD does not have any
		// fields we could set here, so we take a shortcut and query the
		// cfg.TenantsDB instead. If there were any user-modifyable fields, then we
		// would need to do SSA patching instead.
		//
		// In the long term we will probably start defining things like e.g.
		// t-shirt sizes which will be passed by a json in a request body and
		// then validated+decoded by Gin using a dedicated struct. This
		// requires more thought as the tenant creation calls are
		// project-scoped (i.e. user pases only their projectID in the call)
		// and e.g. t-shirt size affects whole top-level group. On the other
		// hand the provisioning API uses user's credentials (GLPAT,Oauth
		// token), and changing of the tenant's t-shirt size would require some
		// extra billing checks and authorization - user should not be allowed
		// to do it themselves.

		// NOTE(prozlach): ProvisioningAPI by design is mean to allow fetching
		// tenants assigned to the given project ID. We do not forsee ATM
		// fetching tenants by top-level GroupID or tenant name.
		cfg.TenantsDB.RLock()
		defer cfg.TenantsDB.RUnlock()

		if _, ok := cfg.TenantsDB.DB[groupID]; ok {
			// NOTE(prozlach) Informer is eventually consistent, it is possible
			// that we return this message prematurely.
			cfg.Logger.Debugw("tenant already exists", "groupID", groupID)
			ginCtx.AbortWithStatus(http.StatusConflict)
			return
		}

		ctx, cancelF := context.WithTimeout(ginCtx, K8sRequestTimeout)
		defer cancelF()

		obj := &opstracev1alpha1.GitLabObservabilityTenant{
			ObjectMeta: metav1.ObjectMeta{
				Name: fmt.Sprintf("tenant-%d", groupID),
			},
			Spec: opstracev1alpha1.GitLabObservabilityTenantSpec{
				TopLevelNamespaceID: groupID,
			},
		}
		err := cfg.K8sClient.Create(ctx, obj)
		if err != nil {
			ginCtx.AbortWithError(
				http.StatusInternalServerError,
				fmt.Errorf("unable to ensure tenant: %w", err),
			)
			return
		}

		cfg.Logger.Debugw("tenant created", "groupID", groupID)
		ginCtx.Status(http.StatusCreated)
	}
}

func DeleteTenantHandlerFactory(cfg Config) func(*gin.Context) {
	return func(ginCtx *gin.Context) {
		var pathParams ProjectPathParams
		if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
			ginCtx.AbortWithError(
				http.StatusBadRequest,
				fmt.Errorf("path parameters validation failed: %w", err),
			)
			return
		}

		cfg.Logger.Debugw("DeleteTenantHandlerFactory executes", "projectID", pathParams.ProjectID)

		ginCtx.Status(http.StatusNotImplemented)
	}
}
