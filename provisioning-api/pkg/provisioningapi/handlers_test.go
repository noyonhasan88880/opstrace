package provisioningapi_test

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"gitlab.com/gitlab-org/opstrace/opstrace/provisioning-api/pkg/provisioningapi"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const topLevelNamespace = 22

func createTenant(ctx context.Context, status string) *opstracev1alpha1.GitLabObservabilityTenant {
	GinkgoHelper()

	res := &opstracev1alpha1.GitLabObservabilityTenant{
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("tenant-%d", topLevelNamespace),
		},
		Spec: opstracev1alpha1.GitLabObservabilityTenantSpec{
			TopLevelNamespaceID: topLevelNamespace,
		},
	}
	Expect(crK8sClient.Create(ctx, res)).To(Succeed())

	if status == "unknown" {
		return res
	}

	var condition metav1.Condition
	switch status {
	case "not ready":
		condition = metav1.Condition{
			Status:             metav1.ConditionFalse,
			Reason:             common.ReconciliationFailedReason,
			Message:            "foo error prevents tenant from getting ready",
			Type:               common.ConditionTypeReady,
			ObservedGeneration: 1,
		}
	case "ready":
		condition = metav1.Condition{
			Status:             metav1.ConditionTrue,
			Reason:             common.ReconciliationSuccessReason,
			Message:            "All components are in ready state",
			Type:               common.ConditionTypeReady,
			ObservedGeneration: 1,
		}
	default:
		Fail(fmt.Sprintf("Unrecognized status given: %s", status))
	}
	apimeta.SetStatusCondition(&res.Status.Conditions, condition)
	Expect(crK8sClient.Status().Update(ctx, res)).To(Succeed())

	return res
}

var _ = Context("handlers tests", func() {
	var (
		router    *gin.Engine
		logOutput *testutils.SyncBuffer
		cancelF   func()
		ctx       context.Context
	)

	BeforeEach(func() {
		ctx, cancelF = context.WithCancel(rootCtx)

		gin.SetMode(gin.DebugMode)
		gin.DefaultWriter = GinkgoWriter

		// router setup
		router = gin.New()
		router.Use(
			ginzap.GinzapWithConfig(
				logger.Desugar(),
				&ginzap.Config{
					TimeFormat: time.RFC3339,
					UTC:        true,
				},
			),
		)
		router.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))

		// setup TenantsDB
		tenantsDB, err := provisioningapi.NewGitlabTenantDB(logger, ctx, dynamicK8sClient)
		Expect(err).NotTo(HaveOccurred())

		// logs capture
		logOutput = new(testutils.SyncBuffer)
		GinkgoWriter.TeeTo(logOutput)

		// configure the middleware
		cfg := provisioningapi.Config{
			K8sClient: crK8sClient,
			TenantsDB: tenantsDB,
			Logger:    logger,
		}

		provisioningapi.SetRoutes(router, cfg)
	})

	AfterEach(func() {
		GinkgoWriter.ClearTeeWriters()
		cancelF()
	})

	Context("GET requests", func() {
		It("returns 404 when tenant does not exist", func() {
			recorder := httptest.NewRecorder()

			testReq, err := http.NewRequest(http.MethodGet, "/v3/tenant/1", nil)
			Expect(err).NotTo(HaveOccurred())
			testReq.Header.Set("x-top-level-namespace", strconv.FormatInt(topLevelNamespace, 10))

			router.ServeHTTP(recorder, testReq)

			Expect(recorder.Code).To(Equal(http.StatusNotFound))
			Expect(logOutput.String()).To(ContainSubstring(`"status": 404`))
		})

		It("returns 400 when x-top-level-namespace header is not set", func() {
			recorder := httptest.NewRecorder()

			testReq, err := http.NewRequest(http.MethodGet, "/v3/tenant/1", nil)
			Expect(err).NotTo(HaveOccurred())

			router.ServeHTTP(recorder, testReq)

			Expect(recorder.Code).To(Equal(http.StatusBadRequest))
			Expect(logOutput.String()).To(ContainSubstring(`top-level namespace header is not set or invalid`))
		})

		DescribeTable("returns 200 when tenant exists",
			func(tenantStatus string) {

				tenant := createTenant(ctx, tenantStatus)
				defer func() {
					GinkgoHelper()

					Expect(crK8sClient.Delete(ctx, tenant)).To(Succeed())
				}()
				Eventually(func(g Gomega) {
					recorder := httptest.NewRecorder()
					testReq, err := http.NewRequest(http.MethodGet, "/v3/tenant/1", nil)
					testReq.Header.Set("x-top-level-namespace", strconv.FormatInt(topLevelNamespace, 10))
					g.Expect(err).NotTo(HaveOccurred())

					router.ServeHTTP(recorder, testReq)

					g.Expect(recorder.Code).To(Equal(http.StatusOK))
					g.Expect(logOutput.String()).To(ContainSubstring(`"status": 200`))

					expected := fmt.Sprintf(`{"name":"tenant-22","topLevelNamespaceID":22,"status":"%s"}`, tenantStatus)
					g.Expect(recorder.Body.String()).To(MatchJSON(expected))
				}).WithTimeout(time.Second * 10).Should(Succeed())
			},
			Entry(nil, "ready"),
			Entry(nil, "not ready"),
			Entry(nil, "unknown"),
		)

		Context("PUT requests", func() {
			It("returns 201 when tenant did not exist before", func() {
				recorder := httptest.NewRecorder()

				testReq, err := http.NewRequest(http.MethodPut, "/v3/tenant/1", nil)
				Expect(err).NotTo(HaveOccurred())
				testReq.Header.Set("x-top-level-namespace", "103")

				router.ServeHTTP(recorder, testReq)

				res := new(opstracev1alpha1.GitLabObservabilityTenant)
				key := client.ObjectKey{
					Name: "tenant-103",
				}
				Expect(crK8sClient.Get(ctx, key, res)).To(Succeed())
				Expect(crK8sClient.Delete(ctx, res)).To(Succeed())

				Expect(recorder.Code).To(Equal(http.StatusCreated))
				Expect(logOutput.String()).To(ContainSubstring(`"status": 201`))
			})

			It("returns 409 when tenant did exist before", func() {
				tenant := createTenant(ctx, "ready")
				defer func() {
					GinkgoHelper()

					Expect(crK8sClient.Delete(ctx, tenant)).To(Succeed())
				}()

				recorder := httptest.NewRecorder()

				testReq, err := http.NewRequest(http.MethodPut, "/v3/tenant/1", nil)
				Expect(err).NotTo(HaveOccurred())
				testReq.Header.Set("x-top-level-namespace", strconv.FormatInt(topLevelNamespace, 10))

				router.ServeHTTP(recorder, testReq)

				Expect(recorder.Code).To(Equal(http.StatusConflict))
				Expect(logOutput.String()).To(ContainSubstring(`"status": 409`))
			})
		})

	})
})
