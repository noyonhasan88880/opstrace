locals {}

generate "backend" {
  path      = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
    backend "gcs" {
        bucket  = "${get_env("TF_VAR_bucket_name", "my-terraform-bucket")}"
        prefix  = "${get_env("TF_VAR_remote_state_prefix")}/${path_relative_to_include()}/terraform.tfstate"
    }
}
EOF
}