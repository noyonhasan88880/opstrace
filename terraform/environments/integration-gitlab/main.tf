terraform {
  # backend config is injected dynamically when running the pipeline
  backend "gcs" {}
}

module "gitlab" {
  source = "../../modules/gitlab/gcp"

  instance_name = var.instance_name
  project_id    = var.project_id
  zone          = var.location
  # GitLab domain. This is where the GitLab instance will run. This requires setting up a DNS zone in the GCP project manually.
  domain = var.domain
  # GCP cloud dns zone. This is the cloud dns zone that represents the gitlab_domain.
  # It is used to create an A record for the gitlab_domain to point to the GCP machine running GitLab
  cloud_dns_zone = var.cloud_dns_zone
}
