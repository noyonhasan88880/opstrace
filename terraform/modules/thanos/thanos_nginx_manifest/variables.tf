variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "kubeconfig_path" {
  type    = string
  default = ".kubeconfig"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "gke_vpc_name" {
  type        = string
  description = "VPC name of the Cluster where the thanos components will be installed"
}

variable "project_id" {
  type        = string
  description = "Google Cloud project where resources will be created"
}

variable "gke_cluster_name" {
  type        = string
  description = "Cluster/Instance name of the GKE cluster"
}

variable "thanos_receive_remote_write_domain" {
  type        = string
  description = "Domain name that will be used in Ingress resource that receives remote writes"
}

variable "source_ip_ranges_allowed_access" {
  type        = string
  description = "List of IPs that will be allowed access to the ingress resources (thanos-receiver & thanos-store) in comma separated format"
}

variable "ing_address" {
  type        = string
  description = "Ingress IP address"
}