# Deploys ClickHouse Backup cronjob.

resource "kubernetes_secret" "backup-gcs-secret" {
  metadata {
    name = var.backup_secret_name
  }

  data = {
    accessKeyID     = var.gcs_access_key_id
    accessKeySecret = var.gcs_access_key_secret
    gcsRootPath     = var.backup_gcs_bucket_url
    clickhouseDSN   = var.clickhouse_dsn
  }

  immutable = true
}

data "kustomization_overlay" "backup_jobs" {
  resources = ["${path.module}/jobs"]

  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/jobTemplate/spec/template/spec/containers/0/image
        value: ${var.clickhouse_backup_image}
    EOF
    target {
      group   = "batch"
      version = "v1"
      kind    = "CronJob"
      name    = "clickhouse-backup-full"
    }
  }

  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/jobTemplate/spec/template/spec/containers/0/image
        value: ${var.clickhouse_backup_image}
    EOF
    target {
      group   = "batch"
      version = "v1"
      kind    = "CronJob"
      name    = "clickhouse-backup-incremental"
    }
  }
}

resource "kustomization_resource" "backup_jobs" {
  for_each = data.kustomization_overlay.backup_jobs.ids
  manifest = data.kustomization_overlay.backup_jobs.manifests[each.value]
  depends_on = [
    kubernetes_secret.backup-gcs-secret,
  ]
}

// TODO(Arun): Add alerting rules on failed jobs