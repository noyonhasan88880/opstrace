---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-collector-rules
spec:
  groups:
  - name: collector
    rules:
    - alert: CollectorReplicaAbsent
      annotations:
        title: Collector replica(s) down
        description: One or more collector replicas for the given observability tenant might be down
        link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
        grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-tracing/observability-tracing?orgId=1
      expr: |
        max(
          kube_deployment_status_replicas_ready{namespace=~"tenant-.*", deployment="otel-collector"}
          <
          kube_deployment_spec_replicas{namespace=~"tenant-.*", deployment="otel-collector"}
        ) by (namespace) > 0
      for: 5m
      labels:
        alertname: CollectorDeploymentAbsent
        type: collector
        severity: s1
        alert_type: cause

    - alert: CollectorLatencyHigh
      annotations:
        title: Collector latency high
        description: Collector p99 latency for the given observability tenant is higher than 5 seconds
        link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
        grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-tracing/observability-tracing?orgId=1
      expr: |
        histogram_quantile(0.99,
          sum(
            nginx_ingress_controller_request_duration_seconds_bucket{exported_namespace=~"tenant-.*", exported_service="otel-collector"}
          ) by (exported_namespace, le, path)
        ) > 5
      for: 5m
      labels:
        alertname: CollectorLatencyHigh
        type: collector
        severity: s3
        alert_type: cause

    - alert: CollectorTooMany500s
      annotations:
        title: Number of 5xx(s) thrown by collector for the given observability tenant is high
        description: More than 3% of all requests returned 5XX
        link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
        grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-tracing/observability-tracing?orgId=1
      expr: |
        100 * max(
          sum(
            nginx_ingress_controller_requests{exported_namespace=~"tenant-.*", exported_service="otel-collector", status="500"}
          ) by (exported_namespace)
          /
          sum(
            nginx_ingress_controller_requests{exported_namespace=~"tenant-.*", exported_service="otel-collector"}
          ) by (exported_namespace)
        ) by (exported_namespace) > 3
      for: 5m
      labels:
        alertname: CollectorTooMany500s
        type: collector
        severity: s2
        alert_type: cause
