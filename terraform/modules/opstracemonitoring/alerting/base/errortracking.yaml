---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-errortracking-rules
spec:
  groups:
    - name: errortracking
      rules:
        - alert: ErrortrackingAbsent
          annotations:
            title: Errortracking replica(s) down
            description: One or more Errortracking replicas might be down.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-errortracking/observability-errortracking
          expr: |
            count(up{job=~"^errortracking-api"}) < max(kube_statefulset_replicas{statefulset="errortracking-api"})
          for: 5m
          labels:
            alertname: ErrortrackingAbsent
            type: errortracking
            severity: s1
            alert_type: cause

        - alert: ErrortrackingLatencyHigh
          annotations:
            title: Errortracking latency high
            description: Errortracking p99 latency is higher than 5 seconds
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-errortracking/observability-errortracking
          expr: |
            histogram_quantile(0.99, sum(rate(http_requests_duration_seconds_bucket{job=~"^errortracking-api"}[5m])) by (le,path)) > 5
          for: 5m
          labels:
            alertname: ErrortrackingLatencyHigh
            type: errortracking
            severity: s3
            alert_type: cause

        - alert: ErrortrackingTooMany500s
          annotations:
            title: Number of 5xx is high
            description: More than 3% of all requests returned 5XX
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/d/observability-errortracking/observability-errortracking
          expr: |
            sum(rate(http_requests_total{job=~"^errortracking-api", code=~"5.*"}[1m])) by (uri)
              /
            sum(rate(http_requests_total{job=~"^errortracking-api"}[1m])) by (uri)
              > .03
          for: 5m
          labels:
            alertname: ErrortrackingTooMany500s
            type: errortracking
            severity: s2
            alert_type: cause
        # NOTE(prozlach) No 404s alert for now. ATM there are too many 404s in
        # productions compared to normal 200s to have meaningfull signal.
