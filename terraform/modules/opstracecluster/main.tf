resource "kubernetes_secret" "cluster" {
  metadata {
    name      = var.cluster_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    gitlab_oauth_client_id               = var.gitlab_oauth_client_id
    gitlab_oauth_client_secret           = var.gitlab_oauth_client_secret
    internal_endpoint_token              = var.internal_endpoint_token
    gitlab_traces_endpoint               = var.gitlab_traces_endpoint
    gitlab_traces_endpoint_private_token = var.gitlab_traces_endpoint_private_token
  }

  immutable = true
}

resource "kubernetes_secret" "cloudflare" {
  metadata {
    name      = var.cloudflare_secret_name
    namespace = var.cloudflare_secret_namespace
  }

  data = {
    "CF_API_TOKEN" = var.cloudflare_api_token
  }

  immutable = true
}

# create the same secret as above but for cert-manager usage
resource "kubernetes_namespace" "certmanager" {
  metadata {
    name = var.certmanager_deployment_namespace
    # this is a bit of a hack right now to ensure TF does not
    # complain about unknown labels which would be added by the
    # scheduler once Flux/Kustomize get to interact with this
    # object.
    labels = {
      "opstrace.com/name" : var.cluster_instance_name,
      "opstrace.com/namespace" : ""
    }
  }
}

resource "kubernetes_secret" "certmanager-cloudflare" {
  depends_on = [kubernetes_namespace.certmanager]
  metadata {
    name      = var.cloudflare_secret_name
    namespace = var.certmanager_deployment_namespace
  }

  data = {
    "CF_API_TOKEN" = var.cloudflare_api_token
  }

  immutable = true
}

# create_gcs_secret is used as conditional to setup this secret.
# This is meant to be in use by ClickHouse to access GCS.
resource "kubernetes_secret" "clickhouse-gcs-secret" {
  count = var.create_gcs_secret ? 1 : 0

  metadata {
    name      = var.gcs_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    "accessKeyID"     = var.gcs_access_key_id
    "accessKeySecret" = var.gcs_access_key_secret
  }

  immutable = true
}

# Bear in mind, setting up `disable_cluster_creation`
# on an already existing resource would actually delete
# it. This flag is only used to consume the module without
# creating a cluster initially.
resource "kubernetes_manifest" "cluster" {
  count    = var.disable_cluster_creation ? 0 : 1
  manifest = yamldecode(var.cluster_manifest)

  wait {
    condition {
      type   = "Ready"
      status = "True"
    }
  }

  field_manager {
    # force field manager conflicts to be overridden
    force_conflicts = true
  }
}

resource "kubernetes_secret" "clickhouse-cloud-dsn" {
  count = var.create_clickhouse_cloud_dsn_secret ? 1 : 0

  metadata {
    name      = var.clickhouse_cloud_dsn_secret_name
    namespace = var.cluster_secret_namespace
  }

  data = {
    "DSN" = var.clickhouse_cloud_dsn
  }

  immutable = true
}