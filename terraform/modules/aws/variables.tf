variable "cluster_endpoint" {
  type        = string
  description = "Cluster endpoint"
}

variable "cluster_ca_certificate" {
  type        = string
  description = "Cluster CA certificate"
}

variable "clickhouse_s3_bucket" {
  type        = string
  description = "AWS S3 bucket name to be used by Clickhouse for storage"
}

variable "clickhouse_s3_bucket_root" {
  type        = string
  description = "Root folder to be created under the S3 bucket; Needed because of S3 path conventions in Clickhouse"
  default     = "root/"
}

variable "clickhouse_s3_user_name" {
  type        = string
  description = "User name to be used by Clickhouse to access S3"
  default     = "clickhouse-s3-user"
}

variable "aws_region" {
  type        = string
  description = "AWS region name where the S3 bucket will be created"
}

variable "s3_secret_namespace" {
  type        = string
  default     = "default"
  description = "Namespace where the k8s secret for s3 user will be created"
}
