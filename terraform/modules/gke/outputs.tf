output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = "https://${data.google_container_cluster.primary.endpoint}"
  description = "GKE Cluster Host"
}

output "primary_node_pool_name" {
  value       = google_container_node_pool.primary_nodes_hcpu.name
  description = "GKE primary node pool name"
}

output "externaldns_service_account" {
  value       = google_service_account.externaldns_service_account.email
  description = "ExternalDNS service account"
}

output "certmanager_service_account" {
  value       = google_service_account.certmanager_service_account.email
  description = "CertManager service account"
}

output "kubeconfig_path" {
  value       = abspath(var.kubeconfig_path)
  description = "kubeconfig absolute path"
}

output "vpc_id" {
  value       = google_compute_network.vpc.id
  description = "google vpc ID"
}

output "vpc_name" {
  value       = google_compute_network.vpc.name
  description = "google vpc name"
}

output "nat_ips" {
  value       = join(",", formatlist("%s/32", flatten([for natip in google_compute_address.address : natip.address[*]])))
  description = "NAT IPs"
}
