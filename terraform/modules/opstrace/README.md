# Terraform module to setup opstrace

## Prerequisite

* Access to a GCP project via GOOGLE_APPLICATION_CREDENTIALS or via ADC (application default credentials).

  It is expected to have at least admin or greater privileges to the google project.

In current setup the GCP credentials are used to create a GKE instance and install opstrace components in it.
