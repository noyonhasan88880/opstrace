# TODO: create a secret for the docker hub credentials.
# When we do this, we'll also have to plumb all imagePullSecrets in the
# CRs (Cluster, Tenant) through to the container definitions.
# Some are plumbed through, but not all.
# https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1717

resource "kubernetes_config_map" "ratelimits" {
  metadata {
    name      = "ratelimits-config"
    namespace = "default"
  }

  data = {
    "limits.yaml" = var.ratelimits_config != "" ? var.ratelimits_config : file("${path.module}/../../../scheduler/config/rate-limits/limits.yaml")
  }
}

# -------------------------------------------------------------------------------------------
# Opstrace - scheduler, tenant-operator, clickhouse-operator
# -------------------------------------------------------------------------------------------


data "kustomization_build" "scheduler-crds" {
  path = "${path.module}/../../../scheduler/config/crd"
}

resource "kustomization_resource" "scheduler-crds" {
  for_each = data.kustomization_build.scheduler-crds.ids
  # sensitive() is a workaround for masking the massive CRD plan output
  manifest = sensitive(data.kustomization_build.scheduler-crds.manifests[each.value])
}

data "kustomization_build" "tenant-operator-crds" {
  path = "${path.module}/../../../tenant-operator/config/crd"
}

resource "kustomization_resource" "tenant-operator-crds" {
  for_each = data.kustomization_build.tenant-operator-crds.ids
  manifest = sensitive(data.kustomization_build.tenant-operator-crds.manifests[each.value])
}

data "kustomization_build" "clickhouse-operator-crds" {
  path = "${path.module}/../../../clickhouse-operator/config/crd"
}

resource "kustomization_resource" "clickhouse-operator-crds" {
  for_each = data.kustomization_build.clickhouse-operator-crds.ids
  manifest = sensitive(data.kustomization_build.clickhouse-operator-crds.manifests[each.value])
}

// Since our upstream kustomize manifests here override the scheduler image name
// using environment variables which TF doesn't really like, we just add a kustomize
// overlay here which patches the image name as was passed to this module.
data "kustomization_overlay" "scheduler" {
  resources = [
    "${path.module}/../../../scheduler/config/deploy",
  ]
  patches {
    patch = <<-EOF
      - op: replace
        path: /spec/template/spec/containers/0/image
        value: ${var.scheduler_image}
    EOF
    target {
      group   = "apps"
      version = "v1"
      kind    = "Deployment"
      name    = "scheduler-controller-manager"
    }
  }
}

resource "kustomization_resource" "scheduler" {
  for_each = data.kustomization_overlay.scheduler.ids
  manifest = data.kustomization_overlay.scheduler.manifests[each.value]

  depends_on = [
    kustomization_resource.scheduler-crds,
    kustomization_resource.tenant-operator-crds,
  ]
}

resource "google_container_node_pool" "clickhouse_nodes_hcpu" {
  count = var.provision_clickhouse_node_pool ? 1 : 0
  name  = "clickhouse-nodes-hcpu"

  cluster    = var.gke_cluster_name
  node_count = var.ch_nodepool_nodes_number

  management {
    auto_upgrade = var.release_channel == "UNSPECIFIED" ? false : true
    auto_repair  = var.pool_auto_repair
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = merge(var.global_labels, tomap({
      "gitlab/env"      = var.project_id,
      "gitlab/nodepool" = "clickhouse-nodes"
    }))

    taint = [{
      key    = "nodepool"
      value  = "clickhouse-nodes"
      effect = "NO_SCHEDULE"
    }]

    # preemptible  = true
    machine_type = var.ch_nodepool_machine_type
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  upgrade_settings {
    strategy = "BLUE_GREEN"
    blue_green_settings {
      standard_rollout_policy {
        # number of blue nodes to drain in a batch.
        # defaults to 1 node at a time.
        batch_node_count = var.ch_nodepool_blue_green_batch_node_count
        # soak time after each batch gets drained.
        # defaults to 2 hours.
        batch_soak_duration = var.ch_nodepool_blue_green_batch_soak_duration
      }
      # time needed after draining the entire blue pool.
      # after this period, the blue pool will be cleaned up.
      # defaults to 30 mins.
      node_pool_soak_duration = var.ch_nodepool_blue_green_node_pool_soak_duration
    }
  }
}
