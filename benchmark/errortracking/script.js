import { check } from 'k6'
import http from 'k6/http'

import payload from './payload.js'
import setup from './setup.js'

// This is a basic test for storing error events. It expects that the POST
// request to /projects/api/{PROJECT_ID}/store succeed with 200 status code.
export default function() {
    const res = http.post(setup.urls.store, JSON.stringify(payload()), {
        headers: setup.headers.store,
    })

    check(res, {
        'successful store': (r) => r.status === 200,
    });
}
