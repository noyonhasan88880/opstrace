package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	backup "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-backup"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

var (
	gcsRootPath       = common.GetEnv("GCS_PATH", "")
	clickHouseDSN     = common.GetEnv("CLICKHOUSE_DSN", "tcp://localhost:9000")
	logLevel          = common.GetEnv("LOG_LEVEL", "debug")
	gcsAccessKeyID    = common.GetEnv("ACCESS_KEY_ID", "")
	gcsAccessKey      = common.GetEnv("SECRET_ACCESS_KEY", "")
	incrementalBackup = common.GetEnv("INCREMENTAL_BACKUP", "true")
)

func main() {
	os.Exit(runBackupExecutor())
}

func runBackupExecutor() int {
	logger, _, config := common.SetupLogger(logLevel)
	// Sync has underlying bug where it errors out unnecessarily. See https://github.com/uber-go/zap/issues/880
	//nolint:errcheck
	defer logger.Sync()
	conn, err := openClickHouseConn(clickHouseDSN, config)
	if err != nil {
		logger.Errorf("clickhouse open conn: %v", err)
		return 1
	}

	backupExec := &executor{
		chConn:           conn,
		logger:           logger,
		createdBackups:   []*chBackup{},
		gcsRootPath:      gcsRootPath,
		gcsAccessKeyID:   gcsAccessKeyID,
		gcsAccessKey:     gcsAccessKey,
		lock:             sync.Mutex{},
		tables:           []string{},
		done:             make(chan struct{}, 1),
		backupsRemaining: atomic.Int64{},
	}

	if err := backupExec.validateParameters(); err != nil {
		logger.Error("validateParameters: ", err)
		return 1
	}

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	logger.Info("collecting tables")
	err = backupExec.collectTables(ctx)
	if err != nil {
		logger.Error("collectTables: ", err)
		return 1
	}

	logger.Infof("taking clickhouse backup for tables: %v", backupExec.tables)
	backupExec.createLoop(ctx)

	ticker := time.NewTicker(time.Second * 3)
	for loop := true; loop; {
		select {
		case <-backupExec.done:
			ticker.Stop()
			loop = false
		case <-ticker.C:
			logger.Info("checking backup status")
			backupExec.checkLoop(ctx)
		case <-ctx.Done():
			logger.Info(ctx.Err())
			stop()
			loop = false
		}
	}

	if len(backupExec.failedBackups) > 0 {
		logger.Error("one or more backups finished unsuccessfully, see log lines below:")
		for _, failed := range backupExec.failedBackups {
			//nolint:errcheck
			bts, _ := json.Marshal(failed)
			// To avoid additional escaping of json encoded messages, use the workaround
			logger.Desugar().Error("backup", zap.Any("item", json.RawMessage(bts)))
		}
		return 1
	}

	//nolint:errcheck
	bts, _ := json.Marshal(backupExec.createdBackups)
	logger.Desugar().Info("all backups are finished successfully: ", zap.Any("items", json.RawMessage(bts)))
	return 0
}

type executor struct {
	chConn             clickhouse.Conn
	logger             *zap.SugaredLogger
	createdBackups     []*chBackup
	gcsAccessKeyID     string
	gcsAccessKey       string
	gcsRootPath        string
	tables             []string
	backupsRemaining   atomic.Int64
	failedBackups      []*chBackup
	done               chan struct{}
	lock               sync.Mutex
	incrementalBackups bool
}

// chBackup is client side representation of a backup.
type chBackup struct {
	ID          string    `json:"id"`
	Table       string    `json:"table"`
	Incremental bool      `json:"incremental"`
	Path        string    `json:"path"`
	StartTime   time.Time `json:"start_time"`
	EndTime     time.Time `json:"end_time"`
	Status      string    `json:"status"`
}

type Result struct {
	backup *BackupResult
	err    error
}

// BackupResult holds clickhouse representation of a backup and is used for marshaling responses from the query.
type BackupResult struct {
	ID        string    `ch:"id" json:"id"`
	Status    string    `ch:"status" json:"status"`
	Error     string    `ch:"error" json:"error"`
	StartTime time.Time `ch:"start_time" json:"start_time"`
	EndTime   time.Time `ch:"end_time" json:"end_time"`

	// only needed to add more context to log lines
	Table string `json:"table"`
}

func (e *executor) validateParameters() error {
	if gcsRootPath == "" {
		return fmt.Errorf("missing arg: GCS_PATH")
	}
	if gcsAccessKey == "" {
		return fmt.Errorf("missing arg: ACCESS_KEY_ID")
	}
	if gcsAccessKeyID == "" {
		return fmt.Errorf("missing arg: SECRET_ACCESS_KEY")
	}
	if incrementalBackup == "" {
		return fmt.Errorf("missing arg: INCREMENTAL_BACKUP")
	}
	parsed, err := strconv.ParseBool(incrementalBackup)
	if err != nil {
		return fmt.Errorf("INCREMENTAL_BACKUP expected boolean got: %v", incrementalBackup)
	}
	e.incrementalBackups = parsed
	return nil
}

func (e *executor) collectTables(parentCtx context.Context) error {
	const query = `
SELECT database || '.' || table AS table
FROM system.tables 
WHERE database = ?  AND engine != 'Distributed';
`

	var (
		tableResults []struct {
			Name string `ch:"table"`
		}
		databases = []string{constants.TracingDatabaseName, constants.ErrorTrackingAPIDatabaseName}
	)

	ctx, cancel := context.WithTimeout(parentCtx, time.Second*3)
	defer cancel()
	for _, database := range databases {
		if err := e.chConn.Select(
			ctx,
			&tableResults,
			query,
			database,
		); err != nil {
			return fmt.Errorf("collecting tables: %w", err)
		}
		for _, table := range tableResults {
			e.tables = append(e.tables, table.Name)
		}
	}

	return nil
}

func (e *executor) createLoop(parentCtx context.Context) {
	results := make(chan *Result)

	wg := sync.WaitGroup{}
	for _, table := range e.tables {
		wg.Add(1)
		e.backupsRemaining.Add(1)
		table := table
		go e.createBackup(parentCtx, table, results, &wg)
	}

	go func() {
		// after all backups are created - closes the result channel
		wg.Wait()
		close(results)
	}()

	for res := range results {
		if res.err != nil {
			e.logger.Errorf("backup creation unsuccessful: %+v", res.backup)
			continue
		}
		//nolint:errcheck
		bts, _ := json.Marshal(res.backup)
		e.logger.Info("backup started successfully: ", string(bts))
	}
	e.logger.Info("all backups are started successfully")
}

const backupQueryTmpl = `
BACKUP TABLE %s TO S3(?, ?, ?) ASYNC;
`

const backupQueryIncTmpl = `
BACKUP TABLE %s TO S3(?, ?, ?) SETTINGS base_backup = S3(?, ?, ?) ASYNC;
`

func (e *executor) createBackup(parentCtx context.Context, table string, results chan<- *Result, wg *sync.WaitGroup) {
	defer wg.Done()

	path := ""
	basePath := ""
	query := ""
	if e.incrementalBackups {
		// In case of incremental backups - we assume existence of an earlier backup already from the current week.
		// This is to simplify the script code. A more thorough check would be to create a storage client and check if
		// full base backup exists or not.
		basePath = e.gcsRootPath + "/" + backup.BeginningOfWeek().Format(time.DateOnly) + "/full/" + table + ".full"
		path = e.gcsRootPath + "/" + backup.BeginningOfDay().Format(time.DateOnly) + "/incr/" + table + ".incr"
		query = fmt.Sprintf(backupQueryIncTmpl, table)
	} else {
		path = e.gcsRootPath + "/" + backup.BeginningOfWeek().Format(time.DateOnly) + "/full/" + table + ".full"
		query = fmt.Sprintf(backupQueryTmpl, table)
	}

	res, err := runBackupQuery(
		parentCtx,
		e.chConn,
		query, path, basePath, e.gcsAccessKey, e.gcsAccessKeyID, e.incrementalBackups)

	if err == nil {
		res.Table = table
		e.lock.Lock()
		e.createdBackups = append(e.createdBackups, &chBackup{
			ID:          res.ID,
			Table:       table,
			Incremental: e.incrementalBackups,
			Path:        path,
			Status:      res.Status,
		})
		e.lock.Unlock()
	}

	select {
	case <-parentCtx.Done():
		return
	case results <- &Result{backup: res, err: err}:
	}
}

func runBackupQuery(
	parentCtx context.Context,
	conn clickhouse.Conn,
	query, path, basePath, accessKey, accessKeyID string,
	incr bool,
) (*BackupResult, error) {
	var (
		rows driver.Rows
		err  error
	)

	ctx, cancel := context.WithTimeout(parentCtx, time.Second*3)
	defer cancel()
	if incr {
		rows, err = conn.Query(ctx, query, path, accessKeyID, accessKey, basePath, accessKeyID, accessKey)
	} else {
		rows, err = conn.Query(ctx, query, path, accessKeyID, accessKey)
	}

	if err != nil {
		return nil, fmt.Errorf("create backup query: %w", err)
	}

	res := &BackupResult{}
	for rows.Next() {
		if err = rows.ScanStruct(res); err != nil {
			return nil, fmt.Errorf("scanning backup result: %w", err)
		}
	}

	return res, nil
}

const (
	// statusCreating = "CREATING_BACKUP"
	statusCreated = "BACKUP_CREATED"
	statusFailed  = "BACKUP_FAILED"
)

func (e *executor) checkLoop(parentCtx context.Context) {
	results := make(chan *Result)

	wg := sync.WaitGroup{}
	for _, backup := range e.createdBackups {
		wg.Add(1)
		go e.checkBackup(parentCtx, backup, results, &wg)
	}

	go func() {
		// wait for all backup checks to finish - then close the result channel
		wg.Wait()
		close(results)
	}()

	for result := range results {
		if result.backup.Error != "" {
			e.logger.Errorf("backup observed an error: %+v", *result.backup)
		}
		if result.backup.Status == statusFailed || result.backup.Status == statusCreated {
			e.backupsRemaining.Add(-1)
			e.logger.Debugf("backup completed: %+v", *result.backup)
		}
	}

	if int(e.backupsRemaining.Load()) <= 0 {
		e.logger.Info("all backups are finished")
		e.done <- struct{}{}
		return
	}
}

const checkBackupQuery = `
SELECT id, status, error, start_time, end_time FROM clusterAllReplicas('{cluster}', system.backups) WHERE id = ? ;
`

func (e *executor) checkBackup(
	parentCtx context.Context,
	backup *chBackup,
	results chan<- *Result,
	wg *sync.WaitGroup,
) {
	defer wg.Done()
	if backup.Status == statusCreated {
		return
	}
	if backup.Status == statusFailed {
		return
	}

	updated, err := runBackupStatusQuery(parentCtx, e.chConn, backup.ID)
	res := &Result{backup: updated, err: err}

	if err == nil {
		// update the client side backup object
		backup.Status = updated.Status
		backup.StartTime = updated.StartTime
		backup.EndTime = updated.EndTime
	}

	if err == nil && updated.Status == statusFailed {
		updated.Table = backup.Table
		// If the backup came up as failed, mark the result as failed.
		res.err = fmt.Errorf("%s", updated.Error)
		// collect the failed backup object
		e.lock.Lock()
		e.failedBackups = append(e.failedBackups, backup)
		e.lock.Unlock()
	}

	select {
	case <-parentCtx.Done():
		return
	case results <- res:
	}
}

func runBackupStatusQuery(parentCtx context.Context, conn clickhouse.Conn, id string) (*BackupResult, error) {
	ctx, cancel := context.WithTimeout(parentCtx, time.Second*3)
	defer cancel()

	rows, err := conn.Query(ctx, checkBackupQuery, id)
	if err != nil {
		return nil, fmt.Errorf("check backup query: %w", err)
	}

	res := &BackupResult{}
	for rows.Next() {
		if err = rows.ScanStruct(res); err != nil {
			return nil, fmt.Errorf("scanning backup result: %w", err)
		}
	}
	return res, nil
}

func openClickHouseConn(clickHouseDSN string, config zap.Config) (clickhouse.Conn, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	if config.Level.Level() == zapcore.DebugLevel {
		dbOpts.Debug = true
	}

	dbOpts.ConnMaxLifetime = 5 * time.Minute
	dbOpts.MaxOpenConns = 10
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = conn.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("clickhouse ping: %w", err)
	}
	return conn, nil
}
