package backup

import "time"

// BeginningOfDay returns beginning of the current day.
func BeginningOfDay() time.Time {
	y, m, d := time.Now().Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.UTC)
}

// BeginningOfWeek returns the timestamp for beginning of current week.
func BeginningOfWeek() time.Time {
	t := BeginningOfDay()
	weekday := int(t.Weekday())
	weekStartDayInt := int(time.Sunday)

	if weekday < weekStartDayInt {
		weekday = weekday + 7 - weekStartDayInt
	} else {
		weekday -= weekStartDayInt
	}

	return t.AddDate(0, 0, -weekday)
}

// BeginningOfMonth return beginning of the current calendar month
func BeginningOfMonth() time.Time {
	y, m, _ := time.Now().Date()
	return time.Date(y, m, 1, 0, 0, 0, 0, time.UTC)
}
