//go:build linux

package fileobserver

import "github.com/rjeczalik/notify"

const (
	eventMask = notify.InCreate | notify.InMovedTo | notify.InModify | notify.InMoveSelf
)
