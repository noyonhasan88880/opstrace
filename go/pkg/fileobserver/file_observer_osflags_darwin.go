//go:build darwin && cgo

package fileobserver

import "github.com/rjeczalik/notify"

const (
	eventMask = notify.Create | notify.Write | notify.Rename | notify.FSEventsInodeMetaMod
)
