package testutils

import (
	"bytes"
	"sync"
)

type SyncBuffer struct {
	buffer bytes.Buffer
	mutex  sync.Mutex
}

func (s *SyncBuffer) Write(p []byte) (n int, err error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	//nolint:wrapcheck
	return s.buffer.Write(p)
}

func (s *SyncBuffer) String() string {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	return s.buffer.String()
}
