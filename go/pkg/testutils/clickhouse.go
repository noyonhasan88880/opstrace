package testutils

import (
	"context"
	"fmt"
	"path"
	"path/filepath"
	"runtime"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const (
	defaultUserName             = "default"
	defaultPassword             = "clickhouse"
	clickhouseTestContainerName = "test-clickhouse-server"
	clickhouseNativePort        = "9000/tcp"
	clickhouseHTTPPort          = "8123/tcp"
)

type ClickHouseServer struct {
	Host      string
	Port      string
	DSN       string
	Container testcontainers.Container `json:"-"`
}

func NewClickHouseServer(ctx context.Context) (ClickHouseServer, error) {
	chServer := ClickHouseServer{}
	provider, err := testcontainers.ProviderDocker.GetProvider()
	if err != nil {
		return chServer, fmt.Errorf("getting docker daemon: %w", err)
	}
	if err := provider.Health(ctx); err != nil {
		return chServer, fmt.Errorf("checking docker daemon: %w", err)
	}

	//nolint:dogsled
	_, b, _, _ := runtime.Caller(0)
	basePath := filepath.Dir(b)

	containerRequest := testcontainers.ContainerRequest{
		Image: getClickHouseImage(),
		Name:  clickhouseTestContainerName,
		ExposedPorts: []string{
			clickhouseNativePort,
			clickhouseHTTPPort,
		},
		WaitingFor: wait.ForAll(
			wait.ForLog("Ready for connections").WithStartupTimeout(120*time.Second),
			wait.ForHTTP("/").WithPort(clickhouseHTTPPort).WithStartupTimeout(120*time.Second),
		).WithDeadline(120 * time.Second),
		Mounts: []testcontainers.ContainerMount{
			testcontainers.BindMount(
				path.Join(basePath, "./clickhouse-resources/custom.xml"),
				"/etc/clickhouse-server/config.d/custom.xml",
			),
			testcontainers.BindMount(
				path.Join(basePath, "./clickhouse-resources/admin.xml"),
				"/etc/clickhouse-server/users.d/admin.xml",
			),
		},
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: containerRequest,
		Started:          true,
	})
	if err != nil {
		return chServer, fmt.Errorf("starting CH container: %w", err)
	}

	chServer.Container = container
	return chServer, nil
}

func getClickHouseImage() string {
	return constants.OpstraceImages().ClickHouseImage
}

func (c *ClickHouseServer) GetDSN(ctx context.Context) (string, error) {
	// if we have already computed the DSN during this object's
	// lifetime, return the value early
	if c.DSN != "" {
		return c.DSN, nil
	}
	host, err := c.Container.Host(ctx)
	if err != nil {
		return "", fmt.Errorf("getting mapped host: %w", err)
	}
	port, err := c.Container.MappedPort(ctx, "9000")
	if err != nil {
		return "", fmt.Errorf("getting mapped port: %w", err)
	}

	dsn := fmt.Sprintf("clickhouse://%s:%s@%s:%s",
		defaultUserName,
		defaultPassword,
		host,
		port.Port(),
	)
	c.Host = host
	c.Port = port.Port()
	c.DSN = dsn
	return c.DSN, nil
}

func (c *ClickHouseServer) GetConnection() (driver.Conn, error) {
	conn, err := clickhouse.Open(&clickhouse.Options{
		Addr: []string{fmt.Sprintf("%s:%s", c.Host, c.Port)},
		Auth: clickhouse.Auth{
			Database: "default",
			Username: defaultUserName,
			Password: defaultPassword,
		},
		DialTimeout: 10 * time.Second,
	})
	if err != nil {
		return nil, fmt.Errorf("opening clickhouse connection: %w", err)
	}
	return conn, nil
}

func (c *ClickHouseServer) CreateDatabase(ctx context.Context, dbName string) error {
	conn, err := c.GetConnection()
	if err != nil {
		return err
	}
	if err := conn.Exec(
		ctx,
		fmt.Sprintf("CREATE DATABASE IF NOT EXISTS `%s`", dbName),
	); err != nil {
		return fmt.Errorf("creating database %s: %w", dbName, err)
	}
	return nil
}

func (c *ClickHouseServer) Run(ctx context.Context, sqlString string) error {
	conn, err := c.GetConnection()
	if err != nil {
		return err
	}
	if err := conn.Exec(ctx, sqlString); err != nil {
		return fmt.Errorf("executing sql: %w", err)
	}
	return nil
}
