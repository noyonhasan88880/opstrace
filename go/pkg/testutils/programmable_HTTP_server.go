package testutils

import (
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	uberzap "go.uber.org/zap"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

type ReplyT struct {
	Status int
	Body   string
	Func   func(*http.Request) (int, string)
}

type ProgrammableHTTPServer struct {
	srv               *http.Server
	srvSocket         string
	srvStartStopMutex *sync.Mutex
	dataMutex         *sync.Mutex
	wg                *sync.WaitGroup

	credentials string

	logger *uberzap.SugaredLogger

	requests  map[string][]*http.Request
	responses map[string]ReplyT

	initialized bool
}

func NewProgrammableHTTPServer(
	logger *uberzap.SugaredLogger,
	credentials string,
) *ProgrammableHTTPServer {
	res := new(ProgrammableHTTPServer)
	res.wg = new(sync.WaitGroup)
	res.srvStartStopMutex = new(sync.Mutex)
	res.dataMutex = new(sync.Mutex)

	res.logger = logger

	res.credentials = credentials

	res.requests = make(map[string][]*http.Request)
	res.responses = make(map[string]ReplyT)

	return res
}

func (phs *ProgrammableHTTPServer) Start() {
	Expect(phs.initialized).To(BeFalse(), "this programmableHTTPServer has already been initialized")

	listener, err := net.Listen("tcp", "127.0.0.1:0")
	Expect(err).ToNot(HaveOccurred())

	if phs.credentials != "" {
		phs.srvSocket = fmt.Sprintf("http://%s@%s", phs.credentials, listener.Addr().String())
	} else {
		phs.srvSocket = fmt.Sprintf("http://%s", listener.Addr().String())
	}
	phs.logger.Infow("phs socket created", "address", phs.srvSocket)
	mux := http.NewServeMux()
	mux.HandleFunc("/", phs.httpHandleFunc)

	phs.srv = &http.Server{
		Addr: listener.Addr().String(),
		// Some arbitrary values
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        mux,
	}

	phs.wg.Add(1)
	phs.srvStartStopMutex.Lock()
	go func() {
		defer GinkgoRecover()

		defer phs.wg.Done()

		phs.logger.Info("starting programmable HTTPServer")
		phs.srvStartStopMutex.Unlock()
		err := phs.srv.Serve(listener)
		phs.logger.Infow("http srv terminated", "error", err)
	}()

	phs.srvStartStopMutex.Lock()
	phs.initialized = true
}

func (phs *ProgrammableHTTPServer) SetResponse(method, path string, status int, body string) {
	phs.dataMutex.Lock()
	defer phs.dataMutex.Unlock()

	requestKey := phs.generateRequestKey(method, path)
	phs.responses[requestKey] = ReplyT{
		Status: status,
		Body:   body,
	}
}

func (phs *ProgrammableHTTPServer) SetResponseFunc(method, path string, f func(*http.Request) (int, string)) {
	phs.dataMutex.Lock()
	defer phs.dataMutex.Unlock()

	requestKey := phs.generateRequestKey(method, path)
	phs.responses[requestKey] = ReplyT{
		Func: f,
	}
}

func (phs *ProgrammableHTTPServer) GetRequests(method, path string) []*http.Request {
	phs.dataMutex.Lock()
	defer phs.dataMutex.Unlock()

	requestKey := phs.generateRequestKey(method, path)

	//  NOTE(prozlach): Append-only slice, not sure if it makes sense to copy it
	//  here.
	return phs.requests[requestKey]
}

func (phs *ProgrammableHTTPServer) Stop() {
	defer phs.srvStartStopMutex.Unlock()

	phs.logger.Info("stopping programmable HTTPServer")
	err := phs.srv.Close()
	phs.wg.Wait()
	Expect(err).ToNot(HaveOccurred(), "http srv terminated with an error")
}

func (phs *ProgrammableHTTPServer) ServerURL() string {
	return phs.srvSocket
}

func (phs *ProgrammableHTTPServer) generateRequestKey(method, path string) string {
	return fmt.Sprintf("%s %s", method, path)
}

func (phs *ProgrammableHTTPServer) httpHandleFunc(w http.ResponseWriter, r *http.Request) {
	defer GinkgoRecover()

	phs.wg.Add(1)
	defer phs.wg.Done()

	requestKey := phs.generateRequestKey(r.Method, r.URL.Path)
	phs.logger.Infow("phs request received", "requestKey", requestKey)

	phs.dataMutex.Lock()
	if _, ok := phs.requests[requestKey]; !ok {
		phs.requests[requestKey] = make([]*http.Request, 0, 1)
	}
	phs.requests[requestKey] = append(phs.requests[requestKey], r.Clone(r.Context()))

	reply, replyOk := phs.responses[requestKey]
	phs.dataMutex.Unlock()

	var status int
	var body string

	if !replyOk {
		phs.logger.Infow("reply data has not been found", "requestKey", requestKey)
		status = 500
		body = "reply data has not been found"
	} else {
		if reply.Func != nil {
			status, body = reply.Func(r)
		} else {
			status, body = reply.Status, reply.Body
		}
	}

	phs.logger.Infow("phs reply sent", "status", status, "body", body)

	w.WriteHeader(status)
	_, err := w.Write([]byte(body))
	Expect(err).ToNot(HaveOccurred(), "phs failed to write to body")
}
