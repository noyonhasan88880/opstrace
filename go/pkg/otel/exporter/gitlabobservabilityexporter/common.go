package gitlabobservabilityexporter

import (
	"go.opentelemetry.io/collector/pdata/pcommon"
)

func attributesToMap(attributes pcommon.Map) map[string]string {
	m := make(map[string]string, attributes.Len())
	attributes.Range(func(k string, v pcommon.Value) bool {
		m[k] = v.AsString()
		return true
	})
	return m
}

const ProjectIDHeader = "gitlab.target_project_id"

func getProjectID(attributes map[string]string) string {
	projectID := "unknown"
	if _, ok := attributes[ProjectIDHeader]; ok {
		projectID = attributes[ProjectIDHeader]
	}
	return projectID
}
