package errortracking

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQueryBuilder(t *testing.T) {
	b := &queryBuilder{}

	for _, tc := range []struct {
		name     string
		sql      string
		params   []interface{}
		expected string
	}{
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = 1",
			params:   []interface{}{},
			expected: "SELECT * FROM table WHERE thing = 1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ?",
			params:   []interface{}{1},
			expected: "SELECT * FROM table WHERE thing = $1",
		},
		{
			name:     "should build a simple query",
			sql:      " WHERE thing = ? AND otherthing = ?",
			params:   []interface{}{1, 2},
			expected: "SELECT * FROM table WHERE thing = $1 AND otherthing = $2",
		},
	} {
		t.Log(tc.name)

		sql := "SELECT * FROM table"
		b.reset(sql)

		assert.Equal(t, b.sql, sql)
		assert.Len(t, b.args, 0)

		b.build(tc.sql, tc.params...)
		assert.EqualValues(t, tc.params, b.args)
		assert.Equal(t, tc.expected, b.sql)
	}
}
