package models

import (
	"fmt"
	"time"
)

// ErrorTrackingIgnoredError maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingIgnoredError struct {
	ProjectID   uint64    `ch:"project_id"`
	Fingerprint uint32    `ch:"fingerprint"`
	UserID      uint64    `ch:"user_id"`
	UpdatedAt   time.Time `ch:"updated_at"`
}

func (e ErrorTrackingIgnoredError) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO gl_error_tracking_ignored_errors VALUES (%d,%d,%d,%s)",
		e.ProjectID,
		e.Fingerprint,
		e.UserID,
		formatTime(e.UpdatedAt, tz),
	)
}
