package models

import (
	"encoding/hex"
	"fmt"
	"time"

	"github.com/zeebo/blake3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

// ErrorTrackingMessageEvent maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingMessageEvent struct {
	EventID     string    `json:"event_id,omitempty" ch:"event_id"`
	ProjectID   uint64    `json:"project_id" ch:"project_id"`
	Timestamp   time.Time `json:"timestamp" ch:"timestamp"`
	IsDeleted   uint8     `ch:"is_deleted"`
	Fingerprint []byte    `json:"fingerprint" ch:"fingerprint"`

	Environment string `json:"environment,omitempty" ch:"environment"`
	Level       string `json:"level,omitempty" ch:"level"`
	Message     string `json:"message,omitempty" ch:"message"`
	Actor       string `ch:"actor"`
	Platform    string `json:"platform,omitempty" ch:"platform"`
	Release     string `json:"release,omitempty" ch:"release"`
	ServerName  string `json:"server_name,omitempty" ch:"server_name"`
	// sdk and stacktrace_frames are left out for now
	Payload string `json:"payload,omitempty" ch:"payload"`
}

// NewErrorTrackingMessageEvent is a helper that returns an ErrorTrackingMessageEvent
// from the given parameters and calculates a fingerprint using a 64-bit xxHash
// algorithm form the message event message and platform.
func NewErrorTrackingMessageEvent(projectID uint64, e *types.Event, payload []byte) *ErrorTrackingMessageEvent {
	fingerprint := blake3.Sum256([]byte(fmt.Sprintf("%s|%s|%s", e.Message, e.MessageActor(payload), e.Platform)))
	return &ErrorTrackingMessageEvent{
		EventID:     e.EventID,
		ProjectID:   projectID,
		Timestamp:   e.Timestamp,
		IsDeleted:   uint8(0),
		Fingerprint: fingerprint[:],
		Environment: e.Environment,
		Level:       e.Level,
		Message:     e.Message,
		Actor:       e.MessageActor(payload),
		Platform:    e.Platform,
		Release:     e.Release,
		ServerName:  e.ServerName,
		Payload:     string(payload),
	}
}

func (e ErrorTrackingMessageEvent) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf(`INSERT INTO gl_error_tracking_message_events (
		event_id,
		project_id,
		timestamp,
		is_deleted,
		fingerprint,
		environment,
		level,
		message,
		actor,
		platform,
		release,
		server_name,
		payload
	) VALUES (%s,%d,%s,%d,unhex(%s),%s,%s,%s,%s,%s,%s,%s,%s)`,
		quote(e.EventID),
		e.ProjectID,
		formatTime(e.Timestamp, tz),
		e.IsDeleted,
		quote(hex.EncodeToString(e.Fingerprint)),
		quote(e.Environment),
		quote(e.Level),
		quote(e.Message),
		quote(e.Actor),
		quote(e.Platform),
		quote(e.Release),
		quote(e.ServerName),
		quote(e.Payload),
	)
}
