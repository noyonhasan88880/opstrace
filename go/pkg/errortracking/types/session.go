package types

import (
	"encoding/json"
	"fmt"
	"time"
)

// Use a map for finding keys fast.
var statusValues = map[string]uint8{
	"ok":       uint8(1),
	"exited":   uint8(2),
	"crashed":  uint8(3),
	"abnormal": uint8(4),
}

type Session struct {
	SessionID  string            `json:"sid,omitempty"`
	UserID     string            `json:"did,omitempty"`
	Init       uint8             `json:"init,omitempty"`
	Started    time.Time         `json:"started,omitempty"`
	OccuredAt  time.Time         `json:"timestamp,omitempty"`
	Duration   float64           `json:"duration,omitempty"`
	Status     string            `json:"status,omitempty"`
	Attributes SessionAttributes `json:"attrs,omitempty"`
}

type SessionAttributes struct {
	Release     string `json:"release,omitempty"`
	Environment string `json:"environment,omitempty"`
}

func NewSessionFrom(payload []byte) (*Session, error) {
	s := &Session{}
	if err := s.UnmarshalJSON(payload); err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Session) ValidateStatus() error {
	_, ok := statusValues[s.Status]
	if ok {
		return nil
	}
	return fmt.Errorf("session status unknown: %s", s.Status)
}

func (s *Session) Validate() error {
	if s.SessionID == "" || s.Attributes.Release == "" {
		return fmt.Errorf("validation failed: session is missing required filed Release or SessionID")
	}

	if err := s.ValidateStatus(); err != nil {
		return fmt.Errorf("validation failed: %w", err)
	}

	return nil
}

//nolint:cyclop
func (s *Session) UnmarshalJSON(data []byte) error {
	var objMap map[string]*json.RawMessage

	err := json.Unmarshal(data, &objMap)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event into objMap: %w", err)
	}

	for key, val := range objMap {
		if val == nil {
			continue
		}
		switch key {
		case "sid":
			err = json.Unmarshal(*val, &s.SessionID)
		case "did":
			// Rust Sentry SDK returns nil instead of empty string
			err = json.Unmarshal(*val, &s.UserID)
		case "init":
			var init bool
			err = json.Unmarshal(*val, &init)
			if init {
				s.Init = uint8(1)
			} else {
				s.Init = uint8(0)
			}
		case "duration":
			err = json.Unmarshal(*val, &s.Duration)
		case "status":
			err = json.Unmarshal(*val, &s.Status)
		case "started":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			s.Started = ts.Time()
		case "timestamp":
			var ts UnixOrRFC3339Time
			err = json.Unmarshal(*val, &ts)
			if err != nil {
				//nolint:wrapcheck
				return err
			}
			s.OccuredAt = ts.Time()
		case "attrs":
			err = json.Unmarshal(*val, &s.Attributes)
		}
		if err != nil {
			//nolint:wrapcheck
			return err
		}
	}

	// Some SDKs do not set the timestamp field (OccuredAt)
	if s.OccuredAt.IsZero() {
		s.OccuredAt = time.Now().UTC()
	}

	return nil
}
