package errortracking

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestBuildTimeSeriesQuery(t *testing.T) {
	refTime := time.Now()
	for _, tc := range []struct {
		name          string
		statsPeriod   string
		expectedQuery string
		expectedArgs  []interface{}
	}{
		{
			name:        "building a time window for the last 15m",
			statsPeriod: "15m",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfMinute(toDateTime('%d')) AS start,
    toStartOfMinute(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 60))) AS t
`,
				refTime.Add(-1*15*time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 30m",
			statsPeriod: "30m",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfMinute(toDateTime('%d')) AS start,
    toStartOfMinute(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 60))) AS t
`,
				refTime.Add(-1*30*time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 1h",
			statsPeriod: "1h",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfMinute(toDateTime('%d')) AS start,
    toStartOfMinute(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 60))) AS t
`,
				refTime.Add(-1*60*time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 24h",
			statsPeriod: "24h",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfHour(toDateTime('%d')) AS start,
    toStartOfHour(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 3600))) AS t
`,
				refTime.Add(-1*24*time.Hour).Unix(),
				refTime.Add(time.Hour).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 7d",
			statsPeriod: "7d",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfDay(toDateTime('%d')) AS start,
    toStartOfDay(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 86400))) AS t
`,
				refTime.Add(-1*7*24*time.Hour).Unix(),
				refTime.Add(24*time.Hour).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 14d",
			statsPeriod: "14d",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfDay(toDateTime('%d')) AS start,
    toStartOfDay(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 86400))) AS t
`,
				refTime.Add(-1*14*24*time.Hour).Unix(),
				refTime.Add(24*time.Hour).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for the last 30d",
			statsPeriod: "30d",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfDay(toDateTime('%d')) AS start,
    toStartOfDay(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 86400))) AS t
`,
				refTime.Add(-1*30*24*time.Hour).Unix(),
				refTime.Add(24*time.Hour).Unix(),
			),
			expectedArgs: []interface{}{},
		},
		{
			name:        "building a time window for an unspecified period should return data for 24h",
			statsPeriod: "fooh",
			expectedQuery: fmt.Sprintf(`
WITH
    toStartOfHour(toDateTime('%d')) AS start,
    toStartOfHour(toDateTime('%d')) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), 3600))) AS t
`,
				refTime.Add(-1*24*time.Hour).Unix(),
				refTime.Add(time.Hour).Unix(),
			),
			expectedArgs: []interface{}{},
		},
	} {
		t.Log(tc.name)
		query, args := buildTimeSeriesQuery(refTime, tc.statsPeriod)
		assert.Equal(t, tc.expectedQuery, query)
		assert.EqualValues(t, tc.expectedArgs, args)
	}
}

func TestBuildErrorStatsQuery(t *testing.T) {
	refTime := time.Now()
	fingerprints := []uint32{233069371, 1805903340} // random values
	for _, tc := range []struct {
		name          string
		statsPeriod   string
		expectedQuery string
		expectedArgs  []interface{}
	}{
		{
			name:        "querying error events for the last 15m",
			statsPeriod: "15m",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 15 * time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			},
		},
		{
			name:        "querying error events for the last 30m",
			statsPeriod: "30m",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 30 * time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			},
		},
		{
			name:        "query error events for the last 1h",
			statsPeriod: "1h",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 60 * time.Minute).Unix(),
				refTime.Add(time.Minute).Unix(),
			},
		},
		{
			name:        "querying error events for the last 1d",
			statsPeriod: "1d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfHour(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 24 * time.Hour).Unix(),
				refTime.Add(time.Hour).Unix(),
			},
		},
		{
			name:        "querying error events for the last 7d",
			statsPeriod: "7d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 7 * 24 * time.Hour).Unix(),
				refTime.Add(24 * time.Hour).Unix(),
			},
		},
		{
			name:        "querying error events for the last 14d",
			statsPeriod: "14d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 14 * 24 * time.Hour).Unix(),
				refTime.Add(24 * time.Hour).Unix(),
			},
		},
		{
			name:        "querying error events for the last 30d",
			statsPeriod: "30d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 30 * 24 * time.Hour).Unix(),
				refTime.Add(24 * time.Hour).Unix(),
			},
		},
		{
			name:        "querying error events for an unspecified period should return data for 24h",
			statsPeriod: "fooh",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfHour(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
 WHERE fingerprint IN $1 AND t >= $2 AND t <= $3 GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: []interface{}{
				fingerprints,
				refTime.Add(-1 * 24 * time.Hour).Unix(),
				refTime.Add(time.Hour).Unix(),
			},
		},
	} {
		t.Log(tc.name)
		query, args := buildErrorStatsQuery(fingerprints, refTime, tc.statsPeriod)
		assert.Equal(t, tc.expectedQuery, query)
		assert.EqualValues(t, tc.expectedArgs, args)
	}
}
