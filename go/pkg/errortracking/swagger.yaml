swagger: "2.0"
info:
  description: "This schema describes the API endpoints for the error tracking feature"
  version: "0.0.1"
  title: "Error Tracking REST API"
host: "localhost"
basePath: "/errortracking/api/v1"
tags:
- name: "errors"
  description: "Related to the error objects"
- name: "events"
  description: "Related to the error event objects"
- name: "projects"
  description: "Related to the project objects"
- name: "messages"
  description: "Related to the message objects"
schemes:
- "https"
- "http"
securityDefinitions:
  internalToken:
    type: apiKey
    in: header
    name: "Gitlab-Error-Tracking-Token"
security:
  - internalToken: []
paths:
  /projects/{projectId}/errors:
    get:
      tags:
      - "errors"
      summary: "List of errors"
      description: ""
      operationId: "listErrors"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      - name: "sort"
        in: "query"
        type: "string"
        default: "last_seen_desc"
        enum:
        - "last_seen_desc"
        - "first_seen_desc"
        - "frequency_desc"
      - name: "status"
        in: "query"
        type: "string"
        default: "unresolved"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
      - name: "query"
        in: "query"
        type: "string"
      - name: "cursor"
        in: "query"
        type: "string"
        description: "Base64 encoded information for pagination"
      - name: "limit"
        description: "Number of entries to return"
        in: "query"
        type: "integer"
        default: 20
        minimum: 1
        maximum: 100
      - name: "statsPeriod"
        in: "query"
        type: "string"
        default: "24h"
        enum:
        - "15m"
        - "30m"
        - "1h"
        - "24h"
        - "7d"
        - "14d"
        - "30d"
      - name: "queryPeriod"
        in: "query"
        type: "string"
        default: "30d"
        enum:
        - "15m"
        - "30m"
        - "1h"
        - "1d"
        - "7d"
        - "14d"
        - "30d"
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Error"
              # this matches the limit enforced via request parameters
              maxLength: 100
        "404":
          description: "Error not found"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"
  /projects/{projectId}/errors/{fingerprint}:
    get:
      tags:
      - "errors"
      summary: "Get information about the error"
      description: ""
      operationId: "getError"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error that needs to be updated deleted"
        required: true
        type: "integer"
        format: "uint32"
      responses:
        "200":
          description: "Success"
          schema:
            $ref: "#/definitions/Error"
          headers:
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
        "404":
          description: "Error not found"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"
    put:
      tags:
      - "errors"
      summary: "Update the status of the error"
      description: ""
      operationId: "updateError"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error that needs to be updated deleted"
        required: true
        type: "integer"
        format: "uint32"
      - in: "body"
        name: "body"
        description: "Error update object with the new values"
        required: true
        schema:
          $ref: "#/definitions/ErrorUpdatePayload"
      responses:
        "404":
          description: "Error not found"
        "500":
          description: "Internal error"
        "200":
          description: "Success"
          schema:
            $ref: "#/definitions/Error"
          headers:
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
  #
  # Ingestion endpoints required by the Sentry API
  #
  # There is a reason why we have such uncommon path. We depend on a client side
  # error tracking software which modifies URL for its own reasons.
  #
  # When we give user a URL like this:
  #   HOST/errortracking/api/v1/projects/123
  #
  # Then error tracking software will convert it like this:
  #   HOST/errortracking/api/v1/projects/api/123/store/
  #   HOST/errortracking/api/v1/projects/api/123/envelope/
  #
  /projects/api/{projectId}/store:
    post:
      tags:
      - "events"
      summary: "Ingestion endpoint for error events sent from client SDKs"
      description: ""
      #
      # operationId is an optional unique string used to identify an operation.
      # If provided, these IDs must be unique among all operations described in
      # your API.
      #
      # Don't set operation Id so we can $ref this for the envelope endpoint
      # operationId: "createEvent". This means the code generator will give pick
      # a name for the method, example for the go code:
      #     events.NewPostProjectsProjectIDEnvelope
      #     events.NewPostProjectsProjectIDStore
      #
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      responses:
        # Some sentry clients sdk require status 200 OK to work correctly.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/343531.
        "200":
          description: "Accepted. Event will be created async"
          schema:
            $ref: "#/definitions/ErrorEvent"
          headers:
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
        "400":
          description: "Bad request"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"
  /projects/api/{projectId}/envelope:
    post:
      tags:
      - "events"
      summary: "Ingestion endpoint for error events sent from client SDKs"
      description: ""
      #
      # operationId is an optional unique string used to identify an operation.
      # If provided, these IDs must be unique among all operations described in
      # your API.
      #
      # Don't set operation Id so we can $ref this for the envelope endpoint
      # operationId: "createEvent". This means the code generator will give pick
      # a name for the method, example for the go code:
      #     events.NewPostProjectsProjectIDEnvelope
      #     events.NewPostProjectsProjectIDStore
      #
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      responses:
        # Some sentry clients sdk require status 200 OK to work correctly.
        # See https://gitlab.com/gitlab-org/gitlab/-/issues/343531.
        "200":
          description: "Accepted. Event will be created async"
          schema:
            $ref: "#/definitions/ErrorEvent"
          headers:
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
        "400":
          description: "Bad request"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"

  /projects/{projectId}/errors/{fingerprint}/events:
    get:
      tags:
      - "errors"
      - "events"
      summary: "Get information about the events related to the error"
      description: ""
      operationId: "listEvents"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the error was created"
        required: true
        type: "integer"
        format: "uint64"
      - name: "fingerprint"
        in: "path"
        description: "ID of the error within the project"
        required: true
        type: "integer"
        format: "uint32"
      - name: "sort"
        in: "query"
        type: "string"
        default: "occurred_at_asc"
        enum:
        - "occurred_at_asc"
        - "occurred_at_desc"
      - name: "cursor"
        in: "query"
        type: "string"
        description: "Base64 encoded information for pagination"
      - name: "limit"
        description: "Number of entries to return"
        in: "query"
        type: "integer"
        default: 20
        minimum: 1
        maximum: 100
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
          schema:
            type: "array"
            items:
              $ref: "#/definitions/ErrorEvent"
              # matches the limit in request parameters
              maxLength: 100
        "404":
          description: "Error not found"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"
  /projects/{id}:
    delete:
      summary: "Deletes all project related data. Mostly for testing purposes and later for production to clean updeleted projects."
      description: ""
      operationId: "deleteProject"
      tags:
      - "projects"
      parameters:
      - name: "id"
        in: "path"
        description: "ID of the project"
        required: true
        type: "integer"
        format: "uint64"
      responses:
        "201":
          description: "Success, delete will happen async."
          headers:
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
        "404":
          description: "Error not found"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"
  # This should be matched to /groupID/projectID/issues but,
  # it seems that the sentry plugin sends projectID in query params. So it matches that.
  /api/0/organizations/{groupId}/issues/:
    get:
      tags:
        - "errorsV2"
      summary: "List of errors(V2)"
      description: ""
      operationId: "listErrorsV2"
      parameters:
        - name: "project"
          in: "query"
          description: "ID of the project where the error was created"
          required: true
          type: array
          items:
            type: "integer"
            format: "uint64"
          collectionFormat: multi
        - name: "groupId"
          in: "path"
          description: "ID of the group"
          required: true
          type: "integer"
          format: "uint64"
        - name: "status"
          in: "query"
          type: "string"
          default: "unresolved"
          enum:
            - "unresolved"
            - "resolved"
            - "ignored"
        - name: "query"
          in: "query"
          type: "string"
#        - name: "statsPeriod" # until the sentry datasource is updated to use this query parameter
#          in: "query"
#          type: "string"
#          description: "Optional stat period (can be one of '24h', '14d', and '')"
        - name: "start"
          in: "query"
          type: "string"
          description: "Optional start of the stat period in format 2006-01-02T15:04:05"
        - name: "end"
          in: "query"
          type: "string"
          description: "Optional end of the stat period in format 2006-01-02T15:04:05"
        - name: "environment"
          in: "query"
          type: "string"
          description: ""
        - name: "limit"
          description: "Number of entries to return"
          in: "query"
          type: "integer"
          default: 20
          minimum: 1
          maximum: 10000
        - name: "sort"
          in: "query"
          type: "string"
          description: "Optional sorting column of the entries"
          default: "date"
          enum:
            - "date" # last_seen_at
            - "new" # first_seen_at
            - "priority" # unimplemented
            - "freq" # error_count
            - "user" # approximated_user_count
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
          schema:
            type: "array"
            items:
              $ref: "#/definitions/ErrorV2"
              # matches the limit parameter in request parameters
              maxLength: 10000
        "404":
          description: "Error not found"
        "500":
          description: "Internal error"
  /api/0/organizations/{groupId}/projects/:
    get:
      tags:
        - "errorsV2"
      summary: "List of projects"
      description: ""
      operationId: "listProjects"
      parameters:
        - name: "groupId"
          in: "path"
          description: "ID of the group"
          required: true
          type: "integer"
          format: "uint64"
          # TODO: - add more query parameters
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Project"
              # Assumed length for enforcing the limits
              maxLength: 100
        "401":
          description: "Unauthorized"
        "404":
          description: "Project not found"
        "500":
          description: "Internal error"
  /api/0/organizations/{groupId}/stats_v2:
    get:
      tags:
        - "errorsV2"
      summary: "Stats of events received for the group"
      description: ""
      operationId: "getStatsV2"
      parameters:
        - name: "groupId"
          in: "path"
          description: "ID of the group"
          required: true
          type: "integer"
          format: "uint64"
      # TODO: - add more query parameters
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
          schema:
            type: "array"
            items:
              $ref: "#/definitions/StatsObject"
              # Assumed length to enforce limit
              maxLength: 100
        "404":
          description: "Project not found"
        "500":
          description: "Internal error"
  /projects/{projectId}/messages:
    get:
      tags:
      - "messages"
      summary: "List of messages"
      description: ""
      operationId: "listMessages"
      parameters:
      - name: "projectId"
        in: "path"
        description: "ID of the project where the message was created"
        required: true
        type: "integer"
        format: "uint64"
      - name: "limit"
        description: "Number of entries to return"
        in: "query"
        type: "integer"
        default: 20
        minimum: 1
        maximum: 100
      responses:
        "200":
          description: "Success"
          headers:
            Link:
              type: string
              description: Link header containing pagination information (previous and next pages). https://docs.gitlab.com/ee/api/#pagination-link-header
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
            X-Sentry-Rate-Limit-Remaining:
              description: The number of requests this caller has left on this endpoint within the current window.
              type: integer
          schema:
            type: "array"
            items:
              $ref: "#/definitions/MessageEvent"
              # Matches the limit in request parameters
              maxLength: 100
        "404":
          description: "Error not found"
        "429":
          description: "Resource limits exceeded"
          headers:
            X-Sentry-Rate-Limits:
              description: Detailed list of all rate limits that apply per data category, with seconds remaining untill they reset
              type: string
            Retry-After:
              description: Number of seconds after which requests will be permitted again
              type: integer
            X-Sentry-Rate-Limit-Limit:
              description: The maximum number of requests allowed within the window.
              type: integer
            X-Sentry-Rate-Limit-Reset:
              description: The time when the next rate limit window begins and the count resets, measured in UTC seconds from epoch.
              type: integer
        "500":
          description: "Internal error"

definitions:
  Error:
    type: "object"
    properties:
      fingerprint:
        type: "integer"
        format: "uint32"
      project_id:
        type: "integer"
        format: "uint64"
      name:
        type: "string"
        example: "ActionView::MissingTemplate"
      description:
        type: "string"
        example: "Missing template posts/edit"
      actor:
        type: "string"
        example: "PostsController#edit"
      event_count:
        type: "integer"
        format: "uint64"
      approximated_user_count:
        type: "integer"
        format: "uint64"
      last_seen_at:
        type: "string"
        format: "date-time"
      first_seen_at:
        type: "string"
        format: "date-time"
      status:
        type: "string"
        description: "Status of the error"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
      stats:
        type: "object"
        $ref: "#/definitions/ErrorStats"
  ErrorStats:
    type: "object"
    properties:
      frequency:
        type: "object"
        additionalProperties: true
  ErrorUpdatePayload:
    type: "object"
    properties:
      status:
        type: "string"
        description: "Status of the error"
        enum:
        - "unresolved"
        - "resolved"
        - "ignored"
      updated_by_id:
        type: "integer"
        description: "GitLab user id who triggered the update"
  ErrorEvent:
    type: "object"
    properties:
      fingerprint:
        type: "integer"
        format: "uint32"
      projectId:
        type: "integer"
        format: "uint64"
      payload:
        type: "string"
        description: "JSON encoded string"
      name:
        type: "string"
        example: "ActionView::MissingTemplate"
      description:
        type: "string"
        example: "Missing template posts/edit"
      actor:
        type: "string"
        example: "PostsController#edit"
      environment:
        type: "string"
        example: "production"
      platform:
        type: "string"
        example: "ruby"
  ErrorV2:
    type: "object"
    properties:
      id:
        type: "string"
      project:
        $ref: "#/definitions/Project"
      title:
        type: "string"
        example: "ActionView::MissingTemplate"
      actor:
        type: "string"
        example: "PostsController#edit"
      count:
        type: "string"
        format: "uint64"
      userCount:
        type: "integer"
        format: "uint64"
      lastSeen:
        type: "string"
        format: "date-time"
      firstSeen:
        type: "string"
        format: "date-time"
      status:
        type: "string"
        description: "Status of the error"
        enum:
          - "unresolved"
          - "resolved"
          - "ignored"
  Project:
    type: "object"
    properties:
      id:
        type: string
        description: "ID of the project"
      name:
        type: string
        description: "Name of the project"
      slug:
        type: string
        description: "Slug of the project"
  StatsObject:
    type: "object"
    properties:
      start:
        type: string
        description: "ID of the project"
      end:
        type: string
        description: "Name of the project"
      interval:
        type: array
        description: "Slug of the project"
        items:
          type: string
      group:
        type: array
        items:
          type: object
          properties:
            by:
              type: object
              additionalProperties: {}
            totals:
              type: object
              additionalProperties: {}
            series:
              type: object
              additionalProperties: {}
  MessageEvent:
    type: "object"
    properties:
      projectId:
        type: "integer"
        format: "uint64"
      eventId:
        type: "string"
      timestamp:
        type: "string"
        format: "date-time"
      level:
        type: "string"
        example: "info"
      message:
        type: "string"
        example: "some message from the SDK"
      release:
        type: "string"
        example: "v1.0.0"
      environment:
        type: "string"
        example: "production"
      platform:
        type: "string"
        example: "ruby"
