package errortracking

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

func TestNewErrorTrackingSession(t *testing.T) {
	assert := assert.New(t)
	payload := []byte{1, 2}
	projectID := uint64(1234455634)
	s := types.Session{
		SessionID: "1",
		UserID:    "2",
		Init:      uint8(1),
		Started:   time.Date(2022, 11, 17, 20, 34, 58, 651387237, time.UTC),
		OccuredAt: time.Date(2022, 11, 17, 20, 34, 58, 651387237, time.UTC),
		Duration:  0.123,
		Status:    "ok",
		Attributes: types.SessionAttributes{
			Release:     "v1",
			Environment: "dev",
		},
	}
	expectedStmt := "INSERT INTO gl_error_tracking_sessions (\n\t\tproject_id,\n\t\tsession_id,\n\t\tuser_id,\n\t\tinit,\n\t\tpayload,\n\t\tstarted,\n\t\toccurred_at,\n\t\tduration,\n\t\tstatus,\n\t\trelease,\n\t\tenvironment\n\t) VALUES (1234455634,'1','2',1,'\x01\x02',toDateTime64('2022-11-17 20:34:58.651387', 6,'UTC'),toDateTime64('2022-11-17 20:34:58.651387', 6,'UTC'),0.123000,'ok','v1','dev')"

	ets := et.NewErrorTrackingSession(projectID, &s, payload)

	assert.Equal(projectID, ets.ProjectID)
	assert.Equal(s.SessionID, ets.SessionID)
	assert.Equal(s.UserID, ets.UserID)
	assert.Equal(s.Init, ets.Init)
	assert.Equal(string(payload), ets.Payload)
	assert.Equal(s.Started, ets.Started)
	assert.Equal(s.OccuredAt, ets.OccurredAt)
	assert.Equal(s.Duration, ets.Duration)
	assert.Equal(s.Status, ets.Status)
	assert.Equal(s.Attributes.Release, ets.Release)
	assert.Equal(s.Attributes.Environment, ets.Environment)
	assert.Equal(expectedStmt, ets.AsInsertStmt(time.UTC))
}
