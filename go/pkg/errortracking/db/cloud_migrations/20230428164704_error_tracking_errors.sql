-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_errors
(
    project_id UInt64,
    fingerprint UInt32,
    name String,
    description String,
    actor String,
    event_count UInt64,
    approximated_user_count AggregateFunction(uniq, String),
    last_seen_at SimpleAggregateFunction(max, DateTime64(6, 'UTC')),
    first_seen_at SimpleAggregateFunction(min, DateTime64(6, 'UTC'))
)
ENGINE = ReplicatedSummingMergeTree
ORDER BY (project_id, fingerprint);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
