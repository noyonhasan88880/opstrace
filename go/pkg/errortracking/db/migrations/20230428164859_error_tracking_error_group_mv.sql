-- +goose Up
-- +goose StatementBegin
CREATE MATERIALIZED VIEW IF NOT EXISTS gl_error_tracking_group_mv_local ON CLUSTER '{cluster}' TO gl_error_tracking_group_local
AS
SELECT
    project_id,
    fingerprint as id,

    any(title) AS title,
    any(actor) AS actor,

    count() AS error_count,

    argMaxState(status, updated_at) AS status,

    uniqState(user_id) AS approximated_user_count,

    max(timestamp) AS last_seen_at,
    min(timestamp) AS first_seen_at
FROM gl_error_tracking_error_status_v2_local
GROUP BY
    project_id,
    fingerprint;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
