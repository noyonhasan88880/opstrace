-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_group ON CLUSTER '{cluster}' AS gl_error_tracking_group_local
    ENGINE = Distributed('{cluster}', currentDatabase(), gl_error_tracking_group_local, cityHash64(project_id));
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
