-- +goose Up
-- +goose StatementBegin
ALTER TABLE gl_error_tracking_error_status_v2_local ON CLUSTER '{cluster}' MODIFY SETTING storage_policy='gcs_main';
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
