package cloud

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
)

const TracesMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
    ProjectId,
    TraceId,
    SpanId,
    min(Timestamp) AS Start,
    any(ServiceName) AS ServiceName,
    any(SpanName) AS SpanName,
    any(StatusCode) AS StatusCode,
    any(Duration) AS Duration,
    any(ParentSpanId) AS ParentSpanId
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE TraceId != ''
GROUP BY
    ProjectId,
    TraceId,
    SpanId
ORDER BY Start;
`

type TracesMV struct{}

var _ migrations.RegisteredMigration = (*TracesMV)(nil)

func (t *TracesMV) Name() string {
	return "tracesMV"
}

func (t *TracesMV) Schema() string {
	return TracesMVTmpl
}

func (t *TracesMV) ValidateData(data interface{}) error {
	d, ok := data.(tracing.MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" || d.TargetTableName == "" || d.SourceTableName == "" {
		return fmt.Errorf("any of database, table, source table or target table name cannot be empty")
	}
	return nil
}

func (t *TracesMV) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *TracesMV) Setup(data interface{}) error {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)
	if migrations.IsMigrationRegistered(filename) {
		return nil // nothing to do
	}
	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	// add migrations
	goose.AddMigrationContext(
		func(ctx context.Context, tx *sql.Tx) error {
			_, err := tx.Exec(query)
			//nolint:wrapcheck
			return err
		},
		nil,
	)
	// mark migration registered
	migrations.RegisterMigration(filename)
	return nil
}
