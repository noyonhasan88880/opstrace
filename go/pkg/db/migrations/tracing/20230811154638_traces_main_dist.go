package tracing

import (
	"context"
	"database/sql"
	"fmt"
	"runtime"

	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

const (
	TracesMainDistributedTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.DistTableName}}
ON CLUSTER '{cluster}' AS {{.DatabaseName}}.{{.TableName}}
ENGINE = Distributed('{cluster}', {{.DatabaseName}}, {{.TableName}}, cityHash64(ProjectId, TraceId));
`
)

type TracesMainDist struct{}

var _ migrations.RegisteredMigration = (*TracesMainDist)(nil)

func (t *TracesMainDist) Name() string {
	return "tracesMainDist"
}

func (t *TracesMainDist) Schema() string {
	return TracesMainDistributedTmpl
}

func (t *TracesMainDist) ValidateData(data interface{}) error {
	d, ok := data.(MigrationData)
	if !ok {
		return fmt.Errorf("passed interface not applicable")
	}
	if d.DatabaseName == "" || d.TableName == "" || d.DistTableName == "" {
		return fmt.Errorf("any of database or table name cannot be empty")
	}
	return nil
}

func (t *TracesMainDist) Render(data interface{}) (string, error) {
	return migrations.Render(t.Name(), t.Schema(), data)
}

func (t *TracesMainDist) Setup(data interface{}) error {
	// check if this migration has already been setup/registered
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(0)
	if migrations.IsMigrationRegistered(filename) {
		return nil // nothing to do
	}
	// validate passed data
	if err := t.ValidateData(data); err != nil {
		return err
	}
	// render template
	query, err := migrations.Render(t.Name(), t.Schema(), data)
	if err != nil {
		return fmt.Errorf("rendering registered template %s: %w", t.Name(), err)
	}
	// add migrations
	goose.AddMigrationContext(
		func(ctx context.Context, tx *sql.Tx) error {
			_, err := tx.Exec(query)
			//nolint:wrapcheck
			return err
		},
		nil,
	)
	// mark migration registered
	migrations.RegisterMigration(filename)
	return nil
}
