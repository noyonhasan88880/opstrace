package tracing

import (
	"embed"
)

// Migrations contains all files in the sql migrations directory as is.
//
//go:embed sql/*
var Migrations embed.FS

// MigrationData defines all data we need to render tracing-specific
// migrations.
type MigrationData struct {
	DatabaseName    string
	TableName       string
	DistTableName   string
	TargetTableName string
	SourceTableName string
}
