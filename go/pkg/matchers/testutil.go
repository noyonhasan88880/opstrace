package matchers

import (
	"fmt"

	"github.com/onsi/gomega/format"
	"github.com/onsi/gomega/types"
	"k8s.io/apimachinery/pkg/api/errors"
)

type IsNotFoundErrorMatcher struct{}

func BeNotFound() types.GomegaMatcher {
	return &IsNotFoundErrorMatcher{}
}

func (s *IsNotFoundErrorMatcher) Match(actual interface{}) (success bool, err error) {
	if actual == nil {
		return false, nil
	}

	switch e := actual.(type) {
	case error:
		return errors.IsNotFound(e), nil
	default:
		return false, fmt.Errorf("IsNotFoundErrorMatcher can match errors only")
	}
}

func (s *IsNotFoundErrorMatcher) FailureMessage(actual interface{}) (message string) {
	return format.Message(actual, "to match NotFoundError")
}

func (s *IsNotFoundErrorMatcher) NegatedFailureMessage(actual interface{}) (message string) {
	return format.Message(actual, "not to match NotFoundError")
}
