package common

import (
	"net/http"
	"sync/atomic"

	netv1 "k8s.io/api/networking/v1"
)

const (
	ConditionStatusSuccess = "True"
)

func IsIngressReady(ingress *netv1.Ingress) bool {
	if ingress == nil {
		return false
	}

	return len(ingress.Status.LoadBalancer.Ingress) > 0
}

func ReadyHandler(isReady *atomic.Bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, _ *http.Request) {
		if !isReady.Load() {
			http.Error(rw, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
			return
		}
		_, err := rw.Write([]byte("OK"))
		if err != nil {
			http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	}
}
