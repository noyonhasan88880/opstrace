package common

type EnvironmentTarget string

// Supported platform targets.
var (
	AWS   EnvironmentTarget = "aws"
	GCP   EnvironmentTarget = "gcp"
	KIND  EnvironmentTarget = "kind"
	DEVVM EnvironmentTarget = "devvm"
)
