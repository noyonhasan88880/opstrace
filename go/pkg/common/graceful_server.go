package common

import (
	"context"
	"net/http"
	"time"

	"go.uber.org/zap"
)

type GracefulServer struct {
	logger  *zap.Logger
	servers []*http.Server
	// This is a configurable timeout estimating how long it might take for a
	// pod's endpoint to be removed from service endpoints and/or load-balancers.
	// Note that, `nginx-ingress-controller`` uses endpoints (podIP:port) instead
	// of a service, so we continue to serve requests till the pod is actually
	// kicked out.
	// For more details, see:
	// https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#service-upstream
	// https://github.com/kubernetes/ingress-nginx/issues/257
	waitBeforeShutdownSeconds int
}

func NewGracefulServer(
	logger *zap.Logger,
	servers []*http.Server,
	wait int,
) *GracefulServer {
	return &GracefulServer{
		logger:                    logger,
		servers:                   servers,
		waitBeforeShutdownSeconds: wait,
	}
}

func (g *GracefulServer) Start() {
	for _, s := range g.servers {
		s := s
		go func() {
			if err := s.ListenAndServe(); err != nil {
				g.logger.Fatal("starting-up server", zap.Error(err))
			}
		}()
	}
}

func (g *GracefulServer) Stop() {
	time.Sleep(time.Second * time.Duration(g.waitBeforeShutdownSeconds))
	// Ideally the code paths should never reach this point given the graceful sleep while
	// the pod is explicitly torn down. In case the pod does survive the stipulated timout,
	// we shutdown the servers explicitly here.
	for _, s := range g.servers {
		s := s
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		if err := s.Shutdown(ctx); err != nil {
			g.logger.Error("shutting-down server", zap.Error(err))
		}
		cancel()
	}
}
