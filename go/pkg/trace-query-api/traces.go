package query

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"go.uber.org/zap"
)

type TraceResult struct {
	ProjectID int64 `json:"project_id"`

	Traces      []*TraceItem `json:"traces"`
	TotalTraces int64        `json:"total_traces"`

	// NextPageToken is the token for next page if the results are greater than page size.
	NextPageToken string `json:"next_page_token,omitempty"`
}

type SpanItem struct {
	Timestamp    time.Time `json:"timestamp"`
	SpanID       string    `json:"span_id"`
	TraceID      string    `json:"trace_id"`
	ServiceName  string    `json:"service_name"`
	Operation    string    `json:"operation"`
	DurationNano uint64    `json:"duration_nano"`
	ParentSpanID string    `json:"parent_span_id"`
	// HttpMethod         string    `ch:"httpMethod"`
	// Method             string    `json:"method"`
	StatusCode string `ch:"statusCode" json:"statusCode"`
	// RPCMethod          string    `ch:"rpcMethod"`
}

type TraceItem struct {
	Timestamp    time.Time `json:"timestamp"`
	TraceID      string    `json:"trace_id"`
	ServiceName  string    `json:"service_name"`
	Operation    string    `json:"operation"`
	StatusCode   string    `json:"statusCode"`
	DurationNano uint64    `json:"duration_nano"`

	Spans      []SpanItem `json:"spans"`
	TotalSpans uint64     `json:"totalSpans"`
}

type traceHandlerParams struct {
	GroupID   int64 `uri:"group_id" binding:"required,numeric,min=1"`
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

type traceHandlerParamsV3 struct {
	ProjectID int64 `uri:"project_id" binding:"required,numeric,min=1"`
}

type pageParams struct {
	// Default Page Size is 100.
	PageSize  int64  `form:"page_size,default=100" binding:"numeric,max=1000"`
	PageToken string `form:"page_token"`
}

type requestFilterParams struct {
	TraceIDs        []string
	Period          string
	Operations      []string
	ServiceNames    []string
	NotOperations   []string
	NotServiceNames []string
	NotTraceIDs     []string

	LtDuration int64
	GtDuration int64

	Page   *pageParams
	cursor *Page
}

type APIVersion int64

const (
	NoVersion = iota
	V3
)

type HandlerFactoryOptions struct {
	version APIVersion
}

func (c *Controller) TraceHandlerFactory(options HandlerFactoryOptions) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		projectID := int64(0)

		switch options.version {
		case V3:
			handlerParams := &traceHandlerParamsV3{}
			if err := ctx.BindUri(handlerParams); err != nil {
				c.Logger.Error("bind error", zap.Error(err))
				// context already canceled by BindUri

				return
			}
			projectID = handlerParams.ProjectID
		default:
			handlerParams := &traceHandlerParams{}
			if err := ctx.BindUri(handlerParams); err != nil {
				c.Logger.Error("bind error", zap.Error(err))
				// context already canceled by BindUri

				return
			}
			projectID = handlerParams.ProjectID
		}

		page := &pageParams{}
		if err := ctx.ShouldBindQuery(page); err != nil {
			c.Logger.Error("bind error", zap.Error(err))
			ctx.AbortWithError(http.StatusBadRequest, err).SetType(gin.ErrorTypeBind) //nolint:errcheck
			return
		}
		c.Logger.Debug("got", zap.Int64("pageSize", page.PageSize), zap.String("pageToken", page.PageToken))

		filter, err := c.collectFilterParameters(ctx)
		if err != nil {
			ctx.AbortWithError(400, err)
			return
		}

		if page.PageToken != "" {
			cursor, err := decodePage(&page.PageToken)
			if err != nil {
				c.Logger.Error("decoding page:", zap.Error(err))
				ctx.AbortWithError(http.StatusBadRequest, err)
				return
			}
			filter.cursor = cursor
		}
		filter.Page = page

		results, nextPageToken, err := c.Q.GetTraces(ctx, projectID, filter)
		if err != nil {
			c.Logger.Error("get traces", zap.Error(err))
			ctx.AbortWithError(500, err)
			return
		}

		resp := &TraceResult{
			ProjectID:     projectID,
			TotalTraces:   int64(len(results)),
			Traces:        results,
			NextPageToken: nextPageToken,
		}
		ctx.JSON(200, resp)
	}
}

// Helper to register metrics for traces Handler
// In order to avoid cardinality issues with metrics additionally sliced with path that have groupID/projectID
// just return the handler path.
func stripProjectIDFromPath(path string) string {
	if strings.Contains(path, "/v1/traces") {
		return "/v1/traces"
	}
	return path
}

func errorLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		for _, err := range c.Errors {
			logger.Error("", zap.Error(err), zap.String("request_uri", c.Request.RequestURI))
		}
	}
}

const (
	traceIDParam     = "trace_id"
	periodParam      = "period"
	operationParam   = "operation"
	serviceNameParam = "service_name"

	notTraceIDParam     = "not[trace_id]"
	notOperationParam   = "not[operation]"
	notServiceNameParam = "not[service_name]"

	gtDurationParam = "gt[duration_nano]"
	ltDurationParam = "lt[duration_nano]"
)

var validPeriods = map[string]bool{
	common.Period1m:  true,
	common.Period5m:  true,
	common.Period15m: true,
	common.Period30m: true,
	common.Period1h:  true,
	common.Period4h:  true,
	common.Period12h: true,
	common.Period24h: true,
	common.Period7d:  true,
	common.Period14d: true,
	common.Period30d: true,
}

// Note: This is a simple function only to validate parameters, further refactoring will only make the logic hard to
// understand.
//
//nolint:cyclop
func (c *Controller) collectFilterParameters(ctx *gin.Context) (*requestFilterParams, error) {
	filter := &requestFilterParams{}

	traceIDs, ok := ctx.GetQueryArray(traceIDParam)
	if ok {
		for i, t := range traceIDs {
			id, err := uuid.Parse(t)
			if err != nil {
				return nil, fmt.Errorf("invalid trace_id: %s - %w", t, err)
			}
			// use standard 36 char string representation that works with ClickHouse
			// i.e. xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
			traceIDs[i] = id.String()
		}
		filter.TraceIDs = traceIDs
	}

	notTraceIDs, ok := ctx.GetQueryArray(notTraceIDParam)
	if ok {
		for i, t := range notTraceIDs {
			id, err := uuid.Parse(t)
			if err != nil {
				return nil, fmt.Errorf("invalid trace_id: %s - %w", t, err)
			}
			notTraceIDs[i] = id.String()
		}
		filter.NotTraceIDs = notTraceIDs
	}

	period, ok := ctx.GetQuery(periodParam)
	if ok {
		valid := validPeriods[period]
		if !valid {
			return nil, fmt.Errorf("invalid period: %s", period)
		}
		filter.Period = period
	}

	// Note: Though these query parameters are not validated as they are free-form strings, we always use
	// queries with parameter bindings so sql injection is not possible.
	operations, ok := ctx.GetQueryArray(operationParam)
	if ok {
		filter.Operations = operations
	}

	serviceNames, ok := ctx.GetQueryArray(serviceNameParam)
	if ok {
		filter.ServiceNames = serviceNames
	}

	notOps, ok := ctx.GetQueryArray(notOperationParam)
	if ok {
		filter.NotOperations = notOps
	}

	notServ, ok := ctx.GetQueryArray(notServiceNameParam)
	if ok {
		filter.NotServiceNames = notServ
	}

	ltDuration, ok := ctx.GetQuery(ltDurationParam)
	if ok {
		nanos, err := strconv.ParseInt(ltDuration, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid duration got - %s: %w", ltDuration, err)
		}
		filter.LtDuration = nanos
	}
	gtDuration, ok := ctx.GetQuery(gtDurationParam)
	if ok {
		nanos, err := strconv.ParseInt(gtDuration, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid duration got - %s: %w", gtDuration, err)
		}
		filter.GtDuration = nanos
	}

	return filter, nil
}
