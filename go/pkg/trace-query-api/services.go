package query

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"
)

type ServicesResult struct {
	Services []ServiceItem `json:"services"`
}

type ServiceItem struct {
	Name string `json:"name"`
}

func (c *Controller) ServicesHandler(ctx *gin.Context) {
	handlerParams := &traceHandlerParamsV3{}
	if err := ctx.BindUri(handlerParams); err != nil {
		c.Logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri

		return
	}
	projectID := handlerParams.ProjectID

	services, err := c.Q.GetServices(ctx, projectID)
	if err != nil {
		c.Logger.Error("get services", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(200, ServicesResult{
		Services: services,
	})
}

func (q *querier) GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error) {
	const query = `
SELECT ServiceName FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? GROUP BY ServiceName ORDER BY ServiceName ASC`

	services := []struct {
		ServiceName string
	}{}
	err := q.db.Select(ctx, &services, query, strconv.Itoa(int(projectID)))
	if err != nil {
		return nil, fmt.Errorf("query services: %w", err)
	}

	sns := make([]ServiceItem, len(services))
	for i, s := range services {
		sns[i].Name = s.ServiceName
	}

	return sns, nil
}
