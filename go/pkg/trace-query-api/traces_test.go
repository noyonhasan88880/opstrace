package query

import (
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Context("trace handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		gin.SetMode(gin.DebugMode)
		gin.DefaultWriter = GinkgoWriter
		router = gin.New()
	})

	Context("List Traces", func() {
		DescribeTable("query API",
			func(url string, statusCode int, expectedFilter *requestFilterParams) {
				q := &mockQueryDB{}
				controller := &Controller{
					Q:      q,
					Logger: logger.Desugar(),
				}
				SetRoutes(controller, router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))

				Expect(q.passedFilter).To(Equal(expectedFilter))
			},
			Entry(
				"returns 200 for a list of traces",
				"/query/1/123/v1/traces",
				http.StatusOK,
				&requestFilterParams{
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"gets 404 on missing groupID",
				"/query/1/v1/traces",
				http.StatusNotFound,
				nil,
			),
			Entry(
				"gets 400 on invalid traceID",
				"/query/1/123/v1/traces?trace_id=1222",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"gets 400 on invalid period",
				"/query/1/123/v1/traces?period=10m",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with valid params",
				"/query/1/123/v1/traces?trace_id=b7e641ba-918b-3147-2cba-e93b13e7de2a&period=1m",
				http.StatusOK,
				&requestFilterParams{
					TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
					Period:   "1m",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with valid operation and service name",
				"/query/1/123/v1/traces?service_name=blah&operation=fake",
				http.StatusOK,
				&requestFilterParams{
					ServiceNames: []string{"blah"},
					Operations:   []string{"fake"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with repeated params name",
				"/query/1/123/v1/traces?service_name=blah&operation=fake&operation=boo&service_name=fiver",
				http.StatusOK,
				&requestFilterParams{
					ServiceNames: []string{"blah", "fiver"},
					Operations:   []string{"fake", "boo"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with not operator in params name",
				"/query/1/123/v1/traces?not[service_name]=blah&not[operation]=fake&not[operation]=boo&not[service_name]=fiver",
				http.StatusOK,
				&requestFilterParams{
					NotServiceNames: []string{"blah", "fiver"},
					NotOperations:   []string{"fake", "boo"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 400 with incorrect duration params",
				"/query/1/123/v1/traces?lt[duration_nano]=400s&gt[duration_nano]=200s&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with proper duration params",
				"/query/1/123/v1/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h",
				http.StatusOK,
				&requestFilterParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with custom period of 4h",
				"/query/1/123/v1/traces?period=4h",
				http.StatusOK,
				&requestFilterParams{
					Period: "4h",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with custom period of 5m",
				"/query/1/123/v1/traces?period=5m",
				http.StatusOK,
				&requestFilterParams{
					Period: "5m",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"returns 200 with page size parameter",
				"/query/1/123/v1/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h&page_size=20",
				http.StatusOK,
				&requestFilterParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					Page: &pageParams{
						PageSize: 20,
					},
				},
			),
			Entry(
				"returns 200 with page size and page token parameter",
				"/query/1/123/v1/traces?page_size=20&page_token=eyJzdGFydF90aW1lc3RhbXAiOiIyMDIzLTA5LTA0IDExOjI1OjIzLjE1NjkyMzAwMCJ9",
				http.StatusOK,
				&requestFilterParams{
					Page: &pageParams{
						PageSize:  20,
						PageToken: "eyJzdGFydF90aW1lc3RhbXAiOiIyMDIzLTA5LTA0IDExOjI1OjIzLjE1NjkyMzAwMCJ9",
					},
					cursor: &Page{
						StartTimestamp: "2023-09-04 11:25:23.156923000",
					},
				},
			),
			Entry(
				"returns 400 with wrong page token parameter",
				"/query/1/123/v1/traces?page_size=20&page_token=full",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 for a list of traces",
				"/v3/query/123/traces",
				http.StatusOK,
				&requestFilterParams{
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 gets 400 on invalid traceID",
				"/v3/query/123/traces?trace_id=1222",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 gets 400 on invalid period",
				"/v3/query/123/traces?period=10m",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with valid params",
				"/v3/query/123/traces?trace_id=b7e641ba-918b-3147-2cba-e93b13e7de2a&period=1m",
				http.StatusOK,
				&requestFilterParams{
					TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
					Period:   "1m",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with valid operation and service name",
				"/v3/query/123/traces?service_name=blah&operation=fake",
				http.StatusOK,
				&requestFilterParams{
					ServiceNames: []string{"blah"},
					Operations:   []string{"fake"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with repeated params name",
				"/v3/query/123/traces?service_name=blah&operation=fake&operation=boo&service_name=fiver",
				http.StatusOK,
				&requestFilterParams{
					ServiceNames: []string{"blah", "fiver"},
					Operations:   []string{"fake", "boo"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with not operator in params name",
				"/v3/query/123/traces?not[service_name]=blah&not[operation]=fake&not[operation]=boo&not[service_name]=fiver",
				http.StatusOK,
				&requestFilterParams{
					NotServiceNames: []string{"blah", "fiver"},
					NotOperations:   []string{"fake", "boo"},
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 400 with incorrect duration params",
				"/v3/query/123/traces?lt[duration_nano]=400s&gt[duration_nano]=200s&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"v3 returns 200 with proper duration params",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h",
				http.StatusOK,
				&requestFilterParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with custom period of 4h",
				"/v3/query/123/traces?period=4h",
				http.StatusOK,
				&requestFilterParams{
					Period: "4h",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with custom period of 5m",
				"/v3/query/123/traces?period=5m",
				http.StatusOK,
				&requestFilterParams{
					Period: "5m",
					Page: &pageParams{
						PageSize: 100,
					},
				},
			),
			Entry(
				"v3 returns 200 with page size parameter",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h&page_size=20",
				http.StatusOK,
				&requestFilterParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					Page: &pageParams{
						PageSize: 20,
					},
				},
			),
			Entry(
				"v3 returns 200 with page size and page token parameter",
				"/v3/query/123/traces?page_size=20&page_token=eyJzdGFydF90aW1lc3RhbXAiOiIyMDIzLTA5LTA0IDExOjI1OjIzLjE1NjkyMzAwMCJ9",
				http.StatusOK,
				&requestFilterParams{
					Page: &pageParams{
						PageSize:  20,
						PageToken: "eyJzdGFydF90aW1lc3RhbXAiOiIyMDIzLTA5LTA0IDExOjI1OjIzLjE1NjkyMzAwMCJ9",
					},
					cursor: &Page{
						StartTimestamp: "2023-09-04 11:25:23.156923000",
					},
				},
			),
			Entry(
				"v3 returns 400 with wrong page token parameter",
				"/v3/query/123/traces?page_size=20&page_token=full",
				http.StatusBadRequest,
				nil,
			),
		)
	})
})

var _ = Context("trace query builder", func() {
	var (
		refTime          = time.Now().UTC()
		defaultStartTime = refTime.Add(-1 * 24 * time.Hour).Unix()
		defaultEndTime   = refTime.Add(1 * time.Hour).Unix()
	)

	DescribeTable("builds queries",
		func(projectID int64, filter *requestFilterParams, expected string, args []interface{}) {
			builder := buildTraceMVQuery(projectID, filter, refTime)
			diff := cmp.Diff(builder.sql, expected)
			logger.Debug("diff - ", diff)
			Expect(diff).To(Equal(""))
			Expect(builder.args).To(Equal(args))
		},
		Entry(
			"list trace query with default period and no filters",
			int64(1),
			&requestFilterParams{},
			`
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3)
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				defaultStartTime,
				defaultEndTime,
				"1",
			},
		),
		Entry(
			"list trace query with no period and 1 trace ID",
			int64(1),
			&requestFilterParams{
				TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
			},
			`
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (ProjectId = $1) AND ( TraceId IN [ UUIDStringToNum($2) ] ) 
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				"1",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period and list of traceID",
			int64(1),
			&requestFilterParams{
				TraceIDs: []string{
					"b7e641ba-918b-3147-2cba-e93b13e7de2a",
					"b7e641ba-918b-3147-2dss-e13c13edd1x0",
				},
				Period: "1m",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3) AND ( TraceId IN [ UUIDStringToNum($4), UUIDStringToNum($5) ] ) 
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				"b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"b7e641ba-918b-3147-2dss-e13c13edd1x0",
			},
		),
		Entry(
			"list trace query with custom period and 1 service and operations",
			int64(1),
			&requestFilterParams{
				ServiceNames: []string{"service-1"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3) AND ( ServiceName IN ( $4 ) )  AND ( SpanName IN ( $5 ) ) 
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				[]string{"service-1"},
				[]string{"op-1", "op-2"},
			},
		),
		Entry(
			"list trace query with custom period and multiple service and operations",
			int64(1),
			&requestFilterParams{
				ServiceNames: []string{"service-1", "service-2"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3) AND ( ServiceName IN ( $4 ) )  AND ( SpanName IN ( $5 ) ) 
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				[]string{"service-1", "service-2"},
				[]string{"op-1", "op-2"},
			},
		),
		Entry(
			"list trace query with custom period and duration filter",
			int64(1),
			&requestFilterParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3) AND ( Duration <= $4 )  AND ( Duration >= $5 ) 
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
			},
		),
		Entry(
			"list trace query with custom period of 12h",
			int64(1),
			&requestFilterParams{
				Period: "12h",
			},
			`
WITH
    toStartOfHour(toDateTime($1)) AS r_start,
    toStartOfHour(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3)
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 12 * time.Hour).Unix(),
				refTime.Add(1 * time.Hour).Unix(),
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 4h",
			int64(1),
			&requestFilterParams{
				Period: "4h",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3)
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 4 * time.Hour).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
			},
		),
		Entry(
			"list trace query with custom period of 5m",
			int64(1),
			&requestFilterParams{
				Period: "5m",
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3)
    GROUP BY TraceId

    ORDER BY trace_start_ts DESC
    LIMIT 100
`,
			[]interface{}{
				refTime.Add(-1 * 5 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter and page token",
			int64(1),
			&requestFilterParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Page: &pageParams{
					PageSize:  50,
					PageToken: "eyJzdGFydF90aW1lc3RhbXAiOiIyMDIzLTA5LTA0IDExOjI1OjIzLjE1NjkyMzAwMCJ9",
				},
				cursor: &Page{StartTimestamp: "2023-09-04 11:25:23.156923000"},
			},
			`
WITH
    toStartOfMinute(toDateTime($1)) AS r_start,
    toStartOfMinute(toDateTime($2)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = $3) AND ( Duration <= $4 )  AND ( Duration >= $5 ) 
    GROUP BY TraceId

    HAVING trace_start_ts < $6

    ORDER BY trace_start_ts DESC
    LIMIT 50
`,
			[]interface{}{
				refTime.Add(-1 * 1 * time.Minute).Unix(),
				refTime.Add(1 * time.Minute).Unix(),
				"1",
				int64(300),
				int64(500),
				"2023-09-04 11:25:23.156923000",
			},
		),
	)
})
