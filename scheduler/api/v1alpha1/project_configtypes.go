package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	cfg "sigs.k8s.io/controller-runtime/pkg/config/v1alpha1"
)

// LogLevel defines verbosity for cluster wide logging.
// Supported values are "info", "warn", "debug", "error".
// Default is "info"
// +kubebuilder:validation:Enum=trace;debug;error;warn;info
type LogLevel string

// ProjectConfig is the Schema for the projectconfigs API
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +kubebuilder:object:root=true
type ProjectConfig struct {
	metav1.TypeMeta `json:",inline"`

	// ControllerManagerConfigurationSpec returns the configurations for controllers
	cfg.ControllerManagerConfigurationSpec `json:",inline"`
	// Set Log level, default is "info"
	// +optional
	LogLevel LogLevel `json:"logLevel"`
}

func init() {
	SchemeBuilder.Register(&ProjectConfig{})
}
