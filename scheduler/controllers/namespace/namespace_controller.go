package namespace

import (
	"context"
	"net/http"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	v1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

const (
	finalizerName = "gitlabnamespace.opstrace.com/finalizer"
)

// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabnamespaces/finalizers,verbs=update
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=tenants/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=groups,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=opstrace.com,resources=groups/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=extensions;apps,resources=deployments;deployments/finalizers;statefulsets;statefulsets/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=events,verbs=get;list;watch;create;patch
// +kubebuilder:rbac:groups="",resources=namespaces;configmaps;secrets;serviceaccounts;services;persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=namespaces/status;,verbs=get;list;watch

// SetupWithManager sets up the controller with the Manager.
func (r *ReconcileGitLabNamespace) SetupWithManager(mgr ctrl.Manager) error {
	clusterHandler := handler.EnqueueRequestsFromMapFunc(
		// NOTE(prozlach): We do not really care about the object, as Cluster
		// CR is a singleton for a forseable future.
		func(_ client.Object) []reconcile.Request {
			allGitlabNamespaces := new(v1alpha1.GitLabNamespaceList)
			err := r.Client.List(context.TODO(), allGitlabNamespaces)
			if err != nil {
				return nil
			}

			res := make([]reconcile.Request, 0, len(allGitlabNamespaces.Items))
			for _, item := range allGitlabNamespaces.Items {
				res = append(
					res,
					reconcile.Request{
						NamespacedName: types.NamespacedName{
							Name:      item.GetName(),
							Namespace: item.GetNamespace(),
						},
					},
				)
			}

			return res
		})

	// TODO(prozlach): Instead of a Watch() function here, the Tenant object
	// should have a proper controller reference set instead, so that this
	// controller will be reconciled via Own() function instead.
	tenantHandler := handler.EnqueueRequestsFromMapFunc(
		func(tenant client.Object) []reconcile.Request {
			glNamespace := &v1alpha1.GitLabNamespace{}
			objKey := client.ObjectKey{
				Name: tenant.GetNamespace(),
			}
			err := r.Client.Get(context.TODO(), objKey, glNamespace)
			if err != nil {
				return nil
			}

			return []reconcile.Request{
				{
					NamespacedName: types.NamespacedName{
						Name:      glNamespace.GetName(),
						Namespace: glNamespace.GetNamespace(),
					},
				},
			}
		})

	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{
			// The same object will not be reconciled in parallel.
			// This allows for many GitLabNamespaces to be reconciled
			// in parallel. A sufficiently large number here
			// could cause Kubernetes API rate-limits to kick in.
			MaxConcurrentReconciles: 50,
		}).
		For(&v1alpha1.GitLabNamespace{}).
		Watches(
			&source.Kind{Type: &v1alpha1.Cluster{}},
			clusterHandler,
			builder.WithPredicates(predicate.GenerationChangedPredicate{}),
		).
		Watches(
			&source.Kind{Type: &tenantOperator.Tenant{}},
			tenantHandler,
			// NOTE(prozlach): We need resourece version here, as the code uses
			// data from Tenant's status.
			builder.WithPredicates(predicate.ResourceVersionChangedPredicate{}),
		).
		Owns(&tenantOperator.Tenant{}).
		Owns(&tenantOperator.Group{}).
		Owns(&appsv1.Deployment{}).
		Owns(&corev1.Secret{}).
		Owns(&corev1.Namespace{}).
		Owns(&rbacv1.Role{}).
		Owns(&rbacv1.RoleBinding{}).
		Complete(r)
}

var _ reconcile.Reconciler = &ReconcileGitLabNamespace{}

// ReconcileGitLabNamespace reconciles a GitLabNamespace object
type ReconcileGitLabNamespace struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	Client       client.Client
	Scheme       *runtime.Scheme
	Transport    *http.Transport
	Recorder     record.EventRecorder
	Log          logr.Logger
	StatusPoller *polling.StatusPoller
}

// Reconcile: The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true. Upon completion, we just requeue the Request explicitly to be reconciled every minute
// thereafter.
//
// One valid use-case of the repeated reconcile(s) of the underlying `GitlabNamespace` object is the need for
// updating a `Tenant` to inherit changes in `ClusterSpec.GOUI`. Since `Cluster“ updates do not trigger
// `GitlabNamespace` and by proxy `Tenant` reconciliation, th controller attempts to reconcile all underlying
// objects every X period of time.
func (r *ReconcileGitLabNamespace) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result ctrl.Result, err error) {
	cr := &v1alpha1.GitLabNamespace{}
	err = r.Client.Get(ctx, request.NamespacedName, cr)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			r.Log.Info("GitLabNamespace has been removed from API", "name", request.Name)
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.GitlabNamespaceFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()

	// Read current state
	currentState := NewGitLabNamespaceState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	// Get the actions required to reach the desired state
	// The order of process matters here. In general, we should first update
	// the lowest order dependencies and then work our way up the stack.
	// Reconciliation will block (and retry) until each action returns successfully.
	// This means that readiness check actions will return errors
	// until each readiness check passes, which blocks downstream actions until the readiness
	// check passes. So in the following case, ClickHouse gitlabNamespace
	// won't we upgraded/reconciled until the ClickHouse operator has been reconciled
	// and passes the readiness check.
	nsReconciler := NewGitLabNamespaceReconciler(teardown, r.Log)
	desiredState := nsReconciler.Reconcile(currentState, cr)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.ClusterFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager)
	err = actionRunner.RunAll(desiredState)
	if err != nil {
		r.manageError(cr, err)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr, currentState)
	}

	return reconcile.Result{}, nil
}

// Handle success case
func (r *ReconcileGitLabNamespace) manageSuccess(cr *v1alpha1.GitLabNamespace, state *GitLabNamespaceState) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	// Check conditions of group and tenant and only then set condition to true.
	if state.Tenant == nil || !apimeta.IsStatusConditionTrue(state.Tenant.Status.Conditions, common.ConditionTypeReady) {
		condition.Status = metav1.ConditionFalse
		condition.Reason = common.ReconciliationFailedReason
		condition.Message = "Tenant not Ready"
	} else if state.Group == nil || !apimeta.IsStatusConditionTrue(state.Group.Status.Conditions, common.ConditionTypeReady) {
		condition.Status = metav1.ConditionFalse
		condition.Reason = common.ReconciliationFailedReason
		condition.Message = "Group not Ready"
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)

	r.Log.Info("gitlabNamespace successfully reconciled", "gitlabNamespace", cr.Name)
}

// Handle error case: update gitlabNamespace with error message and status
func (r *ReconcileGitLabNamespace) manageError(cr *v1alpha1.GitLabNamespace, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: cr.GetGeneration(),
	}
	apimeta.SetStatusCondition(&cr.Status.Conditions, condition)
}
