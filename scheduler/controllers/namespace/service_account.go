package namespace

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func getServiceAccountName() string {
	return GetTenantOperatorName()
}

func getServiceAccountLabels() map[string]string {
	return GetTenantOperatorSelector()
}

func getServiceAccountAnnotations(cr *v1alpha1.GitLabNamespace, existing map[string]string) map[string]string {
	return existing
}

func ServiceAccount(clusterConfig *v1alpha1.Cluster, cr *v1alpha1.GitLabNamespace) *v1.ServiceAccount {
	return &v1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:        getServiceAccountName(),
			Namespace:   cr.Namespace(),
			Labels:      getServiceAccountLabels(),
			Annotations: getServiceAccountAnnotations(cr, nil),
		},
		ImagePullSecrets: clusterConfig.Spec.ImagePullSecrets,
	}
}

func ServiceAccountMutator(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
	current *v1.ServiceAccount,
) error {
	current.Labels = getServiceAccountLabels()
	current.Annotations = getServiceAccountAnnotations(cr, current.Annotations)
	current.ImagePullSecrets = clusterConfig.Spec.ImagePullSecrets

	return nil
}
