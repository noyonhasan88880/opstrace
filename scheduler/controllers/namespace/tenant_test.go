package namespace

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	tenantOperator "gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestTenant(t *testing.T) {
	domain := "foo.com"
	glURL := "gitlab.com"
	cr := &v1alpha1.GitLabNamespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "test",
		},
		Spec: v1alpha1.GitLabNamespaceSpec{
			ID:                  32,
			TopLevelNamespaceID: 32,
		},
	}
	makeTenant := func(spec tenantOperator.TenantSpec) *tenantOperator.Tenant {
		return &tenantOperator.Tenant{
			ObjectMeta: metav1.ObjectMeta{
				Name:      constants.TenantName,
				Namespace: "32",
				Labels:    map[string]string{},
			},
			Spec: spec,
		}
	}

	tests := []struct {
		name    string
		cluster *v1alpha1.Cluster
		cr      *v1alpha1.GitLabNamespace
		want    *tenantOperator.Tenant
	}{
		{
			"default setup with domain and gitlab URL",
			&v1alpha1.Cluster{
				Spec: v1alpha1.ClusterSpec{
					DNS: v1alpha1.DNSSpec{
						Domain: &domain,
					},
					GitLab: v1alpha1.GitLabSpec{
						InstanceURL: glURL,
					},
				},
			},
			cr,
			makeTenant(tenantOperator.TenantSpec{
				Domain: &domain,
			}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Tenant(tt.cluster, tt.cr)
			assert.Equal(t, tt.want, got)
		})
	}
}
