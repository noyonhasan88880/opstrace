package cluster

import (
	"errors"
	"fmt"
	"sync"

	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"k8s.io/utils/pointer"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing/cloud"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger

	// Note: Only required while we have dual write for Cloud instance.
	once sync.Once
}

func NewClickHouseReconciler(teardown bool, logger logr.Logger) *ClickHouseReconciler {
	return &ClickHouseReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse"),
		once:     sync.Once{},
	}
}

func (i *ClickHouseReconciler) Reconcile(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	desired := common.DesiredState{}

	// when tearing down our desired state, it is important we follow a sequence of
	// actions which allows all components to be deleted correctly without creating
	// orphans OR causing a component to be gone before anything that still references
	// and/or needs it. Though not mandatory, a good sequence to follow is the reverse
	// order of how each component got provisioned when building the desired state
	// initially.
	if i.Teardown {
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
	} else {
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
	}

	desired = desired.AddActions(i.getReadiness(state, cr))
	desired = desired.AddActions(i.setupCloudDatabases(state, cr))

	return desired
}

func (i *ClickHouseReconciler) getReadiness(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if i.Teardown {
		if state.ClickHouse.Cluster != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ClickHouse.Cluster,
					Msg: "check clickhouse cluster is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}

	if !state.ClickHouse.isClusterReady() {
		return []common.Action{common.LogAction{
			Msg:   "clickhouse cluster not ready",
			Error: errors.New("clickhouse cluster not ready"),
		}}
	}

	endpoints, err := state.ClickHouse.GetEndpoints()
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
			Error: err,
		}}
	}

	// add storage-backend specific databases as needed first
	actions := i.setupDatabases(state, cr)

	// check if needed migrations have already been applied
	if cr.Status.LastMigrationApplied != nil {
		if *cr.Status.LastMigrationApplied == constants.DockerImageTag {
			return actions
		}
	}
	// for go migrations in goose, we first need to set them up.
	if err := setupTracingMigrations(); err != nil {
		// likely programming error, malformed migrations are bad
		panic(fmt.Sprintf("migration setup failed: %s", err.Error()))
	}
	// for SQL migrations, we can use them embedded as-is, i.e. from the
	// the embed.FS object: tracing.Migrations.
	actions = append(actions, common.ClickHouseMigrationAction{
		Msg:             "create clickhouse migrations for tracing",
		ClickHouseDSN:   fmt.Sprintf("%s/%s", endpoints.Native.String(), constants.TracingDatabaseName),
		MigrationsToRun: tracing.Migrations,
		SetLastMigrationApplied: func(version string) {
			cr.Status.LastMigrationApplied = pointer.String(version)
		},
	})

	return actions
}

func (i *ClickHouseReconciler) setupDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	useRemoteStorageTracing, err := common.ParseFeatureAsBool(cr.Spec.Features, "TRACING_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <TRACING_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}

	endpoints, err := state.ClickHouse.GetEndpoints()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
				Error: err,
			},
		}
	}

	actions := []common.Action{
		common.LogAction{
			Msg: "clickhouse cluster is ready",
		},
		common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerDatabaseName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
				constants.JaegerDatabaseName),
			URL: endpoints.Native,
		},
		common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.ErrorTrackingAPIDatabaseName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
				constants.ErrorTrackingAPIDatabaseName),
			URL: endpoints.Native,
		},
	}

	if useRemoteStorageTracing {
		switch cr.Spec.Target {
		case common.GCP:
			actions = append(actions, common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerGCSDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.JaegerGCSDatabaseName),
				URL: endpoints.Native,
			})
		case common.AWS:
			actions = append(actions, common.ClickHouseAction{
				Msg: fmt.Sprintf("create clickhouse database if not exists: %s", constants.JaegerGCSDatabaseName),
				SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'",
					constants.JaegerS3DatabaseName),
				URL: endpoints.Native,
			})
		}
	}

	return actions
}

func (i *ClickHouseReconciler) setupCloudDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	var actions []common.Action

	endpoint, err := state.ClickHouse.GetClickHouseCloudEndpoint()
	if err != nil {
		actions = append(actions, common.LogAction{
			Msg:   "failed to obtain clickhouse cloud endpoint",
			Error: err,
		})
		return actions
	}

	if endpoint == nil {
		return []common.Action{
			common.LogAction{
				Msg: "ClickHouse Cloud DSN not provided",
			},
		}
	}

	actions = append(actions, common.ClickHouseAction{
		Msg: fmt.Sprintf("create clickhouse database on CH Cloud if not exists: %s", constants.ErrorTrackingAPIDatabaseName),
		SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", constants.ErrorTrackingAPIDatabaseName),
		URL: endpoint.Native,
	})
	actions = append(actions, common.ClickHouseAction{
		Msg: fmt.Sprintf("create clickhouse database on CH Cloud if not exists: %s", constants.ErrorTrackingAPIDatabaseName),
		SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", constants.TracingDatabaseName),
		URL: endpoint.Native,
	})

	if cr.Status.LastCloudMigrationApplied != nil {
		if *cr.Status.LastCloudMigrationApplied == constants.DockerImageTag {
			return actions
		}
	}
	// Clear up registered go migrations inside goose as already registered migrations will not work.
	// We already keep track of migrations registered separately.
	// This should be called only once as otherwise there would be mismatch between the internal goose state and ours.
	i.once.Do(func() {
		goose.ClearCollectedGoMigrations()
	})

	if err = setupTracingMigrationsForCloud(); err != nil {
		panic(fmt.Sprintf("migration setup failed: %s", err.Error()))
	}

	// Add tracing database for migrations
	cloudDSN := endpoint.Native.JoinPath(constants.TracingDatabaseName).String()
	actions = append(actions, common.ClickHouseCloudMigrationAction{
		Msg:             "create clickhouse cloud migrations for tracing",
		ClickHouseDSN:   cloudDSN,
		MigrationsToRun: nil,
		SetLastMigrationApplied: func(version string) {
			cr.Status.LastCloudMigrationApplied = pointer.String(version)
		},
	})

	return actions
}

func setupTracingMigrations() error {
	if err := (&tracing.TracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName: constants.TracingDatabaseName,
			TableName:    constants.TracingTableName,
		},
	); err != nil {
		return err
	}
	if err := (&tracing.TracesMainDist{}).Setup(
		tracing.MigrationData{
			DatabaseName:  constants.TracingDatabaseName,
			TableName:     constants.TracingTableName,
			DistTableName: constants.TracingDistTableName,
		},
	); err != nil {
		return err
	}
	// Note: Order of migrations is important
	if err := (&tracing.TracesMVTarget{}).Setup(
		tracing.MigrationData{
			DatabaseName: constants.TracingDatabaseName,
			TableName:    constants.TracingMVTargetTableName,
		}); err != nil {
		return err
	}
	if err := (&tracing.TracesMVTargetDist{}).Setup(
		tracing.MigrationData{
			DatabaseName:  constants.TracingDatabaseName,
			TableName:     constants.TracingMVTargetTableName,
			DistTableName: constants.TracingMVTargetDistTableName,
		}); err != nil {
		return err
	}
	if err := (&tracing.TracesMV{}).Setup(
		tracing.MigrationData{
			DatabaseName:    constants.TracingDatabaseName,
			TableName:       constants.TracingMVName,
			TargetTableName: constants.TracingMVTargetTableName,
			SourceTableName: constants.TracingTableName,
		}); err != nil {
		return err
	}
	return nil
}

func setupTracingMigrationsForCloud() error {
	if err := (&cloud.TracesMain{}).Setup(
		tracing.MigrationData{
			DatabaseName: constants.TracingDatabaseName,
			TableName:    constants.TracingDistTableName,
		},
	); err != nil {
		return err
	}
	// Note: Order of migrations is important
	if err := (&cloud.TracesMVTarget{}).Setup(
		tracing.MigrationData{
			DatabaseName: constants.TracingDatabaseName,
			TableName:    constants.TracingMVTargetDistTableName,
		}); err != nil {
		return err
	}
	if err := (&cloud.TracesMV{}).Setup(
		tracing.MigrationData{
			DatabaseName:    constants.TracingDatabaseName,
			TableName:       constants.TracingMVName,
			TargetTableName: constants.TracingMVTargetDistTableName,
			SourceTableName: constants.TracingDistTableName,
		}); err != nil {
		return err
	}
	return nil
}

func (i *ClickHouseReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := clickhouse.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "clickhouse credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "clickhouse credentials",
		Mutator: func() error {
			return clickhouse.CredentialsMutator(cr, s)
		},
	}
}

func (i *ClickHouseReconciler) getClickHouseDesiredState(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if state.ClickHouse.Credentials == nil {
		if i.Teardown {
			// if the secret does not exist now AND we're tearing down the CR, we're done here
			// because the CR should have been deleted in a previous reconcile loop. Just make
			// sure that has happened successfully.
			if state.ClickHouse.Cluster != nil {
				return []common.Action{
					common.CheckGoneAction{
						Ref: state.ClickHouse.Cluster,
						Msg: "check clickhouse CR is gone",
					},
				}
			} else {
				return []common.Action{} // nothing to do
			}
		}
		// if the secret does not exist yet BUT we're in a provisioning loop, report transient error
		// and come back again during the next reconcile.
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: fmt.Errorf("haven't read clickhouse scheduler user credentials yet, this should be a transient error that resolves on next reconcile"),
			},
		}
	}

	user, err := state.ClickHouse.GetSchedulerCredentials()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: err,
			},
		}
	}
	current := clickhouse.ClickHouse(cr, user)

	if i.Teardown {
		actions := []common.Action{}
		actions = append(actions, common.GenericDeleteAction{
			Ref: current,
			Msg: "clickhouse CR",
		})
		// make sure the CR is correctly deleted after the operator is done
		// cleaning up all associated resources
		if state.ClickHouse.Cluster != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.ClickHouse.Cluster,
				Msg: "check clickhouse CR is gone",
			})
		}
		return actions
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: current,
			Msg: "Clickhouse CR",
			Mutator: func() error {
				return clickhouse.ClickHouseMutator(cr, current, user)
			},
		},
	}
}

func (i *ClickHouseReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouse.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse cluster servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse cluster servicemonitor",
		Mutator: func() error {
			return clickhouse.ServiceMonitorMutator(cr, monitor)
		},
	}
}
