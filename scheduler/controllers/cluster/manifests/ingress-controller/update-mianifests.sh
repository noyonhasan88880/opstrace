#!/usr/bin/env bash

set -eou pipefail

# https://github.com/stakater/Reloader/releases

TRAEFIK_VER=v23.2.0
TRAEFIK_FA_PLUGIN_VER=main

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

TMPDIR="$(mktemp -d)"
trap 'rm -rf -- "$TMPDIR"' EXIT

# NOTE(prozlach): CM size limit is 1MB, but it should be enough for us for now.
# Once we start using bigger codebases, we can switch to using init-containers
# which pull data directly from the repo during traefik's start.
git clone --depth=1 https://gitlab.com/gitlab-org/opstrace/traefik-plugin-gob-forward-auth.git $TMPDIR/gob-forward-auth/
pushd $TMPDIR/gob-forward-auth/
git ls-files | grep -vE 'go.mod|go.sum|*.go$|.traefik.yml' | xargs rm -v
sed -i 's/^import: gitlab.com\/gitlab-org\/opstrace\/gobforwardauth$/import: gobforwardauth/g' .traefik.yml
kubectl create \
    -n default configmap traefik-plugin-gob-forward-auth \
    --from-file . \
    --from-literal=commitSHA=$(git rev-parse --short HEAD) \
    -o yaml \
    --dry-run=client \
        > ${SCRIPT_DIR}/cm_traefik-plugin-gob-forward-auth.yaml
popd

#NOTE(prozlach): GET redirects get 301-redirect, all other methods (POST, PUT,
# etc...) get 308.
cat <<EOF > $TMPDIR/values.yaml
globalArguments:
 - --global.sendanonymoususage=false
 - --global.checknewversion=false

additionalArguments:
 - --entrypoints.web.transport.respondingtimeouts.idletimeout=3700s
 - --entrypoints.websecure.transport.respondingtimeouts.idletimeout=3700s
 - --experimental.localPlugins.gob-forward-auth.moduleName=gobforwardauth
 - --experimental.plugins.htransformation.modulename=github.com/tomMoulard/htransformation
 - --experimental.plugins.htransformation.version=v0.2.7
 - --entryPoints.web.http.redirections.entryPoint.to=websecure
 - --entryPoints.web.http.redirections.entryPoint.scheme=https
 - --entryPoints.web.http.redirections.entryPoint.permanent=true
 - --providers.file.filename=/config/dynamic-tls.toml

logs:
  general:
    level: WARN
  access:
    enabled: true
    fields:
      general:
        defaultmode: keep
        names:
          ClientUsername: drop
      headers:
        defaultmode: keep
        names:
          User-Agent: keep
          Authorization: drop
          Content-Type: keep

experimental:
  plugins:
    enabled: true

providers:
  kubernetesCRD:
    enabled: true
    allowCrossNamespace: true

  kubernetesIngress:
    enabled: true
    publishedService:
      enabled: true

volumes:
 - type: configMap
   name: traefik-plugin-gob-forward-auth
   mountPath: /plugins-local/src/gobforwardauth/
 - type: configMap
   name: traefik-dynamic-tls-conf
   mountPath: "/config"
   readOnly: true
 - type: secret
   name: https-cert
   mountPath: "/certs"
   readOnly: true
EOF
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm template \
    --name-template=traefik \
    --values $TMPDIR/values.yaml \
    --version $TRAEFIK_VER \
        traefik/traefik > ${SCRIPT_DIR}/bundle.yaml

APP_VERSION=$(helm search repo traefik/traefik --version ${TRAEFIK_VER} -o json | jq -r '.[0].app_version')
curl https://raw.githubusercontent.com/traefik/traefik/v2.10/docs/content/reference/dynamic-configuration/kubernetes-crd-definition-v1.yml -o ${SCRIPT_DIR}/kubernetes-crd-definition-v1.yaml
curl https://raw.githubusercontent.com/traefik/traefik/v2.10/docs/content/reference/dynamic-configuration/kubernetes-crd-rbac.yml -o ${SCRIPT_DIR}/kubernetes-crd-rbac.yaml

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
