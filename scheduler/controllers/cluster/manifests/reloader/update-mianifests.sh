#!/usr/bin/env bash

set -eou pipefail

# https://github.com/stakater/Reloader/releases

VER=v1.0.1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

helm repo add stakater https://stakater.github.io/stakater-charts
helm repo update
helm template \
    --name-template=reloader \
    --version $VER \
    --set reloader.deployment.containerSecurityContext.allowPrivilegeEscalation=false \
    --set reloader.watchGlobally=false \
        stakater/reloader > ${SCRIPT_DIR}/bundle.yaml

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
