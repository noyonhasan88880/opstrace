#!/usr/bin/env bash

set -eou pipefail

# https://github.com/kubernetes-sigs/external-dns/releases
CHART_VERSION=v1.12.2

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

helm repo add external-dns https://kubernetes-sigs.github.io/external-dns/
helm repo update
helm template \
    --name-template external-dns \
    --version $CHART_VERSION \
    --set fullnameOverride=external-dns \
    --set serviceAccount.annotations.app=external-dns \
    --set serviceMonitor.enabled=true \
    --set sources={service} \
    --set policy=sync \
    --set resources.limits.cpu=200m \
    --set resources.limits.memory=200Mi \
    --set resources.requests.cpu=100m \
    --set resources.requests.memory=100Mi \
    --set securityContext.allowPrivilegeEscalation=false \
    --set env[0].name=app \
    --set env[0].value=external-dns \
    external-dns/external-dns > ${SCRIPT_DIR}/bundle.yaml

# delete --provider=aws from rendered Deployment, handled dynamically inside controller
sed -i.bak '/--provider=aws/d' ${SCRIPT_DIR}/bundle.yaml; rm *.bak

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
