# external-dns

These sets of `kustomize` files are used to deploy an instance of `external-dns` within GitLab Observability Backend.

## Updating

When updating the chart/app-version, you can use `update-manifests.sh` to generate new files from upstream `external-dns` charts.

Please refer to for more details:

- https://github.com/kubernetes-sigs/external-dns/releases
- https://github.com/kubernetes-sigs/external-dns/tree/master/charts/external-dns

## Important

The concerned upstream helm-chart renders a `--provider=aws` by default which makes it hard to dynamically add patches later on from inside the `scheduler`. As a workaround, we just delete this addition from the rendered `Deployment` object and add them later during reconciliation.