#!/usr/bin/env bash

set -eou pipefail

# https://github.com/prometheus-operator/prometheus-operator/releases
VER=v1.2.4
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

kustomize build "github.com/spotahome/redis-operator/manifests/kustomize/overlays/default?ref=$VER" > ${SCRIPT_DIR}/bundle.yaml

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
