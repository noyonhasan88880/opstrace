package tenant

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type TenantState struct {
	Groups *GroupsState
}

func NewTenantState() TenantState {
	return TenantState{}
}

func (i *TenantState) Read(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	return i.readGroupsState(ctx, cr, client)
}

func (i *TenantState) readGroupsState(ctx context.Context, cr *v1alpha1.Tenant, client client.Client) error {
	i.Groups = NewGroupsState()
	return i.Groups.Read(ctx, cr, client)
}
