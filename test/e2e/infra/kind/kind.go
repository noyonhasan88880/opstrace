package kind

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"k8s.io/client-go/rest"
	"k8s.io/utils/pointer"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/yaml"
)

// Kind is mostly a noop implementation as we expect
// the kind cluster to be already running.
// It is equivalent of "bring your own kubeconfig" mode.
type Kind struct {
	config common.Configuration
}

func New() *Kind {
	return &Kind{}
}

func (g *Kind) LoadConfiguration() (common.Configuration, error) {
	cfg := common.Configuration{}
	schedulerImage, err := common.GetEnv(common.TestEnvSchedulerImage)
	if err != nil {
		return cfg, err
	}

	gobHost, err := common.GetEnv("TEST_GOB_HOST")
	if err != nil {
		gobHost = "localhost"
	}

	gitlabHost, err := common.GetEnv("TEST_GITLAB_HOST")
	if err != nil {
		gitlabHost = "gitlab.com"
	}

	// these default to https if not set
	gobScheme := os.Getenv("TEST_GOB_SCHEME")
	gitlabScheme := os.Getenv("TEST_GITLAB_SCHEME")

	gitlabAdminToken, err := common.GetEnv("TEST_GITLAB_ADMIN_TOKEN")
	if err != nil {
		gitlabAdminToken = "not_set!"
	}

	g.config = common.Configuration{
		GOBHost:        gobHost,
		GitLabHost:     gitlabHost,
		GOBScheme:      gobScheme,
		GitLabScheme:   gitlabScheme,
		InstanceName:   "kind",
		SchedulerImage: schedulerImage,
		// We're not deploying GitLab in this case
		GitLabImage:      "",
		GitLabAdminToken: gitlabAdminToken,
	}
	return g.config, nil
}

func (g *Kind) InternalConfiguration() (map[string]string, error) {
	// nothing to see here
	return map[string]string{}, nil
}

func (g *Kind) CreateGitLabInstance() error {
	return nil
}

func (g *Kind) DestroyGitLabInstance() error {
	return nil
}

func (g *Kind) CreateK8sCluster() error {
	return nil
}

// GetRestConfig returns the default go-client rest config.
// See https://pkg.go.dev/sigs.k8s.io/controller-runtime/pkg/client/config#GetConfig for more details.
func (g *Kind) GetRestConfig(_ context.Context) (*rest.Config, error) {
	c, err := config.GetConfig()
	if err != nil {
		return nil, fmt.Errorf("get rest config: %w", err)
	}
	return c, nil
}

func (g *Kind) DestroyK8sCluster() error {
	// do nothing right now
	return nil
}

func (g *Kind) FetchClusterDefinition(_ context.Context) (*schedulerv1alpha1.Cluster, error) {
	clusterObjPath, err := filepath.Abs("../../../scheduler/config/examples/Cluster.yaml")
	if err != nil {
		return nil, fmt.Errorf("unable to resolve relative path to base cluster object data: %w", err)
	}

	clusterObjBytes, err := os.ReadFile(clusterObjPath)
	if err != nil {
		return nil, fmt.Errorf("unable to load base cluster object data: %w", err)
	}

	res := new(schedulerv1alpha1.Cluster)
	err = yaml.Unmarshal(clusterObjBytes, res)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal cluster object data: %w", err)
	}

	res.Spec.Target = gocommon.KIND
	res.Spec.DNS.Domain = pointer.String(g.config.GOBHost)
	res.Spec.GitLab.InstanceURL = g.config.GitLabAddress()

	return res, nil
}

func (g *Kind) Close() error {
	// do nothing right now
	return nil
}
