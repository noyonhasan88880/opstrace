package common

import (
	"fmt"
	"os"
)

// GetEnv returns and environment variable if set or an error if not.
func GetEnv(key string) (string, error) {
	if val, ok := os.LookupEnv(key); ok {
		return val, nil
	}
	return "", fmt.Errorf("environment variable %s not set", key)
}

// GetGitLabImage returns the GitLab image to use for the tests.
// Defaults to gitlab/gitlab-ee:latest.
func GetGitLabImage() string {
	const latest = "gitlab/gitlab-ee:latest"
	v, err := GetEnv(TestEnvGitLabImage)
	if err != nil {
		return latest
	}
	return v
}
