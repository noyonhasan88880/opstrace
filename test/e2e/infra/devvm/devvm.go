package devvm

import (
	"context"
	"fmt"

	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/e2e/infra/common"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

type Devvm struct {
	config  common.Configuration
	cluster *schedulerv1alpha1.Cluster
}

func New() *Devvm {
	return &Devvm{}
}

//nolint:unparam
func (g *Devvm) LoadConfiguration() (common.Configuration, error) {
	gitlabAdminToken, err := common.GetEnv("TEST_GITLAB_ADMIN_TOKEN")
	if err != nil {
		gitlabAdminToken = "not_set!"
	}

	g.config = common.Configuration{
		GOBHost:      "gob.devvm",
		GitLabHost:   "gdk.devvm:3443",
		GOBScheme:    "https",
		GitLabScheme: "https",
		InstanceName: "kind",
		// We're not deploying GitLab in this case
		GitLabImage:      "",
		GitLabAdminToken: gitlabAdminToken,
	}
	return g.config, nil
}

func (g *Devvm) InternalConfiguration() (map[string]string, error) {
	// nothing to see here
	return map[string]string{}, nil
}

func (g *Devvm) CreateGitLabInstance() error {
	return nil
}

func (g *Devvm) DestroyGitLabInstance() error {
	return nil
}

func (g *Devvm) CreateK8sCluster() error {
	return nil
}

// GetRestConfig returns the default go-client rest config.
// See https://pkg.go.dev/sigs.k8s.io/controller-runtime/pkg/client/config#GetConfig for more details.
func (g *Devvm) GetRestConfig(_ context.Context) (*rest.Config, error) {
	c, err := config.GetConfig()
	if err != nil {
		return nil, fmt.Errorf("get rest config: %w", err)
	}
	return c, nil
}

func (g *Devvm) DestroyK8sCluster() error {
	// do nothing right now
	return nil
}

func (g *Devvm) FetchClusterDefinition(ctx context.Context) (*schedulerv1alpha1.Cluster, error) {
	// NOTE(prozlach): Devvm already provisions its own cluster object that is
	// better suited for its infra/configuration. We also want to keep the
	// coupling of e2e tests and devvm at minimum. Hence we fetch the cluster
	// that is running on devvm during the first call which is guaranteed to be
	// called before any destructive test executed.
	if g.cluster == nil {
		restConfig, err := g.GetRestConfig(ctx)
		if err != nil {
			return nil, fmt.Errorf("failed to fetch k8s cluster rest config: %w", err)
		}

		k8sClient, err := client.New(
			restConfig,
			client.Options{
				Scheme: scheme.Scheme,
			},
		)
		if err != nil {
			return nil, fmt.Errorf("unable to create k8s client: %w", err)
		}

		key := client.ObjectKey{
			Name: "dev-cluster",
		}
		tmp := new(schedulerv1alpha1.Cluster)
		err = k8sClient.Get(ctx, key, tmp)
		if err != nil {
			return nil, fmt.Errorf("unable to fetch cluster definition from devvm: %w", err)
		}
		g.cluster = new(schedulerv1alpha1.Cluster)
		g.cluster.Spec = tmp.Spec
		g.cluster.Name = tmp.Name
	}

	return g.cluster.DeepCopy(), nil
}

func (g *Devvm) Close() error {
	// do nothing right now
	return nil
}
