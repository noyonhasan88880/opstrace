#!/usr/bin/env bash

# cleans up all A and TXT recordsets in the testplatform zone.
# our terraform does not clean up zones, as they are managed by externaldns.
# warning: do not run this while a test is running.

set -u

project_id=${PROJECT_ID:?GCP project ID must be set}
echo "Project ID: $project_id"

zone=${DNS_ZONE:?DNS zone must be set}
echo "DNS Zone: $zone"

recordsets="$(gcloud dns record-sets list --project "$project_id" --zone "$zone" --format=json)"

todelete="$(echo "$recordsets" | jq 'map(select(.type=="TXT" or .type=="A"))')"
if [[ "$todelete" == "[]" ]]; then
  echo "No recordsets to delete"
  exit 0
fi

names="$(echo "$todelete" | jq -r '.[].name')"
echo "Recordsets to delete:"
echo "$names"

tsv="$(echo "$todelete" | jq -r '.[] | [.name, .type] | @tsv')"
echo "$tsv" | while IFS=$'\t' read -r name type; do
  gcloud dns record-sets delete "$name" --project "$project_id" --zone "$zone" --type "$type"
done