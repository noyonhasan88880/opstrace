it("setup has all correct environment variables", () => {
  expect(Cypress.env("GITLAB_ADDRESS")).to.not.be.empty;
  expect(Cypress.env("GITLAB_USERNAME")).to.not.be.empty;
  expect(Cypress.env("GITLAB_PASSWORD")).to.not.be.empty;
});
