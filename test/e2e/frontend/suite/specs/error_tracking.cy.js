import * as Sentry from "@sentry/browser";

describe("error tracking", () => {
  beforeEach(() => {
    expect(Cypress.env("SENTRY_DSN")).to.not.be.empty;
    expect(Cypress.env("ERROR_TRACKING_PROJECT_PATH")).to.not.be.empty;

    cy.loginViaUi(
      Cypress.env("GITLAB_USERNAME"),
      Cypress.env("GITLAB_PASSWORD")
    );
  });

  it("error tracking smoke test", async () => {
    const sentryDSN = Cypress.env("SENTRY_DSN");
    const projectPath = Cypress.env("ERROR_TRACKING_PROJECT_PATH");

    cy.log(`Sentry DSN: ${JSON.stringify(sentryDSN)}`);

    Sentry.init({
      dsn: sentryDSN,
      tracesSampleRate: 1.0,
    });

    // reports an error
    failureFromE2ETest();

    cy.visit(`${projectPath}/-/error_tracking`);

    // check error appears in the error list
    cy.contains("Error failureFromE2ETest", { timeout: 20000 })
      .should("be.visible")
      .contains("Error")
      .click();

    // sanity check for error details
    cy.contains("Users").should("be.visible");
    cy.contains("Events").should("be.visible");
    cy.contains("Stack trace").should("be.visible");
    cy.contains("error_tracking.cy.js").should("be.visible");
  });
});

function failureFromE2ETest() {
  Sentry.captureException(new Error("Test failure"));
}
