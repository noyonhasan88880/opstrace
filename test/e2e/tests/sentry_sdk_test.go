package tests

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/testcontainers/testcontainers-go"
	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/pkg/containers"
)

const (
	// Our tag for the sentry SDK example images generated from the test/sentry-sdk app.
	// Don't use "latest", it will produce inconsistent results.
	// Set env var TEST_SENTRY_IMAGE_TAG to override.
	TestSentryImageTag = "0.3.0-634d71fb"
)

type errorResp struct {
	Status      string `json:"status,omitempty"`
	ProjectID   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	EventCount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

var _ = Describe("sentry sdk", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)
	beforeAllConfigureGitLab()

	Context("on running the language specific harness", func() {

		initialErrorCount := 0

		var containerRequests []testcontainers.ContainerRequest

		BeforeEach(func(ctx SpecContext) {
			initialErrorCount = countProjectErrors()

			containerNetworks := []string{}
			if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
				p, err := testcontainers.ProviderDefault.GetProvider()
				Expect(err).NotTo(HaveOccurred())
				if _, err := p.GetNetwork(ctx, testcontainers.NetworkRequest{
					Name: "kind",
				}); err == nil {
					containerNetworks = []string{"kind"}
				}
			}

			// TEST_SENTRY_IMAGE_TAG can be set to override the default tag.
			// useful when developing the test/sentry-sdk apps.
			tag := gocommon.GetEnv("TEST_SENTRY_IMAGE_TAG", TestSentryImageTag)

			containerRequests = containers.GetTestContainers(
				gitLab.errorTrackingConfig.clientKey.SentryDsn,
				true, tag)
			for i, r := range containerRequests {
				cr := r
				cr.Networks = containerNetworks
				containerRequests[i] = cr
			}
		})

		It("sample containers should successfully send error to GOB", func(ctx SpecContext) {
			By("Sending Errors to GOB from SDK containers")
			for _, req := range containerRequests {
				By("Starting container: " + req.Name)

				container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
					ContainerRequest: req,
					Started:          true,
				})
				Expect(err).ToNot(HaveOccurred())

				logs, errLogs := container.Logs(ctx)
				Expect(errLogs).ToNot(HaveOccurred())
				all, errLogs := io.ReadAll(logs)
				Expect(errLogs).ToNot(HaveOccurred())
				GinkgoWriter.Printf("container logs:\n---->\n%s<----\n\n", string(all))
				state, errState := container.State(ctx)
				Expect(errState).ToNot(HaveOccurred())
				GinkgoWriter.Printf("state : %+v\n", state)
				By("Ensuring that the container exited cleanly")
				Expect(state.ExitCode).To(Equal(0), "exit code should be 0")
				container.Terminate(ctx)
			}

			By("Verifying that the errors were sent to GOB")

			Eventually(func(g Gomega) {
				// TODO(joe): actually verify error contents here, not just the number.
				g.Expect(countProjectErrors()).To(Equal(initialErrorCount + len(containerRequests)))
			}, time.Second*10, time.Second).Should(Succeed())
		})
	})
})

func countProjectErrors() int {
	By("Listing Errors From GOB")
	GinkgoWriter.Println("List Errors Endpoint", gitLab.errorTrackingConfig.listEndpoint)
	req, err := http.NewRequest(http.MethodGet, gitLab.errorTrackingConfig.listEndpoint, nil)
	Expect(err).ToNot(HaveOccurred())
	req.Header.Add("Private-Token", gitLab.groupReadOnlyToken.Token)
	resp, err := httpClient.Do(req)
	Expect(err).ToNot(HaveOccurred())
	defer resp.Body.Close()
	Expect(resp.StatusCode).To(Equal(http.StatusOK))

	var errors []errorResp
	err = json.NewDecoder(resp.Body).Decode(&errors)
	Expect(err).ToNot(HaveOccurred())

	GinkgoWriter.Printf("list error response: %v\n", errors)

	return len(errors)
}
