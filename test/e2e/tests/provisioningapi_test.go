package tests

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/provisioning-api/pkg/provisioningapi"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

func provisioningAPIRequest(method string, token string) *http.Response {
	GinkgoHelper()

	req, err := http.NewRequest(
		method,
		fmt.Sprintf("%s/v3/tenant/%d", infraConfig.GOBAddress(), gitLab.project.ID),
		nil,
	)
	Expect(err).NotTo(HaveOccurred())
	req.Header.Set("private-token", token)

	cl := &http.Client{
		Timeout: 30 * time.Second,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
		cl.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true, //nolint:gosec
			},
		}
	}
	resp, err := cl.Do(req)
	Expect(err).NotTo(HaveOccurred())

	return resp
}

var _ = Describe("provisioning API", Ordered, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)
	beforeAllConfigureGitLab()

	Context("Auth", func() {
		It("returns 302 with an empty token", func() {
			resp := provisioningAPIRequest("GET", "")
			Expect(resp.StatusCode).To(Equal(http.StatusFound))
		})

		It("returns 403 on GET with non-read-write tokens", func() {
			for _, t := range []string{
				gitLab.groupReadOnlyToken.Token,
				gitLab.groupWriteOnlyToken.Token,
			} {
				resp := provisioningAPIRequest("GET", t)
				Expect(resp.StatusCode).To(Equal(http.StatusForbidden))
			}
		})

		It("returns 403 on PUT with non-read-write tokens", func() {
			for _, t := range []string{
				gitLab.groupReadOnlyToken.Token,
				gitLab.groupWriteOnlyToken.Token,
			} {
				resp := provisioningAPIRequest("PUT", t)
				Expect(resp.StatusCode).To(Equal(http.StatusForbidden))
			}
		})
	})

	// NOTE(prozlach): Simple workflow test, more thorough tests are done as
	// part of unittesting.
	It("is able to query and create tenants", func(ctx SpecContext) {
		token := gitLab.groupReadWriteToken.Token
		tenantName := fmt.Sprintf("tenant-%d", gitLab.group.ID)

		By("Verify that getting info about inexistant tenant returns 404")
		resp := provisioningAPIRequest("GET", token)
		Expect(resp.StatusCode).To(Equal(http.StatusNotFound))

		By("Create a tenant")
		resp = provisioningAPIRequest("PUT", token)
		Expect(resp.StatusCode).To(Equal(http.StatusCreated))

		By("Verify creation of tenant using k8s client")
		tenant := new(schedulerv1alpha1.GitLabObservabilityTenant)
		key := client.ObjectKey{
			Name: tenantName,
		}
		Expect(k8sClient.Get(ctx, key, tenant)).To(Succeed())
		Expect(tenant.Spec.TopLevelNamespaceID).To(Equal(int64(gitLab.group.ID)))

		DeferCleanup(func(ctx SpecContext) {
			By("Delete tenant, see if we get 404 again")
			Expect(k8sClient.Delete(ctx, tenant)).To(Succeed())
			// NOTE(prozlach): informer is eventually consistent
			Eventually(func(g Gomega) {
				resp = provisioningAPIRequest("GET", token)
				g.Expect(resp.StatusCode).To(Equal(http.StatusNotFound))
			}).Should(Succeed())
		})

		By("Verify that getting info about existing tenant now returns 200")
		// NOTE(prozlach): informer is eventually consistent
		Eventually(func(g Gomega) {
			resp = provisioningAPIRequest("GET", token)
			g.Expect(resp.StatusCode).To(Equal(http.StatusOK))

			tenantInfo := new(provisioningapi.TenantData)
			err := json.NewDecoder(resp.Body).Decode(tenantInfo)
			g.Expect(err).NotTo(HaveOccurred())

			g.Expect(*tenantInfo.Name).To(Equal(tenantName))
			g.Expect(*tenantInfo.TopLevelNamespaceID).To(Equal(int64(gitLab.group.ID)))
			g.Expect(*tenantInfo.Status).To(Equal("ready"))
		}).Should(Succeed())
	})
})
