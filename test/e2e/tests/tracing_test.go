package tests

import (
	"context"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	. "github.com/onsi/ginkgo/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"

	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	query "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

// service name for generated spans compatible query
const tracerServiceName = "test_otlp_tracing_api"

var _ = Describe("Tracing", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)
	beforeAllConfigureGitLab()

	tenant := &schedulerv1alpha1.GitLabObservabilityTenant{}
	beforeAllEnsureGitLabObservabilityTenant(tenant)

	// tracer configured with serviceName to create spans
	var tracer trace.Tracer

	// tokens for accessing the group/project
	var readOnlyToken string
	var writeOnlyToken string

	// send a test span to the otel trace http endpoint
	sendSpans := func(ctx SpecContext, apiKey string, spans ...trace.Span) error {
		opts := []otlptracehttp.Option{
			// #nosec G402
			otlptracehttp.WithTLSClientConfig(&tls.Config{
				InsecureSkipVerify: true,
			}),
			otlptracehttp.WithEndpoint(infraConfig.GOBHost),
			otlptracehttp.WithURLPath(fmt.Sprintf("/v3/%d/%d/ingest/traces", gitLab.group.ID, gitLab.project.ID)),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Private-Token": apiKey,
				},
			),
		}
		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { Expect(exporter.Shutdown(ctx)).To(Succeed()) }()
		return exporter.ExportSpans(ctx, readSpans(spans...))
	}

	// creates a span from the tracer and returns the span context and span.
	// span properties are randomly assigned.
	getSingleSpan := func(ctx SpecContext, tracer trace.Tracer) (context.Context, trace.Span) {
		spanLabel := getRandomSpanLabel()

		kind := randItem(
			trace.SpanKindUnspecified,
			trace.SpanKindInternal,
			trace.SpanKindServer,
			trace.SpanKindClient,
			trace.SpanKindProducer,
			trace.SpanKindConsumer,
		)

		status := randItem(
			codes.Unset,
			codes.Ok,
			codes.Error,
		)

		// provide a unique name for lookup convenience
		operationName := "test_" + spanLabel
		spanCtx, span := tracer.Start(ctx, operationName,
			trace.WithSpanKind(kind),
			trace.WithAttributes(
				attribute.String("testing", spanLabel),
			))
		span.AddEvent("emitting test span", trace.WithAttributes(attribute.String("testing", spanLabel)))
		span.SetStatus(status, "test status")

		By(fmt.Sprintf("created span %s for trace %s",
			span.SpanContext().SpanID(), span.SpanContext().TraceID()))

		return spanCtx, span
	}

	// query a single span using the tracing API
	queryForSpan := func(ctx SpecContext, apiKey string, span trace.Span, matcher OmegaMatcher) {
		rs := readSpan(span)
		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/v3/query/%d/traces", infraConfig.GOBAddress(), gitLab.project.ID), nil)
		Expect(err).NotTo(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)
		q := req.URL.Query()
		q.Add("trace_id", rs.SpanContext().TraceID().String())
		req.URL.RawQuery = q.Encode()

		// rs := readSpan(span)
		// attr := map[string]string{}
		// for _, kv := range rs.Attributes() {
		// 	attr[string(kv.Key)] = kv.Value.AsString()
		// }
		// query := &jaegerapiv3.TraceQueryParameters{
		// 	ServiceName:   tracerServiceName,
		// 	OperationName: rs.Name(),
		// 	Attributes:    attr,
		// 	StartTimeMin: &types.Timestamp{
		// 		Seconds: rs.StartTime().Unix() - 1,
		// 	},
		// 	StartTimeMax: &types.Timestamp{
		// 		Seconds: rs.EndTime().Unix() + 1,
		// 	},
		// 	NumTraces: 1,
		// }

		By("querying for span and checking contents")
		// wait for up to ~60s for span to appear in queries.
		// There should normally be up to a 5s delay for the next batch from the collector to Jaeger,
		// plus another <5s or so for the trace to appear in queries, but waiting longer should avoid flakes.
		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

			traces := &query.TraceResult{}
			g.Expect(json.NewDecoder(resp.Body).Decode(traces)).To(Succeed())
			g.Expect(traces.ProjectID).To(BeNumerically("==", gitLab.project.ID), "trace project id")
			g.Expect(traces.TotalTraces).To(BeNumerically("==", 1), "total traces")
			g.Expect(traces.Traces).To(HaveLen(1), "traces")

			trace := traces.Traces[0]
			hexStringsEqual(g, trace.TraceID, rs.SpanContext().TraceID().String(), "trace id")
			g.Expect(trace.Timestamp.UnixNano()).To(Equal(rs.StartTime().UnixNano()), "start time")
			g.Expect(trace.Spans).To(HaveLen(1), "spans")

			verifySpan(g, rs, &traces.Traces[0].Spans[0])
		}).WithTimeout(time.Minute).Should(matcher)
	}

	makeTracer := func(serviceName string) trace.Tracer {
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(serviceName),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).NotTo(HaveOccurred())
		tp := sdktrace.NewTracerProvider(sdktrace.WithResource(r))
		return tp.Tracer("otel_tracer")
	}

	BeforeAll(func(ctx SpecContext) {
		By("set up a shared tracer for our tests")
		tracer = makeTracer(tracerServiceName)

		readOnlyToken = gitLab.groupReadOnlyToken.Token
		writeOnlyToken = gitLab.groupWriteOnlyToken.Token
	})

	Context("Auth handling", func() {
		Context("Ingest", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "", span)).NotTo(Succeed())
				queryForSpan(ctx, readOnlyToken, span, Not(Succeed()))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "bad"+writeOnlyToken, span)).NotTo(Succeed())
				queryForSpan(ctx, readOnlyToken, span, Not(Succeed()))
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, readOnlyToken, span)).NotTo(Succeed())
				queryForSpan(ctx, readOnlyToken, span, Not(Succeed()))
			})
		})

		Context("Query", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				queryForSpan(ctx, "", span, Not(Succeed()))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				queryForSpan(ctx, "bad"+readOnlyToken, span, Not(Succeed()))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				queryForSpan(ctx, writeOnlyToken, span, Not(Succeed()))
			})
		})
	})

	Context("Spans", func() {
		It("can save and retrieve single span", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx, tracer)
			span.End()
			Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
			queryForSpan(ctx, readOnlyToken, span, Succeed())
		})

		// It("can save and retrieve fragmented spans", func(ctx SpecContext) {\
		// 	By("create chain of nested spans")
		// 	rootContext, rootSpan := getSingleSpan(ctx)
		// 	rootSpan.SetAttributes(attribute.String("fragment", "root"))
		// 	rootSpan.AddEvent("emitting root test span")

		// 	childContext, childSpan := tracer.Start(rootContext,
		// 		"test_"+getRandomSpanLabel(),
		// 		trace.WithSpanKind(trace.SpanKindServer))
		// 	childSpan.SetAttributes(attribute.String("fragment", "child"))
		// 	childSpan.AddEvent("emitting child test span")

		// 	_, childSiblingSpan := tracer.Start(rootContext,
		// 		"test"+getRandomSpanLabel(),
		// 		trace.WithLinks(trace.Link{SpanContext: childSpan.SpanContext()}),
		// 		trace.WithSpanKind(trace.SpanKindProducer))

		// 	_, grandchildSpan := tracer.Start(childContext, "test_"+getRandomSpanLabel())
		// 	grandchildSpan.SetAttributes(attribute.String("fragment", "grandchild"))
		// 	grandchildSpan.AddEvent("emitting grandchild test span")
		// 	grandchildSpan.RecordError(errors.New("error"))
		// 	grandchildSpan.SetStatus(codes.Error, "error")

		// 	grandchildSpan.End()
		// 	childSpan.End()
		// 	rootSpan.End()
		// 	childSiblingSpan.End()

		// 	getTraceID := func(span trace.Span) trace.TraceID {
		// 		return readSpan(span).SpanContext().TraceID()
		// 	}

		// 	traceID := getTraceID(rootSpan)
		// 	// ensure all the trace ids are identical
		// 	Expect(
		// 		[]trace.TraceID{getTraceID(rootSpan), getTraceID(childSpan), getTraceID(grandchildSpan)}).
		// 		Should(HaveEach(traceID))

		// 	By("sending spans separately")
		// 	Expect(sendSpans(ctx, namespaceID, apiKey, rootSpan)).To(Succeed())
		// 	Expect(sendSpans(ctx, namespaceID, apiKey, childSpan, childSiblingSpan)).To(Succeed())
		// 	Expect(sendSpans(ctx, namespaceID, apiKey, grandchildSpan)).To(Succeed())

		// 	client, close := jaegerClient(ctx)
		// 	defer close()

		// 	By("query by trace id")
		// 	var resourceSpans []*tracev1.ResourceSpans
		// 	// allow 1m for spans to appear
		// 	Eventually(func(g Gomega) {
		// 		rcv, err := client.GetTrace(ctx, &jaegerapiv3.GetTraceRequest{
		// 			TraceId: traceID.String(),
		// 		})
		// 		g.Expect(err).NotTo(HaveOccurred())
		// 		chunk, err := rcv.Recv()
		// 		g.Expect(err).NotTo(HaveOccurred())
		// 		g.Expect(chunk.ResourceSpans).To(HaveLen(4))
		// 		resourceSpans = chunk.ResourceSpans
		// 	}).WithTimeout(time.Minute).Should(Succeed())

		// 	By("check returned spans match ours and have correct parent references")
		// 	spans := map[string]*tracev1.Span{}
		// 	for _, s := range resourceSpans {
		// 		Expect(s.InstrumentationLibrarySpans).To(HaveLen(1))
		// 		Expect(s.InstrumentationLibrarySpans[0].Spans).To(HaveLen(1))
		// 		span := s.InstrumentationLibrarySpans[0].Spans[0]
		// 		spans[hex.EncodeToString(span.SpanId)] = span
		// 	}

		// 	// expected span chain to verify parent id references
		// 	spanChain := readSpans(rootSpan, childSpan, grandchildSpan)
		// 	for _, s := range spanChain {
		// 		sID := s.SpanContext().SpanID().String()
		// 		gotSpan, ok := spans[sID]
		// 		Expect(ok).To(BeTrue(), "span map lookup")
		// 		if s.Parent().HasSpanID() {
		// 			Expect(hex.EncodeToString(gotSpan.ParentSpanId)).To(Equal(s.Parent().SpanID().String()))
		// 		}
		// 		verifySpan(s, gotSpan)
		// 	}
		// })
	})

	Context("Query", func() {
		It("can get unique service names", func(ctx SpecContext) {
			By("create tracers with different service names")
			_, s1 := getSingleSpan(ctx, tracer)
			t2 := makeTracer(tracerServiceName + "_2")
			_, s2 := getSingleSpan(ctx, t2)
			t3 := makeTracer(tracerServiceName + "_3")
			_, s3 := getSingleSpan(ctx, t3)

			s1.End()
			s2.End()
			s3.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2, s3)).To(Succeed())

			By("querying for service names")
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/v3/query/%d/services", infraConfig.GOBAddress(), gitLab.project.ID), nil)
			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				g.Expect(res).To(HaveHTTPBody(MatchJSON(`{
					"services": [
						{
							"name": "test_otlp_tracing_api"
						},
						{
							"name": "test_otlp_tracing_api_2"
						},
						{
							"name": "test_otlp_tracing_api_3"
						}
					]
				}`)))
			}, "10s").Should(Succeed())
		})
	})
})

func readSpans(span ...trace.Span) []sdktrace.ReadOnlySpan {
	spans := make([]sdktrace.ReadOnlySpan, len(span))
	for i, s := range span {
		spans[i] = readSpan(s)
	}
	return spans
}

func readSpan(span trace.Span) sdktrace.ReadOnlySpan {
	ro, ok := span.(sdktrace.ReadOnlySpan)
	Expect(ok).To(BeTrue(), "convert to readonly span")
	return ro
}

func getRandomSpanLabel() string {
	return common.RandStringRunes(10)
}

func randItem[A any](items ...A) A {
	//#nosec
	return items[rand.Intn(len(items))]
}

// verify a generated OTEL span against one from the jaeger grpc API.
func verifySpan(g Gomega, a sdktrace.ReadOnlySpan, b *query.SpanItem) {
	tid := a.SpanContext().TraceID()
	hexStringsEqual(g, tid.String(), b.TraceID, "trace id")
	g.Expect(a.Name()).To(Equal(b.Operation), "span name")

	sid := a.SpanContext().SpanID()
	hexStringsEqual(g, sid.String(), b.SpanID, "span id")

	if a.Parent().IsValid() {
		pid := a.Parent().SpanID()
		hexStringsEqual(g, pid.String(), b.ParentSpanID, "parent span id")
	}

	g.Expect(tracerServiceName).To(Equal(b.ServiceName), "service name")

	g.Expect(a.StartTime().UnixNano()).To(BeEquivalentTo(b.Timestamp.UnixNano()), "start time")
	g.Expect(a.EndTime().UnixNano()).To(BeEquivalentTo(
		b.Timestamp.Add(time.Duration(b.DurationNano)*time.Nanosecond).UnixNano()), "end time")

	// we return the status code as a string like "STATUS_CODE_OK"
	g.Expect("STATUS_CODE_"+strings.ToUpper(a.Status().Code.String())).To(Equal(b.StatusCode), "status code")

	// NOTE(joe): various items are not exported from our tracing API
	// These used to be tested when we had a Jaeger backend,
	// referenced here from the OTEL span API:
	// - a.SpanKind()
	// - a.Links()
	// - a.Events()
	// - a.Attributes()
	// - a.SpanContext().TraceState()
	// There are other methods, but these are the most important.
}

// Check two hex strings are equivalent.
// Trace ids 16 bytes and span ids are 8 bytes.
// We cannot assume everything is a uuid.
// Our trace query service returns TraceIDs formatted as uuids
// and SpanIDs as upper case hex strings.
func hexStringsEqual(g Gomega, a, b string, msg ...interface{}) {
	// remove any - chars from potential uuids
	ah := strings.ReplaceAll(a, "-", "")
	bh := strings.ReplaceAll(b, "-", "")
	ab, err := hex.DecodeString(ah)
	g.Expect(err).NotTo(HaveOccurred())
	bb, err := hex.DecodeString(bh)
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(ab).To(Equal(bb), msg...)
}
