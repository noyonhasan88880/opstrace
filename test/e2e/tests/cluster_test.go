package tests

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"

	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/inventory"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

var _ = Describe("cluster provisioning", Ordered, Serial, func() {
	cluster := &schedulerv1alpha1.Cluster{}
	beforeAllEnsureCluster(cluster)

	Context("on creating an example cluster", func() {
		It("ensures it can be created successfully", func(ctx SpecContext) {
			expectClusterPodsReady(ctx)
		})
	})

	Context("ensures it is served over HTTPS", func() {
		It("by serving a valid certificate", func() {
			if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
				Skip("skipping on KIND/Devvm clusters as we use self-signed certs")
			}
			conn, err := tls.Dial("tcp", fmt.Sprintf("%s:443", infraConfig.GOBHost), nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(conn).ToNot(BeNil())

			err = conn.VerifyHostname(infraConfig.GOBHost)
			Expect(err).ToNot(HaveOccurred())

			issuer := conn.ConnectionState().PeerCertificates[0].Issuer
			found := strings.Contains(issuer.String(), "O=Let's Encrypt")
			Expect(found).To(BeTrue())
		})
	})

	Context("ensures when accessing a new namespace", func() {
		It("the user is first authorized", func() {
			r, err := httpClient.Get(fmt.Sprintf("%s/-/1", infraConfig.GOBAddress()))
			Expect(err).ToNot(HaveOccurred())
			defer r.Body.Close()
			Expect(r).To(HaveHTTPStatus(http.StatusOK))
			Expect(r).To(HaveHTTPBody(ContainSubstring("<title>Sign in · GitLab</title>")))
		})
	})

	Context("on deleting the example cluster", func() {
		It("ensures it can be deleted successfully", func(ctx SpecContext) {
			By("collect inventory before deleting")
			inv := []struct {
				u *unstructured.Unstructured
				r schema.GroupVersionResource
			}{}
			for _, v := range cluster.Status.Inventory {
				us, err := inventory.List(v)
				Expect(err).ToNot(HaveOccurred())
				for _, u := range us {
					rm, err := k8sClient.RESTMapper().RESTMapping(u.GroupVersionKind().GroupKind(), u.GroupVersionKind().Version)
					Expect(err).ToNot(HaveOccurred())
					inv = append(inv, struct {
						u *unstructured.Unstructured
						r schema.GroupVersionResource
					}{u: u, r: rm.Resource})
				}
			}

			dc, err := dynamic.NewForConfig(restConfig)
			Expect(err).ToNot(HaveOccurred())

			By(fmt.Sprintf("verify %d inventory items exist in the cluster", len(inv)))
			for _, i := range inv {
				_, err = dc.Resource(i.r).Namespace(i.u.GetNamespace()).Get(ctx, i.u.GetName(), metav1.GetOptions{})
				Expect(err).ToNot(HaveOccurred())
			}

			deleteCustomResourceAndVerify(ctx, cluster)

			By("verify all inventory items have been deleted")
			Eventually(ctx, func(g Gomega, ctx SpecContext) {
				for _, i := range inv {
					_, err = dc.Resource(i.r).Namespace(i.u.GetNamespace()).Get(ctx, i.u.GetName(), metav1.GetOptions{})
					g.Expect(err).To(HaveOccurred())
					g.Expect(k8sErrors.IsNotFound(err)).To(BeTrue(), "expected not found error")
				}
			}, "5m").Should(Succeed())
		})

		// Note(joe): this doesn't seem to work anymore.
		PIt("ensures cluster domain has been cleaned up properly", func() {
			if testTarget == gocommon.KIND || testTarget == gocommon.DEVVM {
				Skip("skipping on KIND/devvm clusters, as they use localhost")
			}

			Eventually(func() bool {
				ips, err := net.LookupIP(infraConfig.GOBHost)
				if err != nil {
					var dnsErr *net.DNSError
					if errors.As(err, &dnsErr) {
						if dnsErr.IsNotFound {
							return true // host not found is good state
						}
					}
					return false
				}
				return len(ips) == 0
			}, time.Minute, 5*time.Second).Should(BeTrue())
		})
	})
})
