package tests

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	gogitlab "github.com/xanzy/go-gitlab"
	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

// gitLab specific configuration for the test run
var gitLab struct {
	once sync.Once
	// random string used for naming GitLab resources
	randStr string
	// client from go-gitlab configured with the test instance and credentials
	client *gogitlab.Client
	// user is set up with the credentials for the test run
	user         *gogitlab.User
	userPassword string
	// user read only token to use for access testing.
	userReadOnlyToken *gogitlab.PersonalAccessToken
	// group is the pre-created group
	group *gogitlab.Group
	// project is the pre-created project in the created group
	project *gogitlab.Project
	// Read-only Observability token for the pre-created group
	groupReadOnlyToken *gogitlab.GroupAccessToken
	// Write-only Observability token for the pre-created group
	groupWriteOnlyToken *gogitlab.GroupAccessToken
	// Read/write Observability token for the pre-created group
	groupReadWriteToken *gogitlab.GroupAccessToken
	// error tracking configuration for the test run
	errorTrackingConfig struct {
		clientKey    *gogitlab.ErrorTrackingClientKey
		listEndpoint string
	}
}

// beforeAllConfigureGitLab sets up GitLab client and other elements
// needed for our feature tests that interact with the associated
// GitLab client.
// sync.Once is used to avoid setting them up multiple times per run.
func beforeAllConfigureGitLab() {
	BeforeAll(func(ctx SpecContext) {
		gitLab.once.Do(func() {
			initGitLab(ctx)
			initTestGroup()
			initErrorTracking()
		})
	})
}

// Init GitLab group/project and sentry related settings.
func initGitLab(ctx context.Context) {
	Expect(infraConfig.GitLabAdminToken).NotTo(BeEmpty())
	// Test asserts actions using gitlab API like enabling error tracking which shouldn't be done on prod env yet.
	Expect(strings.Index(infraConfig.GitLabHost, "gitlab.com")).To(Equal(-1))

	By("Creating a GitLab API client")
	glClient, err := gogitlab.NewClient(
		infraConfig.GitLabAdminToken,
		gogitlab.WithBaseURL(infraConfig.GitLabAddress()),
		gogitlab.WithHTTPClient(httpClient),
		gogitlab.WithRequestOptions(gogitlab.WithContext(ctx)),
		gogitlab.WithCustomLogger(&glClientLogger{}),
	)
	Expect(err).ToNot(HaveOccurred())

	By("Enabling error tracking on GitLab via application settings")
	req, err := glClient.NewRequest(http.MethodPut, "application/settings", nil, nil)
	Expect(err).ToNot(HaveOccurred())

	q := req.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", infraConfig.GOBAddress())
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	// used for a name for all the things
	randStr, err := gocommon.RandStringASCIIBytes(5)
	Expect(err).ToNot(HaveOccurred())

	By("Creating a GitLab user")
	// generate a password that GitLab will accept.
	// it doesn't like pure alpha passwords, even if random.
	userPassword := ""
	for i := 0; i < len(randStr); i++ {
		userPassword += fmt.Sprintf("%d%s", i, string(randStr[i]))
	}
	user, _, err := glClient.Users.CreateUser(&gogitlab.CreateUserOptions{
		Email:            gogitlab.String(fmt.Sprintf("test-%s@%s", randStr, infraConfig.GitLabHost)),
		Password:         gogitlab.String(userPassword),
		Username:         gogitlab.String(randStr),
		Name:             gogitlab.String(randStr),
		SkipConfirmation: gogitlab.Bool(true),
	})
	Expect(err).ToNot(HaveOccurred())

	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	scopes := []string{"read_api"}
	pat, _, err := glClient.Users.CreatePersonalAccessToken(user.ID, &gogitlab.CreatePersonalAccessTokenOptions{
		Name:      gogitlab.String("readonly"),
		ExpiresAt: &expiresAt,
		Scopes:    &scopes,
	})
	Expect(err).ToNot(HaveOccurred())

	gitLab.randStr = randStr
	gitLab.client = glClient
	gitLab.user = user
	gitLab.userPassword = userPassword
	gitLab.userReadOnlyToken = pat
}

// Init a test group and project for this test.
// Create relevant tokens here too.
func initTestGroup() {
	var err error

	By("Setting up test group and project")

	randStr := gitLab.randStr
	glClient := gitLab.client

	By("Create a new group")
	groupName := fmt.Sprintf("test-group-%s", randStr)
	groupPath := fmt.Sprintf("group-%s", randStr)
	gitLab.group, _, err = glClient.Groups.CreateGroup(&gogitlab.CreateGroupOptions{
		Name: &groupName,
		Path: &groupPath,
	})
	Expect(err).ToNot(HaveOccurred())

	By("Adding the user to the group as a developer")
	_, _, err = glClient.GroupMembers.AddGroupMember(gitLab.group.ID, &gogitlab.AddGroupMemberOptions{
		UserID:      &gitLab.user.ID,
		AccessLevel: gogitlab.AccessLevel(gogitlab.DeveloperPermissions),
	})
	Expect(err).ToNot(HaveOccurred())

	By("creating a project in the group")
	projectName := fmt.Sprintf("errortracking-test-%s", randStr)
	gitLab.project, _, err = glClient.Projects.CreateProject(&gogitlab.CreateProjectOptions{
		Name:        &projectName,
		NamespaceID: &gitLab.group.ID,
	})
	Expect(err).ToNot(HaveOccurred())

	By("Setting up Observability Tokens for the group")
	baseScopes := []string{"read_api"}
	readScope := "read_observability"
	writeScope := "write_observability"
	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	readTokenName := fmt.Sprintf("test-e2e-read-%s", randStr)
	writeTokenName := fmt.Sprintf("test-e2e-write-%s", randStr)
	readWriteTokenName := fmt.Sprintf("test-e2e-read-write-%s", randStr)
	createToken := func(name string, scopes []string) *gogitlab.GroupAccessToken {
		t, _, err := glClient.GroupAccessTokens.CreateGroupAccessToken(
			gitLab.group.ID,
			&gogitlab.CreateGroupAccessTokenOptions{
				Name:      &name,
				Scopes:    &scopes,
				ExpiresAt: &expiresAt,
			})
		Expect(err).ToNot(HaveOccurred())
		return t
	}
	gitLab.groupReadOnlyToken = createToken(readTokenName, append(baseScopes, readScope))
	gitLab.groupWriteOnlyToken = createToken(writeTokenName, append(baseScopes, writeScope))
	gitLab.groupReadWriteToken = createToken(readWriteTokenName, append(baseScopes, readScope, writeScope))
}

// Init error tracking related settings.
// Requires initGitLab to be called first.
func initErrorTracking() {
	var err error

	By("Setting up GitLab Error Tracking")

	glClient := gitLab.client

	By("Enabling error tracking on the project")
	// Enable per-project error tracking and get the Sentry URL from settings and test against it.
	// The go-gitlab client doesn't support this API yet, so we use the raw client.
	req, err := glClient.NewRequest(
		http.MethodPut,
		fmt.Sprintf("projects/%d/error_tracking/settings", gitLab.project.ID), nil, nil,
	)
	Expect(err).ToNot(HaveOccurred())
	q := req.URL.Query()
	q.Add("active", "true")
	q.Add("integrated", "true")
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Creating project SENTRY_DSN key")
	key, _, err := glClient.ErrorTracking.CreateClientKey(gitLab.project.ID, nil)
	Expect(err).ToNot(HaveOccurred())

	listErrorEndpoint := fmt.Sprintf("%s/errortracking/api/v1/projects/%d/errors", infraConfig.GOBAddress(), gitLab.project.ID)

	gitLab.errorTrackingConfig.clientKey = key
	gitLab.errorTrackingConfig.listEndpoint = listErrorEndpoint

	s := spew.Sdump(struct {
		ProjectPath        string
		SentryDSN          string
		ListErrorsEndpoint string
	}{
		ProjectPath:        gitLab.project.PathWithNamespace,
		SentryDSN:          key.SentryDsn,
		ListErrorsEndpoint: listErrorEndpoint,
	})
	By("using error tracking configuration: " + s)
}

type glClientLogger struct{}

func (l *glClientLogger) Printf(format string, v ...interface{}) {
	GinkgoWriter.Printf(format+"\n", v...)
}
