# Sentry-based instrumentation for Ruby

This sample app demonstrates how to instrument your code using Sentry SDK for Ruby.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program:

```sh
bundle install
bundle exec ruby app.rb
```

The output from the application should look like:

```
➜  ruby git:(main) ✗ bundle install
Fetching gem metadata from https://rubygems.org/.........
Resolving dependencies...
Using bundler 2.3.24
Fetching concurrent-ruby 1.2.2
Installing concurrent-ruby 1.2.2
Fetching sentry-ruby 5.9.0
Installing sentry-ruby 5.9.0
Bundle complete! 1 Gemfile dependency, 3 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
➜  ruby git:(main) ✗ bundle exec ruby app.rb
I, [2023-05-23T13:02:09.712585 #65282]  INFO -- sentry: ** [Sentry] [Transport] Sending envelope with items [event] bb452c343be14517a9bec654f9f2e9dc to Sentry
I, [2023-05-23T13:02:09.712669 #65282]  INFO -- sentry: ** [Sentry] [Transport] Sending envelope with items [event] ae3bab0913a0420e9de1be356a495a7b to Sentry
```
