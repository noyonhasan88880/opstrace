package containers

import (
	"fmt"
	"strconv"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

const (
	SentryImageRegistry = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk"
	sentryImagePath     = SentryImageRegistry + "/sentry-"
)

// GetTestContainers returns a list of testcontainers that we support.
// The tag is appended to the image names to allow various versions.
func GetTestContainers(sentryDSN string, insecure bool, tag string) []testcontainers.ContainerRequest {
	names := []string{
		"go",
		"ruby",
		"python",
		"nodejs",
		"java",
		"rust",
		"php",
		// Add more here when available...
	}

	containers := make([]testcontainers.ContainerRequest, len(names))

	env := map[string]string{
		"SENTRY_DSN":  sentryDSN,
		"INSECURE_OK": strconv.FormatBool(insecure),
	}

	for i, name := range names {
		containers[i] = testcontainers.ContainerRequest{
			Name:       name,
			Image:      sentryImagePath + name + ":" + tag,
			Env:        env,
			WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
			HostConfigModifier: func(hostConfig *container.HostConfig) {
				hostConfig.ExtraHosts = []string{
					// TODO(prozlach): hardcoded for now, but in theory
					// we should be fetching these IPs from the devvm
					// itself.
					fmt.Sprintf("gdk.devvm:%s", "10.15.17.1"),
					fmt.Sprintf("gob.devvm:%s", "10.15.16.129"),
				}
			},
		}
	}

	return containers
}
