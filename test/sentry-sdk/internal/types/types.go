package types

type ParsedRequest struct {
	UUID          string              `json:"uuid"`
	URL           string              `json:"url"`
	Protocol      string              `json:"proto"`
	Headers       map[string][]string `json:"headers"`
	Body          string              `json:"body"`
	ClientVersion string              `json:"clientVersion"`
	Endpoint      string              `json:"endpoint"`
}
