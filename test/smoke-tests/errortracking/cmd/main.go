package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	errortracking "gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/applogger"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/config"
	httpWrapper "gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/http"
)

const (
	metricsPort = 2112
)

func main() {
	applogger.Init()
	appLog := applogger.Get()

	err := config.InitializeConfig()
	if err != nil {
		appLog.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	go func() {
		appLog.Info("metrics server starting")
		srv := &http.Server{
			Addr:              fmt.Sprintf("0.0.0.0:%d", metricsPort),
			Handler:           mux,
			ReadHeaderTimeout: time.Second * 3,
		}
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				appLog.Fatal("unable to start metric server")
			}
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	ticker := time.NewTicker(time.Duration(config.GetTestsPeriodInSec()) * time.Second)
	defer ticker.Stop()
RUNNER:
	for {
		select {
		case <-c:
			appLog.Infof("caught interrupt, exiting now...")
			break RUNNER
		case <-ticker.C:
			runner := errortracking.ErrorTrackingTestRunner{
				SentryClient: &errortracking.SentryClientWrapper{},
				HTTPClient:   &httpWrapper.RequestWrapper{},
			}
			if err := runner.Setup(); err != nil {
				appLog.Errorf("error-tracking setup failed: %v", err)
				continue RUNNER
			}
			err := runner.RunUptimeTest()
			if err != nil {
				appLog.Errorf("error-tracking smoke test failed: %v", err)
			} else {
				appLog.Infof("error-tracking smoke test OK")
			}
		}
	}
}
