# Error Tracking Smoke Tests (ETST)

This code is used to perform smoke tests for the read and write path of the error tracking application. It constantly tests the error tracking application and generates metrics that are scraped by prometheus.

The gauge metrics generated can be queried in the `/metrics` path. These are:

- smoketest_errortracking_success_write_path: Is set to 1 when the write path of the error tracking smoke test succeeds
- smoketest_errortracking_success_read_path: Is set to 1 when the read path of the error tracking smoke test succeeds

ETST tests the write path by writting an error using the Sentry Go SDK, then the read path is tested by reading the same error back directly from
the ET API.

## Setup

In order to run this application, generate a . Finally you need to find the Sentry DSN url in the settings of gitlab.com under the observability tab. Once you have the API key and the Sentry DSN then you can execute the following commands:
```
export GITLAB_OBSERVABILITY_TOKEN=<your_api_token>
export SENTRY_DSN=<sentry_dsn>
make docker-build
make docker-run
```

You can run the unit tests of this app by running:
```
make unit-tests
```

