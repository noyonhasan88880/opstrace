package config

import (
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

const (
	gitlabObservabilityAPIToken    = "gitlab-pat"
	sentryDSN                      = "sentry_dsn"
	groupErrorTrackingEndpoint     = "error_tracking_endpoint"
	errorTrackingIngestionDelaySec = "2"
	logLevelDebugInt               = int(logrus.InfoLevel)
	testsPeriod                    = "1"
)

func setAllEnvVars() {
	os.Setenv("GITLAB_OBSERVABILITY_API_TOKEN", gitlabObservabilityAPIToken)
	os.Setenv("SENTRY_DSN", sentryDSN)
	os.Setenv("GROUP_ERROR_TRACKING_ENDPOINT", groupErrorTrackingEndpoint)
	os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", errorTrackingIngestionDelaySec)
	os.Setenv("TESTS_PERIOD_SEC", testsPeriod)
}

func TestGetters(t *testing.T) {
	assert := assert.New(t)
	setAllEnvVars()
	assert.Nil(InitializeConfig())

	t.Run("GetGitLabObservabilityAPIToken should return gitlabObservabilityAPIToken", func(t *testing.T) {
		assert.Equal(gitlabObservabilityAPIToken, GetGitLabObservabilityAPIToken())
	})

	t.Run("GetSentryDSN should return sentryDSN", func(t *testing.T) {
		assert.Equal(sentryDSN, GetSentryDSN())
	})

	t.Run("GetGroupErrorTrackingEndpoint should return groupErrorTrackingEndpoint", func(t *testing.T) {
		assert.Equal(groupErrorTrackingEndpoint, GetGroupErrorTrackingEndpoint())
	})

	t.Run("GetErrorTrackingIngestionDelaySec should return errorTrackingIngestionDelaySec", func(t *testing.T) {
		n, _ := strconv.Atoi(errorTrackingIngestionDelaySec)
		assert.Equal(n, GetErrorTrackingIngestionDelaySec())
	})

	t.Run("GetLogLevel should return logLevel", func(t *testing.T) {
		assert.Equal(logLevelDebugInt, GetLogLevel())
	})

	t.Run("GetTestsPeriodInSec should return testsPeriodInSec", func(t *testing.T) {
		n, _ := strconv.Atoi(testsPeriod)
		assert.Equal(n, GetTestsPeriodInSec())
	})
}

func TestInitializeConfig(t *testing.T) {
	assert := assert.New(t)

	t.Run("Should fail if GITLAB_OBSERVABILITY_API_TOKEN is not set", func(t *testing.T) {
		setAllEnvVars()
		os.Unsetenv("GITLAB_OBSERVABILITY_API_TOKEN")
		assert.True(strings.Contains(InitializeConfig().Error(), "GITLAB_OBSERVABILITY_API_TOKEN"))
	})

	t.Run("Should fail if SENTRY_DSN is not set", func(t *testing.T) {
		setAllEnvVars()
		os.Unsetenv("SENTRY_DSN")
		assert.True(strings.Contains(InitializeConfig().Error(), "SENTRY_DSN"))
	})

	t.Run("Should fail if GROUP_ERROR_TRACKING_ENDPOINT is not set", func(t *testing.T) {
		setAllEnvVars()
		os.Unsetenv("GROUP_ERROR_TRACKING_ENDPOINT")
		assert.True(strings.Contains(InitializeConfig().Error(), "GROUP_ERROR_TRACKING_ENDPOINT"))
	})

	t.Run("Should fail if ERROR_TRACKING_INGESTION_DELAY_SEC is not set", func(t *testing.T) {
		setAllEnvVars()
		os.Unsetenv("ERROR_TRACKING_INGESTION_DELAY_SEC")
		assert.True(strings.Contains(InitializeConfig().Error(), "ERROR_TRACKING_INGESTION_DELAY_SEC"))
	})

	t.Run("Should fail if ERROR_TRACKING_INGESTION_DELAY_SEC is set but is not a number", func(t *testing.T) {
		setAllEnvVars()
		os.Setenv("ERROR_TRACKING_INGESTION_DELAY_SEC", "this value should be a number")
		assert.True(strings.Contains(InitializeConfig().Error(), "ERROR_TRACKING_INGESTION_DELAY_SEC needs to be an integer number"))
	})

	t.Run("Should fail if TESTS_PERIOD_SEC is not set", func(t *testing.T) {
		setAllEnvVars()
		os.Unsetenv("TESTS_PERIOD_SEC")
		assert.True(strings.Contains(InitializeConfig().Error(), "TESTS_PERIOD_SEC"))
	})

	t.Run("Should fail if TESTS_PERIOD_SEC is set but is not a number", func(t *testing.T) {
		setAllEnvVars()
		os.Setenv("TESTS_PERIOD_SEC", "this value should be a number")
		assert.True(strings.Contains(InitializeConfig().Error(), "TESTS_PERIOD_SEC needs to be an integer number"))
	})

	t.Run("Should get INFO as default log level", func(t *testing.T) {
		setAllEnvVars()
		assert.Nil(InitializeConfig())
		assert.Equal(int(logrus.InfoLevel), GetLogLevel())
	})

	t.Run("Should get any other log level if respective env var is set", func(t *testing.T) {
		setAllEnvVars()
		os.Setenv("LOG_LEVEL", "Debug")
		assert.Nil(InitializeConfig())
		assert.Equal(int(logrus.DebugLevel), GetLogLevel())
	})
}
