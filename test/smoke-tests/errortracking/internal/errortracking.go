package errortracking

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/applogger"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/config"
	httpWrapper "gitlab.com/gitlab-org/opstrace/opstrace/test/smoke-tests/errortracking/internal/http"
)

const (
	SomeTestErrorText = "smoke-test-gop fake-error"
	Failure           = 0
	Success           = 1
)

var (
	rpErrorTrackingSuccess = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "smoketest_errortracking_success_read_path",
		Help: "Set to 1 if the error tracking read path succeeded",
	})

	wpErrorTrackingSuccess = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "smoketest_errortracking_success_write_path",
		Help: "Set to 1 if the error tracking write path succeeded",
	})

	appLog = applogger.Get()

	// Variable to be filled in by the LDFLAGS.
	// Release is used when sending error events using the Sentry SDK.
	// It refers to this application release.
	release string
)

// TransportWithHooks is an http.RoundTripper that wraps an existing
// http.RoundTripper adding hooks that run before and after each round trip.
type TransportWithHooks struct {
	http.RoundTripper
	Before func(*http.Request) error
	After  func(*http.Request, *http.Response, error) (*http.Response, error)
}

func (t *TransportWithHooks) RoundTrip(req *http.Request) (*http.Response, error) {
	if err := t.Before(req); err != nil {
		return nil, err
	}
	resp, err := t.RoundTripper.RoundTrip(req)
	return t.After(req, resp, err)
}

type ErrorTrackingTestRunner struct {
	SentryClient
	HTTPClient httpWrapper.RequestInterface
	// hold http roundtrip error from the transport hook
	httpErr error
}

func (g *ErrorTrackingTestRunner) Setup() error {
	// We need to send errors to sentry in a sync way in order
	// to set an error before exiting the function in case something goes wrong
	sentrySyncTransport := sentry.NewHTTPSyncTransport()
	// We chose a number for timeout that give us enough confident that smoke-test
	// will not fail unexpectedly
	sentrySyncTransport.Timeout = time.Second * 6

	err := g.SentryClient.Init(sentry.ClientOptions{
		Debug:     (config.GetLogLevel() == int(logrus.DebugLevel) || config.GetLogLevel() == int(logrus.TraceLevel)),
		Release:   release,
		Dsn:       config.GetSentryDSN(),
		Transport: sentrySyncTransport,
		HTTPTransport: &TransportWithHooks{
			RoundTripper: http.DefaultTransport,
			Before: func(req *http.Request) error {
				return nil
			},
			After: func(req *http.Request, resp *http.Response, err error) (*http.Response, error) {
				if err != nil {
					g.httpErr = err
					return nil, err
				}
				if resp != nil {
					appLog.Infof("sending data to error-tracking: %v", resp.Status)
					if resp.StatusCode != http.StatusOK {
						g.httpErr = fmt.Errorf("sending error to error-tracking failed with %v", resp.Status)
						return nil, g.httpErr
					}
				} else {
					g.httpErr = errors.New("sending error to error-tracking failed, nil response")
					return nil, g.httpErr
				}
				return resp, err
			},
		},
	})
	if err != nil {
		return fmt.Errorf("sentry client init: %w", err)
	}
	return nil
}

func (g *ErrorTrackingTestRunner) RunUptimeTest() error {
	// first we smoke test the write path
	fakeError := errors.New(SomeTestErrorText)
	eventID := g.SentryClient.CaptureException(fakeError)
	if g.httpErr != nil {
		appLog.Errorf("Failed to send error: %v", g.httpErr)
		wpErrorTrackingSuccess.Set(Failure)
		return g.httpErr
	}
	appLog.Infof("reported to Sentry: %s - %v", fakeError, *eventID)
	wpErrorTrackingSuccess.Set(Success)

	// then we allow for the error to be ingested
	time.Sleep(time.Duration(config.GetErrorTrackingIngestionDelaySec()) * time.Second)

	// then we smoke test the read path
	if err := g.findError(); err != nil {
		rpErrorTrackingSuccess.Set(Failure)
		return err
	}

	rpErrorTrackingSuccess.Set(Success)
	return nil
}

type ErrorEvent struct {
	Status      string `json:"status,omitempty"`
	ProjectID   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	EventCount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

func (g *ErrorTrackingTestRunner) findError() error {
	url := config.GetGroupErrorTrackingEndpoint()
	req, err := g.HTTPClient.CreateRequest(http.MethodGet, url, nil)
	if err != nil {
		return fmt.Errorf("check read path: %w", err)
	}

	req.Header.Add("Private-Token", config.GetGitLabObservabilityAPIToken())
	getResp, err := g.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("http do: %w", err)
	}
	defer getResp.Body.Close()

	if getResp.StatusCode != http.StatusOK {
		return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", url, getResp.StatusCode, getResp.Status)
	}

	var errorEvents []ErrorEvent
	if err := json.NewDecoder(getResp.Body).Decode(&errorEvents); err != nil {
		return fmt.Errorf("json decode: %w", err)
	}

	var (
		event      ErrorEvent
		eventFound bool
	)
	for _, e := range errorEvents {
		// find the error we previously wrote
		if strings.Contains(e.Description, SomeTestErrorText) {
			appLog.Infof("found %v", SomeTestErrorText)
			event, eventFound = e, true
			break
		}
	}

	if !eventFound {
		return fmt.Errorf("error-tracking: %v not found", SomeTestErrorText)
	}

	// if the event was found, mark it resolved to avoid keeping stale errors
	resolveURL := fmt.Sprintf("%s/%d", url, event.Fingerprint)

	type UpdateErrorEvent struct {
		Status string `json:"status"`
	}
	putBody, err := json.Marshal(UpdateErrorEvent{Status: "resolved"})
	if err != nil {
		return fmt.Errorf("json marshal: %w", err)
	}

	req, err = g.HTTPClient.CreateRequest(http.MethodPut, resolveURL, bytes.NewReader(putBody))
	if err != nil {
		return fmt.Errorf("http create: %w", err)
	}

	req.Header.Add("Private-Token", config.GetGitLabObservabilityAPIToken())
	req.Header.Add("Content-Type", "application/json")
	putResp, err := g.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("http do: %w", err)
	}
	defer putResp.Body.Close()

	if putResp.StatusCode != http.StatusOK {
		//nolint:lll
		return fmt.Errorf("error-tracking: url: %v, status code: %v, message: %v", resolveURL, putResp.StatusCode, putResp.Status)
	}
	appLog.Infof("resolved %d", event.Fingerprint)
	return nil
}
