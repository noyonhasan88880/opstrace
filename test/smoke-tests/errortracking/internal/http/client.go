package http

import (
	"io"
	"net/http"
)

type RequestInterface interface {
	CreateRequest(method, url string, body io.Reader) (*http.Request, error)
	Do(req *http.Request) (*http.Response, error)
	Get(url string) (*http.Response, error)
}

type RequestWrapper struct{}

func (hr *RequestWrapper) Do(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	//nolint:wrapcheck
	return client.Do(req)
}

func (hr *RequestWrapper) CreateRequest(method, url string, body io.Reader) (*http.Request, error) {
	//nolint:wrapcheck
	return http.NewRequest(method, url, body)
}

func (hr *RequestWrapper) Get(url string) (*http.Response, error) {
	client := &http.Client{}
	//nolint:wrapcheck
	return client.Get(url)
}
