# Architecture Overview

This document explains the architecture of Gitlab Observability Platform (GOP).
To learn more about specific aspects of the stack, refer to the documents below:

* [Infrastructure overview](./infrastructure.md) - description of different environments where GOP is deployed.
* [Error tracking](./error-tracking.md) - details of how error tracking works.
* [Tracing](./tracing.md) - details of how tracing works.

## High-level view

The following diagram is a high-level depiction of GOP components and how they interact with each other.

![Architecture_overview](../assets/architecture_overview.png)

The main component responsible for provisioning is the Scheduler operator.
It comprises two controllers:

* `Cluster` controller
* `GitLabNamespace` controller

These controllers are tightly aligned with the implementation of multi-tenancy in GOP.

## Custom Resource Definitions (CRDs)

Scheduler includes a few accompanying CRDs:

![Architecture_overview](../assets/architecture_crds.png)

### Cluster

The `Cluster` object depicts infrastructure shared between `Tenants` and also references the connected GitLab application and OAuth application secrets. This allows GitLab and GOP to provide a seamless experience. It is reconciled by the `Cluster` controller.

We currently support only a single `Cluster` object for each Kubernetes cluster.

### Tenant

Usernames and group names fall under a special category called namespaces in GitLab (see [Types of namespaces](https://docs.gitlab.com/ee/user/namespace/#types-of-namespaces).
This Custom Resource represents a root level namespace in GitLab.
For instance, the GitLab Observability group: `https://gitlab.com/gitlab-org/opstrace` is a subgroup within the root level namespace `gitlab-org`.
The tenant operator owns this group and provisions a GitLab Observability UI instance for each `Tenant` Custom Resource.
Each root level namespace in GitLab that enables observability has its own instance of GitLab Observability UI for managing plugins and datasources in isolation.

Root level namespaces in GitLab are where account level capabilities are determined (by the namespace’s license).
Tenants map to root level namespaces in GitLab to the extent that limits/capabilities can also be determined by the GitLab license for the associated root namespace in GitLab.

Resources such as the UI and Jaeger/OTel are deployed per tenant.

The `Tenant` name is the same, because the All `Tenant` specific resources are created in a Kubernetes `Namespace` of the same name.

NOTE: ProjectIDs may overlap with NamespaceIDs (Users/Groups). Therefore it is possible to provision a Tenant for UserID `6` while ProjectID `6` also exists.

### Group

The Group Custom Resource represents a namespace in a GitLab namespace tree.
`gitlab-org` has its own Tenant with its own instance of GitLab Observability UI, and the group GitLab Observability is managed as a group within that tenant.
A group is synonymous with a [GOUI group](https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/issues/4), and a group will have its own APIs for tracing, metrics, and logs. Users log in to that group in the UI to visualize and manage their data.

### GitLabNamespace

GitLabNamespaces are created by Gatekeeper and reconciled by the `GitLabNamespace` controller in Scheduler.
Gatekeeper receives a request to enable observability for a GitLab namespace (any group or user namespace). If the correct permissions are supplied, Gatekeeper creates or updates the GitLabNamespace Custom Resource that represents the GitLab namespace.
This keeps namespaces up to date in GitLab Observability as they change in GitLab.
Rate limits, integrations, [Gitlab Observability UI plugins, Datasources and Dashboards](https://gitlab.com/gitlab-org/opstrace/opstrace-ui), are managed on a per-GitlabNamespace basis.

Each `GitLabNamespace` stores its top-level namespace ID, which is also the ID of the root namespace `Tenant` and determines associated resources which are created (if not already present).

## Multi-Tenant Architecture Overview

Each tenant in GitLab Observability has its own highly available [GitLab Obervability UI](https://gitlab.com/gitlab-org/opstrace/opstrace-ui).

Subgroups within the GitLab root-level namespace map to organizations in the UI instance in GitLab Observability Platform (represented by the Group Custom Resource in the diagram).
“Organizations” are renamed in Gitlab Observability UI to “groups” to align the concepts explicitly.

Inside an GitLab Observability tenant, an ingress resource ensures the correct headers are set for the data ingestion path.
A group’s ingestion path may be: `https://opstrace.gitlab.com/api/{groupId}/metrics`, and the ingress sets the OrgId header equal to the groupId. This maintains data separation between groups.

Namespaces in GitLab Observability are only created on an as-needed basis.
For a namespace mapped in GitLab that hasn't yet enabled observability, if a user navigates to it, Gatekeeper checks their access level. If they are an Owner, they are given the option to enable observability.

## Scheduler

As mentioned previously, Scheduler is the central component of GOP that includes two major components:

* `Cluster` controller
* `GitLabNamespace` controller

Scheduler's role is to launch and supervise all the components of GOP. In turn, these components take care of tenants, route traffic, and perform other tasks.

As we work toward sharding in the Observability SaaS (the ability to deploy several GitLab Observability clusters in different regions), the Scheduler operator will also communicate with the sharding control plane on its available capacity and to receive assignments for tenants it should provision.

### `Cluster` controller

`Cluster` controller is one of two controllers comprising the scheduler.
It launches and supervises components that are shared between tenants, namely:

* Prometheus-operator, which in turn launches a Prometheus instance
* nginx-based ingress
* clickhouse operator, which in turn launches a clickhouse cluster
* cert-manager
* gatekeeper
* jaeger-operator
* errortracking-api
* Prometheus node exporter
* redis operator, which in turn launches a redis instance

`Cluster` controller reconciles the built-in manifests of the above components, with overrides specified in the `Cluster` Custom Resource and the state of APIserver.
It then issues API CRUD calls so the state of the cluster matches the desired state.

The benefits of this approach are that:

* User-made changes are reverted by the controller.
* There's a central place (that is `Cluster` CRD) where all GOP configuration resides.
* It over-provisioning of tenants, based on the available resources within the Kubernetes cluster.

#### `Cluster` Overrides

A built-in override function means that overrides can be provided directly on the Custom Resource (CR).
The controller merges the overrides, allowing an SRE to override any attribute of any Kubernetes resource.
Overrides on the CR make it easier to see what has been overridden.
Here's an example of the Cluster CR that overrides the number of replicas and the image for the gatekeeper deployment:

```yaml
apiVersion: opstrace.com/v1alpha1
kind: Cluster
metadata:
  annotations:
  creationTimestamp: "2022-05-17T19:29:57Z"
  finalizers:
  - cluster.opstrace.com/finalizer
  name: mats-dev-cluster
spec:
  dns:
    acmeEmail: ""
    dns01Challenge: {}
    externalDNSProvider: {}
  gitlab:
    groupAllowedAccess: redacted
    groupAllowedSystemAccess: redacted
    instanceUrl: redacted
    authSecret:
      name: dev-secret
  namespace: ""
  overrides:
    gatekeeper:
      components:
        deployment:
          spec:
            template:
              spec:
                replicas: 5
                containers:
                - image: opstrace/gatekeeper:761da560668a6258f8f6352395b7840078f2dc23
                  name: gatekeeper
        ingress: {}
        service: {}
        serviceMonitor: {}
        statefulset: {}
  target: kind
status:
  conditions:
  - lastTransitionTime: "2022-05-19T21:13:14Z"
    message: All components are in ready state
    observedGeneration: 17
    reason: Success
    status: "True"
    type: Ready
```

### `GitlabNamespace` controller

The `GitLabNamespace` controller is the other controller that comprises the Scheduler.
It reconciles the required tenant capabilities and the subgroup capabilities by deploying a tenant operator. In turn, this tenant operator deploys tenant-level components by:

* Creating necessary databases and users in ClickHouse DB.
* Safely supplying ClickHouse credentials to tenant/tenant components.
* Launching tenant operator for the root namespace, along with GitLab Observability UI.

## `Cluster` components

The following diagram provides a high-level overview of components on the `Cluster` level:

![Cluster_overview](../assets/architecture-cluster.png)

The diagram also depicts communication flows.

### Prometheus and Prometheus Operator

Prometheus operator is launched by scheduler.
In turn, the Prometheus operator is responsible for launching and maintaining a Prometheus instance that monitors other components, including itself and the operator.
The list of monitored objects can be displayed with the following command:

```bash
$ kubectl get servicemonitors.monitoring.coreos.com
NAME                      AGE
apiserver                 4h26m
certmanager               4h28m
clickhouse                4h31m
clickhouse-operator       4h31m
coredns                   4h26m
errortracking-api         4h26m
gatekeeper                4h26m
jaeger-operator           4h28m
kube-controller-manager   4h26m
kube-scheduler            4h26m
kube-state-metrics        4h26m
kubelet                   4h26m
nginx-ingress             4h28m
node-exporter             4h26m
Prometheus                4h26m
Prometheus-operator       4h28m
redis                     4h29m
```

There isn't currently a dashboard, so any queries need to be created dynamically through Prometheus UI.

### Ingress

NGINX is currently used as ingress.
It forwards traffic based on ingress objects to:

* gatekeeper:
  * authentication
* error tracking api:
  * errors ingestion
* tenant:
  * GitLab Observability UI

OAuth traffic flow also goes through NGINX during the authentication and authorization process of the user.

### Redis Operator

Launches Redis instance, which in turn is responsible for caching and session storage for gatekeeper.

### Node-exporter

Exposes Prometheus metrics for all the nodes in the Kubernetes cluster.
Metrics are then pulled by the Prometheus instance.

### Error tracking API

Receives ingestion traffic from nginx-ingress and stores the data in ClickHouse.
The Sentry DSN that you copy and paste from the GitLab UI points to the error tracking instance.

NOTE: Currently there is only one instance for all tenants. This will change as [this MR](https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1697) is in progress and it is in the migration phase.

### Gatekeeper

Gatekeeper is responsible for authentication and authorization and creates `GitLabNamespace` objects that are later reconciled by the scheduler.
Gatekeeper is also the component responsible for the "provisioning namespace" wait-page that you see in quickstart. It counts the time and compares it with the usual time it takes to provision and waits for the _Ready_ status.
Gatekeeper uses Redis for caching and also has an internal in-memory LRU (Least Recently Used).

### ClickHouse

The [ClickHouse operator](../../clickhouse-operator/README.md) deployment is created and a `ClickHouse` Custom Resource is created that defines the ClickHouse cluster.

This results in a replicated ClickHouse cluster. Databases in the cluster are designed to be distributed by default, so replication is handled automatically for each application.

Databases are shared across all tenants. Each schema should contain a tenant ID as part of the primary/sorting key to filter records appropriately.

### Jaeger operator

The Jaeger operator Launches Jaeger instances in the tenant.
The Custom Resources that drive it are created by Scheduler while reconciling the `Cluster` object.

## `GitlabNamespace` components

The following diagram provides a high-level overview of components on the `GitlabNamespace` level:

![Gitlabnamespace_overview](../assets/architecture-tenant.png)

The diagram also depicts communication flows.

### Tenant operator

The Tenant operator is responsible for:

* Configuring dashboards for GitLab Observability UI.
* Launching an instance of GitLab Observability UI.

Also, for every Group that belongs to given RootNamespace, the Tenant operator:

* Launches a Jaeger instance by creating a Jaeger Custom Resource which in turn is reconciled by the Jaeger operator.
* Launches the OTel collector.
* Adds ingress routes for the given group's OTel collector and jaeger ingress.

### Jaeger instance and OTel-collector

These components are responsible for tracing.  
The OTel collector gathers traces, which are then presented by Jaeger UI.
ClickHouse is used as the backend.
