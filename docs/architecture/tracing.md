# Tracing deep dive

This document provides a detailed explanation of how tracing works.

It is assumed that the infrastructure has already been provisioned and the tenant created.
For details on how this is done, see the other docs in the `architecture` folder.

## Overview

The following diagram provides a brief overview of the components of the Gitlab Observability Platform (GOP) that implement tracing.

![Tracing-overview](../assets/tracing_overview.png)

The previous diagram assumes there are two `Group`s deployed: `1st` and `n-th` in the given `GitlabNamespace`.
Each Group has corresponding OTel collector and Jaeger instance. Both are deployed by the tenant operator.

Traces are submitted through ingress to the OTel collector, based on the ID of the Group encoded in the HTTP's requests path.
Ingress is responsible for routing requests to the correct collector.

For example, for the `Group.Id == 6`, there are two ingress objects:

```bash
$ kubectl get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o wide
NAME                     CLASS    HOSTS       ADDRESS     PORTS   AGE
opentelemetry-6          <none>   localhost   localhost   80      6h40m
opentelemetry-jaeger-6   <none>   localhost   localhost   80      6h40m

$ kubectl get ingress -n 6 opentelemetry-6 opentelemetry-jaeger-6 -o jsonpath="{range .items[*].spec.rules[0].http.paths[0]}{.path}{'\n'}{end}"
/v1/traces/6
/v1/jaegertraces/6
```

The OTel collector then submits the traces to the Jaeger instance.

In turn, the Jaeger instance uses the [jaeger-clickhouse](https://github.com/jaegertracing/jaeger-clickhouse) plugin to submit these traces to ClickHouse.

Each collector/Group uses a separate ClickHouse database.

Traces are retrieved from ClickHouse by Jaeger, using the same plugin.

Gitlab Observability UI (GOUI) issues API calls to Jaeger to fetch traces. It does not connect directly to ClickHouse.

GOUI, Jaeger and OTel collectors all use Gatekeeper for authorization and authentication.
The source of truth for authentication and authorization data is the GOUI instance or Gitlab instance itself, depending on whether you use an authentication token or a session cookie.
The Authentication token is used on the ingestion path. The session cookie is used when accessing GOUI/Jaeger through a browser.

Gatekeeper uses Redis for caching so that it does not overwhelm Gitlab instance with API calls.

The instrumented code may use Jaeger SDK to send traces, but this is not recommended.
This is because of the deprecation of Jaeger SDK (see [here](https://www.jaegertracing.io/docs/1.37/client-libraries/)).
The preferred way to send metrics is using OTel SDK for the given language.

## OTel collector

The OTel collector is a customized distribution of [the upstream OTel collector](https://opentelemetry.io/docs/collector/).
Currently, only HTTP ingestion is supported using [OTel](https://github.com/open-telemetry/opentelemetry-specification/blob/main/specification/trace/api.md) and Jaeger protocols.
The metrics are then batched and sent to Jaeger using the Jaeger protocol, where they are also logged.

See [this example tracing app](https://gitlab.com/ankitbhatnagar/opstraceware/-/blob/master/cmd/tracing/main.go) for details of how traces can be sent from the application.
Users may send traces directly to GOP. Alternatively, if there is a need for a single outgoing IP or batching/queueing of metrics, an intermediary OTel collector can be connected to the GOP collector.

Currently, GitLab uses [Labkit](https://gitlab.com/gitlab-org/labkit/), but work is ongoing to either add support for sending metrics in OTel format, or to drop Labkit and use OTel SDK directly (see for example [here](https://gitlab.com/gitlab-org/labkit/-/merge_requests/79#note_437451442)).

## Jaeger

Jaeger is currently used for storing and retrieving traces from ClickHouse.
Jaeger uses the default ClickHouse DB schema, but there is plan to improve it using suggestions from ClickHouse team (see [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1804) and [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1692)).
