<!-- markdownlint-disable MD041 -->
<!-- markdownlint-disable MD033 -->

# GitLab Observability Introduction

The GitLab Observability Distribution is a secure, horizontally-scalable, open source observability platform that you can install in your cloud account.

## What Does it Do?

GitLab Observability automates the creation and management of a secure, horizontally-scalable metrics platform.

## How Can I Use It?

First, give our [Quick Start](./quickstart.md) a try.
It takes about half an hour to spin up the instance, but it's a great way to get a feel for how it works.
Furthermore, you don't often need to set up instances since once it's up and running it manages itself.

Then you can check out our three guides on the left—User, Administrator, and Contributor—for more details on how to create and use an GitLab Observability instance.

Missing something?  Check out our [contributing guides](../README.md#Contributing), thanks for your contributions!
