# Using [GitLab Compose kit](https://gitlab.com/gitlab-org/gitlab-compose-kit)

GCK is a tool that allows you to develop GitLab CE/EE on your local machine using Docker Compose.
It is an alternative to the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).
GCK is a good choice especially for linux users.

Follow the [GCK installation instructions](https://gitlab.com/gitlab-org/gitlab-compose-kit/-/blob/master/README.md).

## Bridging GitLab and GOB

The aim is to have full interconnection between a local GitLab and GOB in `kind`
and to allow non-https connections so we don't have to worry about certificates.

To enable GOB to speak to GitLab and GitLab to speak to GOB, we add the following
configuration to our GOB `Cluster.yaml` to route all `gitlab.local` requests from
Gatekeeper through the docker0 bridge IP which routes traffic to the host machine.

```yaml
overrides:
  gatekeeper:
    patchesJson6902:
      - target:
          kind: Deployment
          name: gatekeeper
        patch: |-
          - op: add
            path: /spec/template/spec/hostAliases
            value:
              - ip: "172.17.0.1" # <- docker0 bridge IP
                hostnames:
                - "gitlab.local"
```

To enable GitLab to speak to GOB from within the docker network, add the following
to the `docker-compose.overrides.yml` in GCK. After adding this config, make sure to
`make restart` in GCK for the changes to take effect.

```yaml
services:
  web:
    extra_hosts:
     - "gob.local:172.17.0.1"

```

You can check your docker0 bridge IP with the following:

```bash
docker network inspect bridge --format='{{(index .IPAM.Config 0).Gateway}}'
172.17.0.1

```

### DNS

We need to add the relevant DNS entries to `/etc/hosts` to allow host resolution:

```bash
127.0.0.1   gitlab.local
127.0.0.1   gob.local
```

### Configuring GitLab

> TODO: automate this as we do for terraform/e2e tests.

To Configure GitLab for GOB use one needs to set up relevant feature flags.

In GCK, run `make console` to access the rails console.

Enable the relevant feature flags, which at the time of writing, are:

```ruby
Feature.enable(:integrated_error_tracking);
Feature.enable(:gitlab_error_tracking);
Feature.enable(:observability_group_tab);
```

You can configure the OAuth application for GOB, as instructed in the [quickstart](../../quickstart.md).
Use `https://gob.local/v1/auth/callback` as the callback URL.

Enable "GitLab Error Tracking" in the admin area using `http://gob.local` as the URL.

### Configuring GOB

See [example Cluster.yaml](./gck/Cluster.yaml) for an example configuration using the above domains.

Ensure you create the referenced `dev-secret` first with the following keys:

```yaml
gitlab_oauth_client_id
gitlab_oauth_client_secret
internal_endpoint_token
```

The simplest way to ensure the secret above is set correctly is to export the following variables before running `make deploy` in the root of the repo:

```bash
export gitlab_oauth_client_id=<your_client_id>
export gitlab_oauth_client_secret=<your_client_secret>
export internal_endpoint_token=<your_internal_endpoint_token>
```

Then, to create the secret and deploy the operator, run:

```bash
make deploy
```
