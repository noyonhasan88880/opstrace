(function () {
    /**
     *   We need to let the enclosing GitLab instance know that the page has loaded.
     */
    window.addEventListener("DOMContentLoaded", function () {
        var e = document.getElementById("status-event");

        window.parent?.postMessage({
            type: "AUTH_COMPLETION",
            // status is one of error|success
            // type: string
            status: e.dataset.status,
            // message is the human readable error or success message
            // type: string
            message: e.dataset.message,
            // statusCode represents the status code of the request,
            // i.e. 401 for unauthorized.
            // type: string
            statusCode: e.dataset.code
        }, "*");
    });
})();