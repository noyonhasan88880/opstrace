package gatekeeper

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
)

const (
	authConfigKey     = "gatekeeper/authConfig"
	gitLabServiceKey  = "gatekeeper/gitLabService"
	authTokenKey      = "gatekeeper/authToken"
	namespaceKey      = "gatekeeper/namespace"
	projectKey        = "gatekeeper/namespace"
	actionKey         = "gatekeeper/action"
	minAccessLevelKey = "gatekeeper/minAccessLevel"
	authMetricsKey    = "gatekeeper/authMetrics"
	randIDKey         = "gatekeeper/randSessionID"
)

// Helper to set the oauth2 client config on the context.
func SetAuthConfig(ctx *gin.Context, cfg *oauth2.Config) {
	ctx.Set(authConfigKey, cfg)
}

// Helper to get the oauth2 client config from the context.
func GetAuthConfig(ctx *gin.Context) *oauth2.Config {
	return ctx.MustGet(authConfigKey).(*oauth2.Config)
}

// Helper to get the metrics from the context.
func GetAuthMetrics(ctx *gin.Context) *AuthMetricsData {
	return ctx.MustGet(authMetricsKey).(*AuthMetricsData)
}

// Helper to set the gitLabService on the context.
func SetGitLabService(ctx *gin.Context, us *GitLabService) {
	ctx.Set(gitLabServiceKey, us)
}

// Helper to get the gitLabService from the context.
func GetGitLabService(ctx *gin.Context) *GitLabService {
	return ctx.MustGet(gitLabServiceKey).(*GitLabService)
}

// Helper to set the authToken in the session.
func SetAuthToken(ctx *gin.Context, token *oauth2.Token) error {
	session := sessions.Default(ctx)
	session.Set(authTokenKey, *token)
	return session.Save()
}

// Helper to get the authToken from the session.
func GetAuthToken(ctx *gin.Context) *oauth2.Token {
	session := sessions.Default(ctx)
	token := session.Get(authTokenKey)
	if token == nil {
		return nil
	}
	//nolint:errcheck
	tok := token.(oauth2.Token)
	return &tok
}

// Helper to clear the authToken in the session.
func ClearAuthToken(ctx *gin.Context) error {
	session := sessions.Default(ctx)
	session.Delete(authTokenKey)
	return session.Save()
}

// Helper to get the target namespaceID from the ctx.
func GetNamespace(ctx *gin.Context) string {
	return ctx.MustGet(namespaceKey).(string)
}
