package gatekeeper

import (
	"fmt"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func HandleGLAccessTokenAuth(ctx *gin.Context, bearerToken string, requiredScopes []string) {
	log.Debug("handling Gitlab Access Token auth path")
	// Make gitLabService available to all down stream handlers
	gitLabService, err := NewGitLabServiceFromAccessToken(ctx, bearerToken)
	if err != nil {
		ctx.AbortWithError(500, fmt.Errorf("create gitlabservice: %w", err))
		return
	}
	SetGitLabService(ctx, gitLabService)

	requestGranted := handleAttachedScopes(ctx, gitLabService, requiredScopes)
	if !requestGranted {
		log.Info(
			"token does not have required scopes granted to allow the request",
			requiredScopes,
		)
		ctx.AbortWithStatus(403)
		return
	}

	log.Debug("HandleGLAccessTokenAuth permitted request, handing over to HandleTokenAuth")
}

func handleAttachedScopes(ctx *gin.Context, gitLabService *GitLabService, requiredScopes []string) bool {
	// NOTE(prozlach): AccessToken auth adds another authorization layer on top
	// the checks that are normally done for the session-based users (i.e.
	// using browser/oauth session). The code checks the scopes of the token
	// before checking the group memberships and membership levels.
	tokenScopes, err := gitLabService.GetAccessTokenScopes()
	if err != nil {
		ctx.AbortWithError(403, fmt.Errorf("fetch scopes assigned to the given token: %w", err))
		return false
	}
	if isSubset(requiredScopes, tokenScopes) {
		return true
	}
	return false
}

func isSubset(setElements, supersetElements []string) bool {
	superset := make(map[string]int)
	for _, value := range supersetElements {
		superset[value] += 1
	}

	for _, value := range setElements {
		if count, found := superset[value]; !found {
			return false
		} else if count < 1 {
			return false
		} else {
			superset[value] = count - 1
		}
	}

	return true
}
