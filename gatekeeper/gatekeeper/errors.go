package gatekeeper

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var ErrUnauthorized = errors.New("unauthorized")

// Returns a 401 if the error is due to an authorization issue,
// or aborts with the error and a generic message back to the user.
func HandleError(ctx *gin.Context, err error) {
	if errors.Is(err, ErrUnauthorized) {
		ctx.AbortWithStatus(401)
	} else {
		ctx.AbortWithError(500, err)
	}
}

// nginx custom errors headers - https://kubernetes.github.io/ingress-nginx/user-guide/custom-errors/.
type CustomErrorsParams struct {
	Code        int    `json:"code" form:"code" binding:"required,numeric,min=400,max=599"`
	OriginalURI string `json:"uri" form:"uri"  binding:"required"`
}

// errorsHandler handles nginx default-backend requests for error status pages.
// see also https://kubernetes.github.io/ingress-nginx/user-guide/default-backend/
// and https://kubernetes.github.io/ingress-nginx/user-guide/custom-errors/.
func errorsHandler(ginCtx *gin.Context) {
	var errorsParams CustomErrorsParams

	if err := ginCtx.ShouldBindQuery(&errorsParams); err != nil {
		log.WithError(err).
			Info("Unable to parse headers for error page")
		// If we can't parse the headers, just return a 404.
		negotiateError(ginCtx, 404, "")
		return
	}

	log.WithField("code", errorsParams.Code).
		WithField("original_uri", errorsParams.OriginalURI).
		WithField("Accept header", ginCtx.GetHeader("Accept")).
		Debug("handling custom error")
	negotiateError(ginCtx, errorsParams.Code, "")
}

// negotiateError handles the negotiation of errors with status and optional message.
func negotiateError(ctx *gin.Context, code int, message string) {
	if code == 0 {
		code = 404
	}

	// call abort to prevent further handlers from being called.
	ctx.AbortWithStatus(code)

	status := http.StatusText(code)
	if status == "" {
		status = "Unknown Error"
	}

	if message == "" {
		message = status
	} else {
		message = status + ": " + message
	}

	ctx.Negotiate(code, gin.Negotiate{
		Offered:  []string{gin.MIMEHTML, gin.MIMEJSON},
		HTMLName: "error.gohtml",
		HTMLData: gin.H{"title": message, "statuscode": code},
		JSONData: gin.H{"message": message},
	})
}

// ErrorLogger middleware to log errors created with gin context.
// e.g. c.Error(err) or c.AbortWithError(500, err).
func ErrorLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		for _, err := range c.Errors {
			log.WithError(err).WithField("request_uri", c.Request.RequestURI).Error()
		}
	}
}
