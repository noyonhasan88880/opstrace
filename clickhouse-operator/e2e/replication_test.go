package e2e

import (
	"crypto/tls"
	"crypto/x509"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

var _ = Describe("MergeTree replication", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse

	// Note: This would be better if we can reference this out of the controller
	const clickhouseCertConst = "clickhouse-cert"

	type row struct {
		Dt   time.Time `ch:"dt"`
		Text string    `ch:"text"`
	}

	insertData := func(ctx SpecContext, db clickhouse.Conn) []row {
		By("inserting rows into distributed table")

		batch, err := db.PrepareBatch(ctx, "INSERT into events.things")
		Expect(err).NotTo(HaveOccurred())

		es := make([]row, 1000)
		t0 := time.Now().UTC()
		txt := []string{"foo", "bar", "baz"}
		for i := 0; i < 1_000; i++ {
			es[i] = row{
				Dt:   t0.Add(time.Second * time.Duration(i)),
				Text: txt[i%3],
			}
		}

		for _, e := range es {
			e := e
			Expect(batch.AppendStruct(&e)).To(Succeed())
		}

		Expect(batch.Send()).To(Succeed())

		return es
	}

	assertReplicaBehaviour := func(ctx SpecContext, es []row, remotePort int, tlsConf *tls.Config) {

		for i := 0; i < int(*clickhouseCluster.Spec.Replicas); i++ {
			db, close := openDBReplica(ctx, clickhouseCluster, i, dbConfig{
				database:  "events",
				tlsConfig: tlsConf,
			})
			defer close()
			Expect(db.Ping(ctx)).To(Succeed())

			Eventually(ctx, func(g Gomega) {
				var res []row
				g.Expect(db.Select(ctx, &res, "SELECT * FROM events.things_local")).Should(Succeed())

				g.Expect(res).To(ConsistOf(es))
			}, time.Second*10, time.Second).Should(Succeed())
		}
	}

	assertReplicatedTableCreation := func(ctx SpecContext, db clickhouse.Conn) {
		// Note(joe): this demonstrates various sane defaults:
		// - Atomic database engine.
		// - Default Replicated* args for zookeeper path and replica name

		Expect(db.Exec(ctx, "CREATE DATABASE events ON CLUSTER '{cluster}'")).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things_local ON CLUSTER '{cluster}' (
				dt DateTime64(9, 'UTC'),
				text String
			) ENGINE = ReplicatedMergeTree
			ORDER BY (dt)`)).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things ON CLUSTER '{cluster}' AS events.things_local
			ENGINE = Distributed('{cluster}', events, things_local)`)).To(Succeed())
	}

	JustBeforeEach(func(ctx SpecContext) {
		assertClusterReady(ctx, clickhouseCluster)
	})

	Context("ReplicatedMergeTree and Distributed tables", func() {
		var db clickhouse.Conn
		var closer func()

		beforeEachCreateCluster(&clickhouseCluster, clusterConfig{})

		JustBeforeEach(func(ctx SpecContext) {
			By("creating replicated and distributed example schemas")

			db, closer = openDB(ctx, clickhouseCluster, dbConfig{})
			Expect(db.Ping(ctx)).To(Succeed())

			assertReplicatedTableCreation(ctx, db)
		})

		AfterEach(func() {
			By("closing db and proxy")
			closer()
		})

		It("allows writing and reading from distributed tables", func(ctx SpecContext) {
			es := insertData(ctx, db)
			var res []row

			By("verifying inserts with each replica local table")
			assertReplicaBehaviour(ctx, es, 9000, nil)

			By("verifying data is still available from distributed table")
			Expect(db.Select(ctx, &res, "SELECT * FROM events.things")).Should(Succeed())
			Expect(res).To(ConsistOf(es))
		})

		// TODO(joe): replica scaling doesn't work at the moment.
		PIt("replicates data when the cluster is scaled up", func(ctx SpecContext) {
			es := insertData(ctx, db)

			By("scaling cluster and reading from new replica")
			ch := &clickhousev1alpha1.ClickHouse{}
			Expect(k8sClient.Get(ctx, types.NamespacedName{
				Name:      clickhouseCluster.Name,
				Namespace: clickhouseCluster.Namespace,
			}, ch)).To(Succeed())
			*ch.Spec.Replicas++
			Expect(k8sClient.Update(ctx, ch)).To(Succeed())

			db, closer := openDBReplica(ctx, ch, int(*ch.Spec.Replicas), dbConfig{})
			defer closer()
			Expect(db.Ping(ctx)).To(Succeed())

			Eventually(ctx, func(g Gomega) {
				var res []row
				g.Expect(db.Select(ctx, &res, "SELECT * FROM things_local")).Should(Succeed())

				g.Expect(res).To(ConsistOf(es))
			}).Should(Succeed())
		})
	})

	Context("ReplicatedMergeTree and Distributed tables with SSL enabled", func() {
		var (
			sslDB              clickhouse.Conn
			tlsCert            []byte
			tlsKey             []byte
			caCert             []byte
			dhParam            []byte
			closer             func()
			tlsConf            *tls.Config
			clickhouseCertName string
			dhParamKey         string
		)

		beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
			ssl: true,
		})

		JustBeforeEach(func(ctx SpecContext) {
			By("creating replicated and distributed example schemas with ssl")

			clickhouseCertName = clickhouseCluster.Name + "-" + clickhouseCertConst
			dhParamKey = clickhouseCluster.Name + "-" + "dhparam"

			secretList := &corev1.SecretList{}
			Expect(k8sClient.List(ctx, secretList)).To(Succeed())
			for _, secret := range secretList.Items {
				if secret.Name == clickhouseCertName {
					for k, v := range secret.Data {
						if k == "tls.crt" {
							tlsCert = v
						} else if k == "tls.key" {
							tlsKey = v
						} else if k == "ca.crt" {
							caCert = v
						}
					}
				} else if secret.Name == dhParamKey {
					v, ok := secret.Data["dhparam.pem"]
					if ok {
						dhParam = v
					}
				}
			}
			Expect(tlsCert).ShouldNot(BeEmpty())
			Expect(tlsKey).ShouldNot(BeEmpty())
			Expect(caCert).ShouldNot(BeEmpty())
			Expect(dhParam).ShouldNot(BeEmpty())

			caCertPool, err := x509.SystemCertPool()
			Expect(err).NotTo(HaveOccurred())
			Expect(caCertPool.AppendCertsFromPEM(caCert)).To(BeTrue())
			tlsConf = &tls.Config{
				InsecureSkipVerify: false,
				RootCAs:            caCertPool,
				MinVersion:         tls.VersionTLS12,
			}
			sslDB, closer = openDB(ctx, clickhouseCluster, dbConfig{
				tlsConfig: tlsConf,
			})
			Expect(sslDB.Ping(ctx)).To(Succeed())

			assertReplicatedTableCreation(ctx, sslDB)
		})

		AfterEach(func() {
			By("closing db and proxy")
			closer()
		})

		It("allows writing and reading from distributed tables with ssl", func(ctx SpecContext) {
			es := insertData(ctx, sslDB)
			var res []row

			By("verifying inserts with each replica local table")
			assertReplicaBehaviour(ctx, es, 9440, tlsConf)

			By("verifying data is still available from distributed table")
			Expect(sslDB.Select(ctx, &res, "SELECT * FROM events.things")).Should(Succeed())
			Expect(res).To(ConsistOf(es))
		})
	})

	assertReplicatedS3TableCreation := func(ctx SpecContext, db clickhouse.Conn) {
		// Note(Arun): this relies on the internal config of having a specific storage policy that uses S3 disks.

		Expect(db.Exec(ctx, "CREATE DATABASE events ON CLUSTER '{cluster}'")).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things_local ON CLUSTER '{cluster}' (
				dt DateTime64(9, 'UTC'),
				text String
			) ENGINE = ReplicatedMergeTree
			ORDER BY (dt) SETTINGS storage_policy='policy_s3_only'`)).To(Succeed())
		Expect(db.Exec(ctx,
			`CREATE TABLE events.things ON CLUSTER '{cluster}' AS events.things_local
			ENGINE = Distributed('{cluster}', events, things_local)`)).To(Succeed())
	}

	Context("ReplicatedMergeTree and Distributed tables with S3(object storage support) enabled", func() {
		var (
			s3Secret *corev1.Secret
			db       clickhouse.Conn
			closer   func()
		)

		const s3SecretName = "s3-secret"

		BeforeEach(func(ctx SpecContext) {
			By("creating s3 secret")
			s3Secret = &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      s3SecretName,
					Namespace: "default",
				},
				// Note(Arun): Hardcoded secrets as are provided via minio.yaml
				StringData: map[string]string{
					"accessKeyID":     "clickhouse",
					"accessKeySecret": "clickhouses3secret",
				},
			}
			Expect(k8sClient.Create(ctx, s3Secret)).To(Succeed())
			DeferCleanup(func(ctx SpecContext) {
				By("deleting s3 secret")
				Expect(k8sClient.Delete(ctx, s3Secret)).To(Succeed())
			})
		})

		// Note(Arun): It would be better to construct this endpoint externally
		const endPoint = "http://minio-dev.default.svc.cluster.local:9000/clickhouse-sandbox/root/"

		beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
			objectStorage: &clickhousev1alpha1.ClickHouseObjectStorage{
				Backend:     "S3",
				EndpointURL: endPoint,
				AccessKeyIDSecret: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: s3SecretName,
					},
					Key: "accessKeyID",
				},
				AccessKeySecret: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: s3SecretName,
					},
					Key: "accessKeySecret",
				},
			},
		})

		JustBeforeEach(func(ctx SpecContext) {
			By("creating replicated and distributed example schemas with s3 enabled")

			db, closer = openDB(ctx, clickhouseCluster, dbConfig{})
			Expect(db.Ping(ctx)).To(Succeed())

			assertReplicatedS3TableCreation(ctx, db)
		})

		AfterEach(func() {
			By("closing db and proxy")
			closer()
		})

		It("allows writing and reading from distributed tables backed by s3 disk", func(ctx SpecContext) {
			es := insertData(ctx, db)
			var res []row

			By("verifying inserts with each replica local table")
			assertReplicaBehaviour(ctx, es, 9000, nil)

			By("verifying data is still available from distributed table")
			Expect(db.Select(ctx, &res, "SELECT * FROM events.things")).Should(Succeed())
			Expect(res).To(ConsistOf(es))
		})
	})
})
