package e2e

import (
	"context"
	"math/rand"
	"strconv"
	"time"

	clickhouse "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Cluster rollout", func() {
	var clickhouseCluster *clickhousev1alpha1.ClickHouse
	var db clickhouse.Conn
	var closer func()

	var stopPing chan struct{}

	// test migrating a minor version of clickhouse
	// see also https://clickhouse.com/docs/en/operations/update#multiple-clickhouse-server-versions-in-a-cluster
	const startImage = "clickhouse/clickhouse-server:23.4-alpine"
	const endImage = "clickhouse/clickhouse-server:23.5-alpine"

	beforeEachCreateCluster(&clickhouseCluster, clusterConfig{
		image:        startImage,
		generateName: "e2e-rollout-test-",
	})

	BeforeEach(func(ctx SpecContext) {
		assertClusterReady(ctx, clickhouseCluster)

		db, closer = openDB(ctx, clickhouseCluster, dbConfig{})
		Expect(db.Ping(ctx)).To(Succeed())

		stopPing = make(chan struct{})

		ticker := time.NewTicker(1 * time.Second)
		By("start polling db in background")
		go func() {
			defer GinkgoRecover()
			for {
				select {
				case <-ticker.C:
					// allow only 5 seconds for the ping to succeed
					Eventually(func() error {
						ctx, c := context.WithTimeout(context.Background(), time.Second)
						defer c()
						err := db.Ping(ctx)

						select {
						case <-stopPing:
							return nil
						default:
							return err
						}
					}, 5, 1).Should(Succeed())
				case <-stopPing:
					ticker.Stop()
				}
			}
		}()
	})

	AfterEach(func() {
		By("stop db ping")
		close(stopPing)
		By("closing the database connection")
		closer()
	})

	Context("when updating a server image", func() {

		It("rolls out one by one", MustPassRepeatedly(2), func(ctx SpecContext) {
			clickhouseCluster.Spec.Image = endImage
			Expect(k8sClient.Update(ctx, clickhouseCluster)).Should(Succeed())

			By("wait for image rollout")
			replicaReadyOrder := map[string]int64{}
			Eventually(ctx, func(g Gomega) {
				walkClusterStatefulSets(ctx, clickhouseCluster, func(s *appsv1.StatefulSet) {
					if replicaReadyOrder[s.GetLabels()["replica"]] != 0 {
						return
					}
					ready, _, err := stsReady(s)
					g.Expect(err).NotTo(HaveOccurred())
					if ready && s.Spec.Template.Spec.Containers[0].Image == endImage {
						replicaReadyOrder[s.GetLabels()["replica"]] = time.Now().Unix()
					}
				})
				g.Expect(replicaReadyOrder).To(HaveLen(int(*clickhouseCluster.Spec.Replicas)))
			}).Should(Succeed())
			By("observed rollout stats" + spew.Sdump(replicaReadyOrder))
			By("check rollout order is correct")
			// replica readiness should be in time order
			for i := 0; i < int(*clickhouseCluster.Spec.Replicas)-1; i++ {
				Expect(replicaReadyOrder[strconv.Itoa(i)]).To(
					BeNumerically("<", replicaReadyOrder[strconv.Itoa(i+1)]),
					"replica %d should have been ready before replica %d", i, i+1)
			}

			assertClusterReady(ctx, clickhouseCluster)

			By("verify all images were updated")
			Eventually(ctx, func(g Gomega) {
				walkClusterStatefulSets(ctx, clickhouseCluster, func(sts *appsv1.StatefulSet) {
					g.Expect(sts.Spec.Template.Spec.Containers[0].Image).To(Equal(endImage))
				})
			}).Should(Succeed())
		})
	})

	Context("when randomly terminating a host", func() {
		It("should recover and keep serving connections", MustPassRepeatedly(2), func(ctx SpecContext) {
			//#nosec:G404
			delete := rand.Intn(int(*clickhouseCluster.Spec.Replicas))
			By("deleting StatefulSet " + strconv.Itoa(delete))

			Expect(k8sClient.Delete(ctx, &appsv1.StatefulSet{
				ObjectMeta: metav1.ObjectMeta{
					Name:      clickhouseCluster.Name + "-0-" + strconv.Itoa(delete),
					Namespace: clickhouseCluster.Namespace,
				}})).To(Succeed())

			By("wait for StatefulSet to be recreated")
			assertClusterReady(ctx, clickhouseCluster)
		})
	})
})
