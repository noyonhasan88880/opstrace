package v1alpha1

const (
	// StatusInProgress when we're still waiting for cluster creation.
	StatusInProgress ClickHouseClusterStatus = "InProgress"

	// StatusComplete when the cluster is entirely available.
	StatusCompleted ClickHouseClusterStatus = "Completed"

	// StatusTerminating when the resource is being deleted.
	// Note(joe): not currently used, but likely will be if we need graceful shutdown.
	StatusTerminating ClickHouseClusterStatus = "Terminating"

	// BackendS3 maps to Amazon S3 as the object storage provider.
	BackendS3 ClickhouseObjectStorageBackend = "S3"

	// BackendGCS maps to Google Cloud Storage as the object storage provider.
	// Note(Arun): Currently not in use, as clickhouse support is absent.
	BackendGCS ClickhouseObjectStorageBackend = "GCS"
)
