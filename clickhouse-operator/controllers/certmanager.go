package controllers

import (
	"fmt"

	certmanagerv1 "github.com/cert-manager/cert-manager/pkg/apis/certmanager/v1"
	cmmeta "github.com/cert-manager/cert-manager/pkg/apis/meta/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	clickhouseCertName       = "serving-cert"
	clickhouseCertSecretName = "clickhouse-cert"
	caIssuerName             = "ca-issuer"
	caCertName               = "selfsigned-ca"
	caSecretName             = "root-secret"
	dhParamsKey              = "dhparam.pem"
)

// Note(Arun): Improvement would be to be able to fetch the certificate/key pairs from external references.

func certificate(ch *clickHouseCluster) *certmanagerv1.Certificate {
	return &certmanagerv1.Certificate{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ch.prependClusterName(clickhouseCertName),
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.CertificateSpec{
			DNSNames: []string{
				fmt.Sprintf("%s.%s.svc", ch.Name, ch.Namespace),
				fmt.Sprintf("%s.%s.svc.cluster.local", ch.Name, ch.Namespace),
			},
			// Note(Arun): Although this can be considered a code smell but as we open connections on this address,
			// this is necessary to make the tls verification succeed when connecting directly via this address.
			IPAddresses: []string{"0.0.0.0"},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "Issuer",
				Name: ch.prependClusterName(caIssuerName),
			},
			SecretName: ch.prependClusterName(clickhouseCertSecretName),
			Subject: &certmanagerv1.X509Subject{
				OrganizationalUnits: []string{"clickhouse-operator"},
			},
		},
	}
}

func certificateMutator(ch *clickHouseCluster, current *certmanagerv1.Certificate) {
	cert := certificate(ch)
	current.Name = ch.prependClusterName(clickhouseCertName)
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IPAddresses = cert.Spec.IPAddresses
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.Subject = cert.Spec.Subject
}

func caIssuer(ch *clickHouseCluster) *certmanagerv1.Issuer {
	return &certmanagerv1.Issuer{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ch.prependClusterName(caIssuerName),
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.IssuerSpec{
			IssuerConfig: certmanagerv1.IssuerConfig{
				CA: &certmanagerv1.CAIssuer{
					SecretName: ch.prependClusterName(caSecretName),
					//CRLDistributionPoints: nil,
					//OCSPServers:           nil,
				},
			},
		},
	}
}

func caIssuerMutator(ch *clickHouseCluster, current *certmanagerv1.Issuer) {
	issuer := caIssuer(ch)
	current.Name = ch.prependClusterName(caIssuerName)
	current.Namespace = ch.Namespace
	current.Spec.IssuerConfig.CA = issuer.Spec.IssuerConfig.CA
}

func caCertificate(ch *clickHouseCluster) *certmanagerv1.Certificate {
	return &certmanagerv1.Certificate{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ch.prependClusterName(caCertName),
			Namespace: ch.Namespace,
		},
		Spec: certmanagerv1.CertificateSpec{
			IsCA:       true,
			CommonName: ch.prependClusterName(caCertName),
			SecretName: ch.prependClusterName(caSecretName),
			PrivateKey: &certmanagerv1.CertificatePrivateKey{
				Algorithm: "ECDSA",
				Size:      256,
			},
			DNSNames: []string{
				// No DNS names yet because the certificate is only for the CA authority
				// fmt.Sprintf("%s.%s.svc.cluster.local", getServiceName(), cr.Namespace()),
			},
			IssuerRef: cmmeta.ObjectReference{
				Kind: "ClusterIssuer",
				Name: ch.prependClusterName(rootIssuerName),
				//Group: "",
			},
			Subject: &certmanagerv1.X509Subject{
				OrganizationalUnits: []string{"clickhouse-operator"},
			},
		},
	}
}

func caCertificateMutator(ch *clickHouseCluster, current *certmanagerv1.Certificate) {
	cert := caCertificate(ch)
	current.Spec.IsCA = cert.Spec.IsCA
	current.Spec.CommonName = cert.Spec.CommonName
	current.Spec.SecretName = cert.Spec.SecretName
	current.Spec.PrivateKey = cert.Spec.PrivateKey
	current.Spec.DNSNames = cert.Spec.DNSNames
	current.Spec.IssuerRef = cert.Spec.IssuerRef
	current.Spec.Subject = cert.Spec.Subject
}

func newCACertificateResources(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	issuer := issuer(ch)
	kr = append(kr, &KubernetesResource{
		obj: issuer,
		mutator: func() error {
			issuerMutator(ch, issuer)
			return nil
		},
	})

	caCert := caCertificate(ch)
	kr = append(kr, &KubernetesResource{
		obj: caCert,
		mutator: func() error {
			caCertificateMutator(ch, caCert)
			return nil
		},
	})

	return kr
}

func newSecret(namespace string, name string, labels map[string]string,
	data map[string]string) *KubernetesResource {
	st := &corev1.Secret{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: st,
		mutator: func() error {
			st.Labels = labels
			st.StringData = data
			return nil
		},
	}
}

func newCertificateResources(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	caIssuer := caIssuer(ch)
	kr = append(kr, &KubernetesResource{
		obj: caIssuer,
		mutator: func() error {
			caIssuerMutator(ch, caIssuer)
			return nil
		},
	})

	cert := certificate(ch)
	kr = append(kr, &KubernetesResource{
		obj: cert,
		mutator: func() error {
			certificateMutator(ch, cert)
			return nil
		},
	})

	return kr
}

func newDHParamSecret(ch *clickHouseCluster) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	kr = append(kr, newSecret(
		ch.Namespace,
		fmt.Sprintf("%s-dhparam", ch.Name),
		ch.labels(),
		map[string]string{
			dhParamsKey: ch.DHParams,
		},
	))

	return kr
}
